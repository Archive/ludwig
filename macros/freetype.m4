AC_DEFUN(AM_PATH_FREETYPE2,
[dnl
dnl Get the cflags and libs from the freetype-config script
dnl
AC_ARG_WITH(freetype-prefix,[  --with-freetype-prefix=PFX   Prefix where Freetype2 is installed  (optional)],
            freetype_config_prefix="$withval", freetype_config_prefix="")
AC_ARG_WITH(freetype-exec-prefix,[  --with-freetype-exec-prefix=PFX  Exec prefix where Freetype2 is  installed (optional)],
            freetype_config_exec_prefix="$withval", freetype_config_exec_prefix="")
  if test x$freetype_config_exec_prefix != x ; then
     freetype_config_args="$freetype_config_args --exec-prefix=$freetype_config_exec_prefix"
     if test x${FREETYPE_CONFIG+set} != xset ; then
        FREETYPE_CONFIG=$freetype_config_exec_prefix/bin/freetype-config
     fi
  fi
  if test x$freetype_config_prefix != x ; then   
     freetype_config_args="$freetype_config_args --prefix=$freetype_config_prefix"
     if test x${FREETYPE_CONFIG+set} != xset ; then
        FREETYPE_CONFIG=$freetype_config_prefix/bin/freetype-config
     fi
  fi

  AC_PATH_PROG(FREETYPE_CONFIG, freetype-config, no)
  min_freetype_version=ifelse([$1], , 6:1:0, $1)

  AC_MSG_CHECKING(for Freetype2 >= $min_freetype_version)
  no_freetype=""
  if test "$FREETYPE_CONFIG" = "no" ; then
    no_freetype=yes
  else
    FREETYPE_CFLAGS="`$FREETYPE_CONFIG $freetype_config_args --cflags`"
    FREETYPE_LIBS="`$FREETYPE_CONFIG $freetype_config_args --libs`"
   
    freetype_config_major_version=`$FREETYPE_CONFIG $freetype_config_args --version | \
           sed 's/[[^0-9]]*\([[0-9]]*\)\:\([[0-9]]*\)\:\([[0-9]]*\)/\1/'`
    freetype_config_minor_version=`$FREETYPE_CONFIG $freetype_config_args --version | \
           sed 's/[[^0-9]]*\([[0-9]]*\)\:\([[0-9]]*\)\:\([[0-9]]*\)/\2/'`
    freetype_config_micro_version=`$FREETYPE_CONFIG $freetype_config_args --version | \
           sed 's/[[^0-9]]*\([[0-9]]*\)\:\([[0-9]]*\)\:\([[0-9]]*\)/\3/'`
    needed_major_version=`echo $min_freetype_version | \
           sed 's/[[^0-9]]*\([[0-9]]*\)\:\([[0-9]]*\)\:\([[0-9]]*\)/\1/'`
    needed_minor_version=`echo $min_freetype_version | \
           sed 's/[[^0-9]]*\([[0-9]]*\)\:\([[0-9]]*\)\:\([[0-9]]*\)/\2/'`
    needed_micro_version=`echo $min_freetype_version | \
           sed 's/[[^0-9]]*\([[0-9]]*\)\:\([[0-9]]*\)\:\([[0-9]]*\)/\3/'`

    if test $freetype_config_major_version -lt $needed_major_version; then
        ifelse([$3], , :, [$3])
        no_freetype=yes
    elif test $freetype_config_major_version = $needed_major_version; then
        if test -n "$needed_minor_version" -a $freetype_config_minor_version -lt $needed_minor_version; then
                ifelse([$3], , :, [$3])
                no_freetype=yes
        elif test -n "$needed_minor_version" -a $freetype_config_minor_version = $needed_minor_version; then
                if test -n "$needed_micro_version" -a $freetype_config_micro_version -lt $needed_micro_version; then
                        ifelse([$3], , :, [$3])
                        no_freetype=yes
                fi
        fi
    fi
  fi
  AC_SUBST(FREETYPE_CFLAGS)
  AC_SUBST(FREETYPE_LIBS)
    
  if test "x$no_freetype" = x ; then
     AC_MSG_RESULT(yes)
     ifelse([$2], , :, [$2])
  else
     AC_MSG_RESULT(no)
     if test "$FREETYPE_CONFIG" = "no" ; then
       echo "*** The freetype-config script installed by Freetype 2.0+ could not be found"
       echo "*** If Freetype was installed in PREFIX, make sure PREFIX/bin is in"
       echo "*** your path, or set the FREETYPE_CONFIG environment variable to the"
       echo "*** full path to freetype-config."
       echo "***"
       echo "*** If you have Freetype 2.0 installed but still receive this message, ensure"
       echo "*** that you have freetype-config installed.  Older versions of Freetype do"
       echo "*** not include this script, so you may need to upgrade.  Freetype2 sources"
       echo "*** are available from ftp://ftp.freetype.org"
       echo "***"
       echo "*** If you are using an RPM-based system, be sure that you have the"
       echo "*** approprivate devel RPM installed.  You can obtain an RPM from Eazel"
       echo "*** by going to http://developer.eazel.com/eazel-hacking/updates/freetype2."
     else
        :
     fi
     FREETYPE_CFLAGS=""
     FREETYPE_LIBS=""
     ifelse([$3], , :, [$3])
  fi
])
