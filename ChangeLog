2001-11-28  Christian Rose  <menthos@menthos.com>

	* configure.in: Added "sv" to ALL_LINGUAS.

2001-10-11  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-instrument-add-dialog.[ch]: Major changes.
	Much nicer looking than before.

2001-10-11  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-tool-manager.[ch]: tools changed to GHashTable.
	Added ludwig_tool_manager_motion().

2001-10-09  Cody Russell  <bratsche@gnome.org>

	* Various other small fixes.

2001-10-09  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-app.c: Don't #include <gnome.h>, and #include
	gtk-combo-text.h from <gal/widgets> instead of from "widgets"

2001-10-09  Cody Russell  <bratsche@gnome.org>

	* src/Makefile.am: Removed GLADE_CFLAGS and GLADE_LIBS

2001-10-09  Cody Russell  <bratsche@gnome.org>

	* art/check-filled.xpm: Removed const

2001-09-28  Cody Russell  <crussel8@peabody.jhu.edu>

	* src/ludwig-view.c: Fixed a couple warnings.

2001-09-28  Cody Russell  <crussel8@peabody.jhu.edu>

	* Updates to header comments in source/header files.

2001-09-27  Cody Russell  <crussel8@peabody.jhu.edu>

	* src/ludwig-view-frame.[ch]: Removed some (ugly) unused code.

2001-09-27  Cody Russell  <crussel8@peabody.jhu.edu>

	* config.h.in: Removed.  This is an automatically generated
	file.

2001-09-25  Cody Russell  <crussel8@peabody.jhu.edu>

	* doc/test3.xml: Fixed some errors in the XML code.

2001-09-23  Carlos Perell� Mar�n <carlos@gnome-db.org>

	* configure.in: Fixed AM_INIT_AUTOMAKE, now it works
	with xml-i18n-tools (/s/Ludwig van/Ludwig_van/)

2001-08-19  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-instrument-add-dialog.c: Redoing some of the appearance
	of this dialog.

2001-08-19  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-font/*.h: Fixed all _GET_CLASS() macros.

	* src/ludwig-font/Makefile.am: Use GTK_CFLAGS and GTK_LIBS

	* src/ludwig-view-part.c, ludwig-view-metric.c, ludwig-view-measure.c,
	test-ttf.c: Fixes for get_bpath().

2001-07-28  Cody Russell  <bratsche@gnome.org>

	* art/treble.xpm, alto.xpm, tenor.xpm, bass.xpm: Added.

2001-07-28  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-instrument-list.c: Don't convert clefs to strings.

	* src/ludwig-instrument-add-dialog.c: Added "render_clef" cell
	to ETableExtras.

2001-07-23  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-view.c: Preliminary mouse input code.

2001-07-19  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-view-frame.c: Changed parent_class to be of type
	GtkScrolledWindowClass, as it should be.

2001-07-19  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-app.c: Changed ludwig_app_parent_class to be of
	type BonoboWindowClass, as it should be.

2001-07-19  Cody Russell  <bratsche@gnome.org>

	* src/ludwig.xml: Updates.  Move labels and descriptions to
	<cmd> tags.

2001-07-13  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-interface-prefs-dialog.c: Removed pixmaps array
	and init_images() function.

2001-07-13  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-interface-prefs-dialog.c: Removed checkbox render
	cell from the ETree.  Renamed ETree's header to "Toolbars".

2001-07-13  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-app.c, src/ludwig.xml: Removed InsertTrueTypeText, 
	InsertStaff, and InsertRectangle test verbs.

	* src/ludwig-interface-prefs-dialog.c: ETree Node population now
	limits to dockitems.

2001-07-11  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-interface-prefs-dialog.[ch]: Added.

2001-07-11  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-app.[ch]: Added ludwig_app_get_document ().

	* src/ludwig.xml: Added SettingsInterfaceLayout verb.

	* src/ludwig-instrument-add-dialog.[ch]: Minor updates from
	making LudwigApp into a singleton.

	* src/ludwig-tool.[ch]: {get,set}_pixbuf() methods added.

2001-07-02  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-app.c: Initialize the tool manager upon app
	creation.

2001-07-02  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-app.[ch]: Changed LudwigApp to singleton.  Added
	ludwig_app_create_ruler_popup (), ludwig_app_create_canvas_popup ().

	Also removed test verbs for InsertTrueTypeText, InsertStaff, and
	InsertRectangle verbs.

	* src/ludwig-view.[ch]: Changes to view creation.

	* src/ludwig-zoom-control.[ch]: Updates.

	* src/ludwig-view-frame.[ch]: Updates.

	* src/main.c: Updates.

	* src/ludwig.xml: Removed InsertTrueTypeText, InsertStaff,
	and InsertRectangle verbs.

	* src/ludwig-tool-manager.[ch]: Added ludwig_tool_manager_get_tool ()

2001-07-02  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-document.[ch]: Removed LudwigPart.

	* src/ludwig-doc-item.[ch]: Added.

	* src/ludwig-doc-container.[ch]: Added.

	* src/ludwig-doc-beam.[ch]: Added.

	* src/ludwig-doc-measure.[ch]: Added.

	* src/ludwig-doc-metric.[ch]: Added.

	* src/ludwig-doc-part.[ch]: Added.

	* src/ludwig-types.h: Updated accordingly.

	* src/Makefile.am: Updated accordingly.

2001-07-02  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-tool-manager.[ch]: Added.

	* src/Makefile.am: Updated.

2001-07-02  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-tool-metric.[ch]: Added.

	* src/Makefile.am: Updated.

2001-07-02  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-document.c: Removed LudwigMetric cruft.

2001-07-02  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-metric.[ch]: Removed.

2001-07-02  Cody Russell  <bratsche@gnome.org>

	* src/ludwig-font/ft2/Makefile.am: Fixed include order.

2001-05-29  Anders Carlsson  <andersca@codefactory.se>

	* src/ludwig-font/ft2/Makefile.am (INCLUDES): Replace GTK_CFLAGS with
	GNOMEUI_CFLAGS.

	* src/ludwig-font/Makefile.am (INCLUDES): Replace GTK_CFLAGS with
	GNOMEUI_CFLAGS.

2001-05-26  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-view-part.[ch], ludwig-view.c: Removed "clef" arg from ludwigViewPart.

2001-05-26  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-tool.[ch]: Added.

2001-05-26  Cody Russell  <bratsche@gnome.org>
	Beginnings of new rendering structure.

	* src/ludwig-view-item.[ch]: Added.
	* src/ludwig-view-container.[ch]: Added.
	* src/ludwig-view-measure.[ch]: Added.
	* src/ludwig-view-part.[ch]: Added.
	* src/ludwig-view-staff.[ch]: Added.
	* src/ludwig-view-metric.[ch]: Added.

	* src/ludwig-view.[ch]: Updates with respect to above additions.

>>>>>>> 1.47
2001-05-21  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-instrument-add-dialog.c: ETree now sorts the instruments.

2001-05-21  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-instrument-list.[ch], ludwig-instrument-add-dialog.[ch], ludwig-instrument.h:
	Added instrument positioning (for sorting).

	* src/instrument-list.xml: Fixed a typo.

2001-05-21  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-instrument-list.[ch]: Removed ludwig_instrument_list_create_new(), added
	group positioning (for sorting).

	* src/instrument-list.xml: Updated for group positioning.

2001-05-21  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-view.c: Fixed tenor clef rendering.

	* src/instrument-list.xml: Updates.

2001-05-20  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-view.c: Maintain hash table of all the staves for future
	manipulation.

2001-05-20  Cody Russell  <bratsche@gnome.org>
	* src/test-ttf.c: Small updates.

	* src/ludwig-view.c: Display appropriate clef when instrument is added.

2001-05-19  Cody Russell  <bratsche@gnome.org>
	* src/test-ttf.c: Added.

	* src/ludwig-app.c: Pass app to ludwig_instrument_add_dialog_create().

	* src/ludwig-document.c: Moved ludwig_document_add_instrument() code
	to ludwig_document_add_part().  Emit signal INSTRUMENT_ADDED.

	* src/ludwig-view.c: Connect to "instrument_added" signal with add_instrument().

2001-05-18  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-instrument.[ch]: Created.

	* src/instrument-list.xml: Updated for <profile> tag.

	* src/ludwig-config.[ch]: Created.

	* src/ludwig-app.[ch]: View destruction, added LudwigDocument reference,
	added profile methods.

	* src/ludwig-document.[ch]: Removed "contents_changed" signal, added
	"instrument_added" signal.  Added ludwig_document_add_instrument ().

	* src/ludwig-instrument-add-dialog.[ch]: Maintain a list of ETreePath
	pointers for quick access to all TreeNode structs.  "Ok" button now
	installs selected instruments to LudwigDocument.  Now stores a reference
	to its LudwigApp.

	* src/ludwig-instrument-list.[ch]: Moved a lot of code out of this module
	and created ludwig-instrument.[ch] for it.  Added parsing of <profile>
	tag to the XML loading code.

	* src/ludwig-util.[ch]: Added ludwig_clef_from_string ().

2001-05-15  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-instrument-add-dialog.c: Do not render the "clef" column for
	groups, only for instruments.

	Make etree_is_editable() return TRUE only for the checkbox columns.  We don't
	want the string columns to be editable.

	* src/instrument-list.xml: Updated.

	* src/ludwig-instrument-list.c: Fixed a bug in XML parsing where default clef
	wasn't being overwritten by non-default clefs.

2001-05-15  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-instrument-list.[ch]: New foreach() access methods added.

	* src/ludwig-instrument-add-dialog.c: ETree updates; tree rendering, new
	checkbox rendering displays checkboxes only for instruments, not for groups.

	* art/checkbox-unfilled.xpm, checkbox-filled.xpm, empty.xpm, and mark.xpm added.

	* src/instrument-list.xml: Updated.

2001-05-10  Cody Russell  <bratsche@gnome.org>
	* src/ludwing-instrument-list.[ch]: Updates to structures and XML
	loading code.  Added support for <group> tag.  Made string args
	const.

2001-05-05  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-instrument-add-dialog.c: ETree stores data in ETreeMemory, not in hash.

2001-05-04  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-instrument-list.c: Added SAX XML loading code.

	* src/ludwig-instrument-add-dialog.c: Lots of ETree fixes/updates.

2001-04-29  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-document.c: Removed get_children() and get_root_children()

	* src/ludwig-instrument-list.c: Removed get_children() and get_root_children()

	* src/ludwig-util.[ch]: Added ludwig_xml_get_children() and ludwig_xml_get_root_children()

2001-04-29  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-instrument-list.c: Added XML loading.

	* src/instrument-list.xml: Added.

2001-04-24  Cody Russell  <bratsche@gnome.org>
	* configure.in: Added GAL dependency, removed src/widgets Makefile
	generation.

	* src/Makefile.am: Don't build widgets/ subdirectory anymore.

	* src/ludwig-instrument-add-dialog.[ch]: Use ETree.

2001-04-17  Cody Russell  <bratsche@gnome.org>
	* src/ludwig.xml: Changed Ruler popup menu items to radio items.

	* src/ludwig-app.c: Removed some old stuff.  Added stateful listeners to the new
	metric verbs.

2001-04-17  Cody Russell  <bratsche@gnome.org>
	* src/Makefile.am: Added ludwig-display-metrics.[ch]

	* src/ludwig-display-metrics.[ch]: Fixes.

	* src/ludwig-app.c: Added metrics verbs.

	* src/ludwig.xml: Added metrics verbs.  Added Ruler popup.

	* src/ludwig-view-frame.c: Added ruler popup menu code.

	* src/ludwig-view.[ch]: Added ludwig_view_set_metric_units().

2001-04-16  Cody Russell  <bratsche@gnome.org>
	* src/canvas-items/gp-path.[ch]: Added.

	* src/canvas-items/gnome-canvas-bpath.[ch]: Added.

	* src/ludwig-view-frame.c: Split layout_adjustment_changed() into vertical and
	horizontal functions, layout_vadjustment_changed() and layout_hadjustment_changed().

2001-04-15  Cody Russell  <bratsche@gnome.org>
	* Renamed libgnomefont to ludwig-font to prevent clashes with gnome-print's font lib.

	* src/ludwig-view-frame.[ch]: Added rulers to the canvas.

	* src/ludwig-view.[ch]: Added ludwig_view_add_staff().  Added page sizes and margins and
	corresponding functions.

	* src/ludwig.xml: Added PageProperties verb.  Added Canvas3 popup menu.

	* src/ludwig-display-metrics.[ch]: Added.

	* Tons of other smaller changes.

2001-03-11  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-add-dialog.[ch]: Added, but not built yet.
	Severely unfinished!

	* src/ludwig-utils.c: Open installed font file.

	* fonts/Makefile.am: Added.

	We'll now install fonts to $prefix/share/ludwig/fonts

	* Makefile.am
	* configure.in: Added fonts to SUBDIRS, configure.in.

2001-03-10  Cody Russell  <bratsche@gnome.org>
	* src/Makefile.am: Install ludwig.xml to $prefix/gnome/ui/

	* src/ludwig-app.c: Don't check for ludwig.xml.

	* Makefile.am
	* configure.in
	* acconfig.h
	* macros/gnome.m4
	* macros/freetype.m4  -  (added)
	* src/Makefile.am
	* src/canvas-items/Makefile.am
	* src/libgnomefont/Makefile.am
	* src/libgnomefont/ft2/Makefile.am
	* src/widgets/Makefile.am:
	Major hackage done to configure and build process.  Things
	should actually build semi-correctly now.  Note that we now
	demand FreeType 2.0+ and do not support Freetype 1.3 or GT1.

	* src/ludwig-app.[ch]: Removed #include <bonobo-ui-xml.h>

2001-02-23  Cody Russell  <bratsche@gnome.org>
	* src/ludwig-zoom-control.c: Update BonoboControl stuff with
	respect to the latest Bonobo.

	* src/libgnomefont/Makefile.am: Hackery to attempt to make it
	build correctly.  This was probably wrong, though.

2001-01-30  Cody Russell  <bratsche@gnome.org>
	* src/main.c: Include Glade headers and init Glade library.  We'll
	use it soon.

	* src/ludwig-instrument-list.[ch]: Added functions:
		ludwig_instrument_list_add_instrument ()
		ludwig_instrument_list_set_default_clef ()
		ludwig_instrument_list_set_pitch_ceiling ()
		ludwig_instrument_list_set_pitch_floor ()
		ludwig_instrument_list_add_valid_clef ()
		ludwig_instrument_list_remove_valid_clef ()

2001-01-27  Cody Russell  <bratsche@gnome.org>
	* configure.in: Changed XML check, added GConf check.

	* src/ludwig-zoom-control.[ch], src/ludwig-app.c: Changed
	LudwigZoomControl->zoom_control_names from GPtrArray to GArray.

	* src/main.c: Removed some warnings.

	* src/canvas-items/gnome-canvas-ttext.c: Removed // style comments.

	* src/ludwig-document.[ch]: Several small changes.

	* src/ludwig-instrument-list.[ch]: Added.

	* src/ludwig-instrument-add-dialog.glade: Added.

2001-01-09  Cody Russell  <bratsche@gnome.org>

	* src/canvas-items/ludwig-canvas-staff.c: Removed old debugging
	code.

	* src/ludwig-app.c: about_cb() updates.

	* src/main.c: Removed old about_cb()

	* src/ludwig-app.c: Fixed notebook_page_select() to set the zoom
	control's entry widget directly, rather than setting the
	GtkComboText.

	* src/ludwig-view.c, src/canvas-itms/glyphlist.c,
	src/canvas-items/gnome-canvas-ttext.c,
	src/libgnomefont/ft2/gnome-font-face-ft2.c,
	src/libgnomefont/ft2/gnome-font-glyph-ft2.c: Removed old
	debugging stuff.

	* src/ludwig-view-frame.[ch], src/ludwig-zoom-control.[ch],
	src/ludwig-types.h: Added.

	* src/Makefile.am: Added above files.

	* src/ludwig-app.[ch]: Removed LudwigAppPrivate structure and
	moved contents into LudwigApp, and reworked zoom control.

	* src/ludwig-view.[ch]: Rewritten to use BonoboControl.

2000-12-09  Cody Russell  <bratsche@gnome.org>

	* src/canvas-items/ludwig-canvas-staff.c: Make the space between
	the lines, the width of the lines, and the length of the lines
	all expand proportionally to the zoom of the canvas.

	* src/ludwig-util.c: Updated with respect to set_encoding changes.
	Also changed the default encoding from Unicode to Apple Roman.

	* src/ludwig.xml: Make the "Preferences" icon display correctly.

	* src/libgnomefont/ft2/gnome-font-face-ft2.[ch]: set_encoding
	functions now use GnomeFontEncoding instead of const gchar *

	* src/libgnomefont/gnome-font-face.[ch]: Added GnomeFontEncoding
	and changed set_encoding functions to use it.

2000-11-20  Cody Russell  <bratsche@gnome.org>

	* configure.in: Look for Bonobo 0.29.  Attempt to find
	FreeType 2.0 more easily.

2000-11-19  Cody Russell  <bratsche@gnome.org>

	* Initial CVS import

	* src/ludwig.xml: Added.

	* src/ludwig-app.[ch]: Changed all instances of BonoboWin
	and bonobo_win to BonoboWindow and bonobo_window.

