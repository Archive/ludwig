/*
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_TYPES_H__
#define __LUDWIG_TYPES_H__

typedef struct _LudwigApp      LudwigApp;
typedef struct _LudwigAppClass LudwigAppClass;

typedef struct _LudwigView      LudwigView;
typedef struct _LudwigViewClass LudwigViewClass;

typedef struct _LudwigViewFrame      LudwigViewFrame;
typedef struct _LudwigViewFrameClass LudwigViewFrameClass;

typedef struct _LudwigZoomControl      LudwigZoomControl;
typedef struct _LudwigZoomControlClass LudwigZoomControlClass;

typedef struct _LudwigDocument      LudwigDocument;
typedef struct _LudwigDocumentClass LudwigDocumentClass;
//typedef struct _LudwigPart          LudwigPart;
typedef struct _LudwigTimeSig       LudwigTimeSig;

typedef struct _LudwigDocItem           LudwigDocItem;
typedef struct _LudwigDocItemClass      LudwigDocItemClass;
typedef struct _LudwigDocContainer      LudwigDocContainer;
typedef struct _LudwigDocContainerClass LudwigDocContainerClass;
typedef struct _LudwigDocBeam           LudwigDocBeam;
typedef struct _LudwigDocBeamClass      LudwigDocBeamClass;
typedef struct _LudwigDocMeasure        LudwigDocMeasure;
typedef struct _LudwigDocMeasureClass   LudwigDocMeasureClass;
typedef struct _LudwigDocMetric         LudwigDocMetric;
typedef struct _LudwigDocMetricClass    LudwigDocMetricClass;
typedef struct _LudwigDocPart           LudwigDocPart;
typedef struct _LudwigDocPartClass      LudwigDocPartClass;

typedef enum
{
        CLEF_NONE,
        CLEF_TREBLE,
        CLEF_ALTO,
        CLEF_TENOR,
        CLEF_BASS
} LudwigClef;

#endif /* __LUDWIG_TYPES_H__ */
