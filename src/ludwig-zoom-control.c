/*
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>
#include "ludwig-app.h"
#include "ludwig-view.h"
#include "ludwig-zoom-control.h"
#include <gal/widgets/gtk-combo-text.h>

#define ZOOM_LEVEL_KEY "ZOOM_KEY"

enum
{
        PROP_LEVEL
};

static void
ludwig_zoom_control_get_prop (BonoboPropertyBag *bag,
                              BonoboArg         *arg,
                              guint              arg_id,
                              CORBA_Environment *ev,
                              gpointer           user_data)
{
	GtkObject *zoom = user_data;
        gint i;

	switch (arg_id)
	{
		case PROP_LEVEL:
			i = GPOINTER_TO_UINT (gtk_object_get_data (zoom, ZOOM_LEVEL_KEY));
			BONOBO_ARG_SET_INT (arg, i);
			break;
		default:
			g_warning ("Unhandled arg %d\n", arg_id);
			break;
	}
}

static void
ludwig_zoom_control_set_prop (BonoboPropertyBag *bag,
                              const BonoboArg   *arg,
                              guint              arg_id,
                              CORBA_Environment *ev,
                              gpointer           user_data)
{
	GtkComboText *zoom = user_data;
        guint i;

	switch (arg_id)
	{
		case PROP_LEVEL:
			i = BONOBO_ARG_GET_INT (arg);

			/* Do something with the value here */

			gtk_object_set_data (GTK_OBJECT (zoom), ZOOM_LEVEL_KEY,
                                             GUINT_TO_POINTER (i));
			break;

		default:
			g_warning ("Unhandled arg %d\n", arg_id);
			break;
	}
}

static double
zoom_desc_to_float (const gchar *str)
{
        char *tmp;
        double x;

        tmp = g_strdup (str);

        tmp[strlen (tmp) - 1] = '\0';  /* Take off the '%' sign at the end. */
        x = atol (tmp);
        x = x / 100;

        g_free (tmp);

        return x;
}

static void
zoom_changed_cb (GtkWidget *widget, gpointer data)
{
        LudwigView  *view;
        gchar       *str;
        gchar       *tmp;
        double       new_zoom_level;

        view = ludwig_app_get_current_view ();
        if (!view)
        {
                g_warning ("zoom_changed_cb (): view == NULL!\n");
                return;
        }

        view = ludwig_app_get_current_view ();

        str = gtk_entry_get_text (GTK_ENTRY (widget));
        if (!str)
                return;

        tmp = g_strdup (str);
        new_zoom_level = zoom_desc_to_float (tmp);

        gtk_signal_emit_by_name (GTK_OBJECT (view->zoomable), "set_zoom_level",
                                 new_zoom_level);
}

static void
ludwig_zoom_control_destroy (GtkObject *object)
{
        gtk_widget_destroy (LUDWIG_ZOOM_CONTROL (object)->zoom_box);
}

static void
ludwig_zoom_control_init (LudwigZoomControl *control)
{
        static float zoom_levels[] = { 2.0, 1.5, 1.0, 0.75, 0.5, 0.25 };
        static char *zoom_level_names[] = { "200%", "150%", "100%", "75%", "50%", "25%" };
        int n_zoom_levels = sizeof (zoom_levels) / sizeof (float);

        control->zoom_box = NULL;

        control->zoom_levels = g_array_new (FALSE, FALSE, sizeof (float));
        g_array_append_vals (control->zoom_levels,
                             zoom_levels,
                             n_zoom_levels);

        control->zoom_level_names = g_array_new (FALSE, FALSE, sizeof (gchar *));
        g_array_append_vals (control->zoom_level_names,
                             zoom_level_names,
                             n_zoom_levels);
}

static void
ludwig_zoom_control_class_init (LudwigZoomControlClass *klass)
{
        GtkObjectClass *object_class;

        object_class = (GtkObjectClass *) klass;

        object_class->destroy = ludwig_zoom_control_destroy;
}

GtkType
ludwig_zoom_control_get_type (void)
{
        static GtkType zoom_type = 0;

        if (!zoom_type)
        {
                const GtkTypeInfo zoom_info =
                {
                        "LudwigZoomControl",
                        sizeof (LudwigZoomControl),
                        sizeof (LudwigZoomControlClass),
                        (GtkClassInitFunc)  ludwig_zoom_control_class_init,
                        (GtkObjectInitFunc) ludwig_zoom_control_init,
                        NULL, /* Reserved 1 */
                        NULL, /* Reserved 2 */
                        (GtkClassInitFunc) NULL,
                };

                zoom_type = gtk_type_unique (bonobo_control_get_type (),
                                             &zoom_info);
        }

        return zoom_type;
}

LudwigZoomControl *
ludwig_zoom_control_new ()
{
	BonoboPropertyBag *pb;
	LudwigZoomControl *zoom_control;
        BonoboControl     *control;
	GtkWidget         *zoom_box;
        GtkWidget         *entry;
        int                len, i;

        zoom_control = gtk_type_new (ludwig_zoom_control_get_type ());

	/* Create the widget */
	zoom_box = gtk_combo_text_new (FALSE);
	gtk_combo_box_set_title (GTK_COMBO_BOX (zoom_box), _("Zoom"));
        gtk_widget_show_all (zoom_box);

	entry = GTK_COMBO_TEXT (zoom_box)->entry;
	len = gdk_string_measure (entry->style->font, "%10000");
	gtk_widget_set_usize (entry, len, 0);

	/* Preset values */
	for (i = 0; i < zoom_control->zoom_levels->len; i++)
	{
		gtk_combo_text_add_item (GTK_COMBO_TEXT (zoom_box),
					 g_array_index (zoom_control->zoom_level_names, gchar *, i),
                                         g_array_index (zoom_control->zoom_level_names, gchar *, i));
	}

	/* Select 100% by default */
	gtk_combo_text_select_item (GTK_COMBO_TEXT (zoom_box), 2);

	gtk_signal_connect (GTK_OBJECT (entry), "activate",
			    GTK_SIGNAL_FUNC (zoom_changed_cb), NULL);

	control = bonobo_control_construct (BONOBO_CONTROL (zoom_control), zoom_box);

	/* Create the properties */
	pb = bonobo_property_bag_new (ludwig_zoom_control_get_prop,
                                      ludwig_zoom_control_set_prop,
                                      zoom_box);
	bonobo_control_set_properties (control, pb);
	bonobo_object_unref (BONOBO_OBJECT (pb));

	bonobo_property_bag_add (pb, "level", PROP_LEVEL,
                                 BONOBO_ARG_INT, NULL,
                                 "The zoom level", 0);

	return LUDWIG_ZOOM_CONTROL (control);
}
