/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include "ludwig-view-container.h"
#include "ludwig-types.h"

#ifndef __LUDWIG_VIEW_STAFF_H__
#define __LUDWIG_VIEW_STAFF_H__

#define LUDWIG_TYPE_VIEW_STAFF        (ludwig_view_staff_get_type())
#define LUDWIG_VIEW_STAFF(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_VIEW_STAFF, LudwigViewStaff))
#define LUDWIG_VIEW_STAFF_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_VIEW_STAFF, LudwigViewStaffClass))
#define LUDWIG_IS_VIEW_STAFF(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_VIEW_STAFF))
#define LUDWIG_IS_VIEW_STAFF_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_VIEW_STAFF))

typedef struct _LudwigViewStaff      LudwigViewStaff;
typedef struct _LudwigViewStaffClass LudwigViewStaffClass;

struct _LudwigViewStaff
{
        LudwigViewContainer parent_object;

        double height;
        LudwigClef start_clef;

        double max_length;
        double length;

        LudwigViewStaff *next;
        LudwigViewStaff *prev;
};

struct _LudwigViewStaffClass
{
        LudwigViewContainerClass parent_class;
};

GtkType   ludwig_view_staff_get_type   (void);
void      ludwig_view_staff_construct  (LudwigViewStaff *staff);

#endif /* __LUDWIG_VIEW_STAFF_H__ */
