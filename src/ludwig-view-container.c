/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include "ludwig-view-container.h"

static void ludwig_view_container_class_init (LudwigViewContainerClass *class);
static void ludwig_view_container_init       (LudwigViewContainer      *container);
static void ludwig_view_container_destroy    (GtkObject                *object);

static LudwigViewItemClass *parent_class = NULL;

GtkType
ludwig_view_container_get_type (void)
{
        static GtkType type = 0;

        if (!type)
        {
                GtkTypeInfo info =
                {
                        "LudwigViewContainer",
                        sizeof (LudwigViewContainer),
                        sizeof (LudwigViewContainerClass),
                        (GtkClassInitFunc) ludwig_view_container_class_init,
                        (GtkObjectInitFunc) ludwig_view_container_init,
                        NULL, /* Reserved 1 */
                        NULL, /* Reserved 2 */
                        (GtkClassInitFunc) NULL
                };

                type = gtk_type_unique (ludwig_view_item_get_type (), &info);
        }

        return type;
}

static void
ludwig_view_container_class_init (LudwigViewContainerClass *class)
{
        GtkObjectClass *object_class;

        object_class = (GtkObjectClass *) class;

        parent_class = gtk_type_class (ludwig_view_item_get_type ());

        object_class->destroy = ludwig_view_container_destroy;
};

static void
ludwig_view_container_init (LudwigViewContainer *container)
{
}

static void
ludwig_view_container_destroy (GtkObject *object)
{
}
