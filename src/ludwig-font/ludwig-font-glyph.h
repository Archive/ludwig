#ifndef __LUDWIG_FONT_GLYPH_H__
#define __LUDWIG_FONT_GLYPH_H__

/*
 * LudwigFontGlyph
 *
 * Abstract base class for separate glyphs
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#define LUDWIG_TYPE_FONT_GLYPH		  (ludwig_font_glyph_get_type ())
#define LUDWIG_FONT_GLYPH(obj)		  (GTK_CHECK_CAST ((obj), LUDWIG_TYPE_FONT_GLYPH, LudwigFontGlyph))
#define LUDWIG_FONT_GLYPH_CLASS(klass)	  (GTK_CHECK_CLASS_CAST ((klass), LUDWIG_TYPE_FONT_GLYPH, LudwigFontGlyphClass))
#define LUDWIG_IS_FONT_GLYPH(obj)	  (GTK_CHECK_TYPE ((obj), LUDWIG_TYPE_FONT_GLYPH))
#define LUDWIG_IS_FONT_GLYPH_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), LUDWIG_TYPE_FONT_GLYPH))
#define LUDWIG_FONT_GLYPH_GET_CLASS(o)    ((LudwigFontGlyphClass *)(((GtkObject *) o)->klass))

typedef struct _LudwigFontGlyph	       LudwigFontGlyph;
typedef struct _LudwigFontGlyphClass   LudwigFontGlyphClass;
typedef struct _LudwigFontGlyphPrivate LudwigFontGlyphPrivate;

#include <gtk/gtk.h>
#include <libart_lgpl/art_rect.h>
#include <libart_lgpl/art_bpath.h>
#include "ludwig-vfont.h"

typedef struct _LudwigFontGlyphGrayMap LudwigFontGlyphGrayMap;
typedef struct _LudwigFontGlyphGdkMap LudwigFontGlyphGdkMap;

enum
{
	LUDWIG_FONT_GLYPH_CACHED = 1 << 4
};

/* NB! These are experimental and probably will change */

struct _LudwigFontGlyphGrayMap
{
	guchar *pixels;
	gint width, height, rowstride;
	gint x0, y0;
};

struct _LudwigFontGlyphGdkMap
{
	GdkPixmap * pixmap;
	GdkBitmap * mask;
	gint x0, y0;
};

struct _LudwigFontGlyph
{
	GtkObject object;

	/* Our owner */
	LudwigVFont *vfont;
	gint code;
};

struct _LudwigFontGlyphClass
{
	GtkObjectClass parent_class;

	const ArtBpath               * (* bpath)   (LudwigFontGlyph*glyph, gboolean with_transform);

	/* fixme: This will be changed */
	void                           (* metrics) (LudwigFontGlyph *glyph, ArtDRect * bbox, ArtPoint * advance);
	const LudwigFontGlyphGrayMap * (* graymap) (LudwigFontGlyph *glyph);
	const LudwigFontGlyphGdkMap  * (* gdkmap)  (LudwigFontGlyph *glyph);
};

GtkType ludwig_font_glyph_get_type (void);

#define ludwig_font_glyph_ref(g)   gtk_object_ref (GTK_OBJECT (g))
#define ludwig_font_glyph_unref(g) gtk_object_unref (GTK_OBJECT (g))

/*
 * The interpretation of glyph metrics depend, whether glyph belongs to
 * LudwigFace, LudwigFont or LudwigTranformedFont
 */

const ArtBpath *ludwig_font_glyph_get_bpath (LudwigFontGlyph *glyph, gboolean with_transform);

/*
 * Possible candidates for portable functions
 * Not, that font direction should be already encoded into font,
 * all bearings, outlines and bitmaps are also adjusted for direction
 */

ArtDRect *ludwig_font_glyph_get_dimensions (LudwigFontGlyph *glyph, ArtDRect *box);
ArtPoint *ludwig_font_glyph_get_stdadvance (LudwigFontGlyph *glyph, ArtPoint *p);

/*
 * Graymap
 */

const LudwigFontGlyphGrayMap *ludwig_font_glyph_get_graymap (LudwigFontGlyph *glyph);

/*
 * This is not sufficent - probably
 * What about multiple visuals/displays - but still then there should be
 * probably multiple rfonts anyway
 * Also - handle coloring
 */

const LudwigFontGlyphGdkMap *ludwig_font_glyph_get_gdkmap (LudwigFontGlyph *glyph);

/*
 * Kerning
 */

ArtPoint *ludwig_font_glyph_get_kerning (LudwigFontGlyph *a, LudwigFontGlyph *b, ArtPoint *p);

#endif
