#define LUDWIG_RFONT_C

/*
 * LudwigRFont
 *
 * Base class for rasterized fonts, i.e. the ones scaled to real output device
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <libart_lgpl/art_affine.h>
#include "ludwig-rfont.h"

static void ludwig_rfont_class_init (LudwigRFontClass * klass);
static void ludwig_rfont_init (LudwigRFont * rfont);

static void ludwig_rfont_destroy (GtkObject * object);

static LudwigVFontClass *parent_class = NULL;

GtkType
ludwig_rfont_get_type (void)
{
	static GtkType group_type = 0;

	if (!group_type)
        {
		GtkTypeInfo group_info =
                {
			"LudwigRFont",
			sizeof (LudwigRFont),
			sizeof (LudwigRFontClass),
			(GtkClassInitFunc) ludwig_rfont_class_init,
			(GtkObjectInitFunc) ludwig_rfont_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};

		group_type = gtk_type_unique (ludwig_vfont_get_type (), &group_info);
	}

	return group_type;
}

static void
ludwig_rfont_class_init (LudwigRFontClass *klass)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class (ludwig_vfont_get_type ());

	object_class->destroy = ludwig_rfont_destroy;
}

static void
ludwig_rfont_init (LudwigRFont *rfont)
{
	rfont->font = NULL;
	art_affine_identity (rfont->affine);
}

static void
ludwig_rfont_destroy (GtkObject *object)
{
	LudwigRFont *font;

	font = (LudwigRFont *) object;

	/* fixme: unref font */

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

