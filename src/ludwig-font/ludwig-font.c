#define LUDWIG_FONT_C

/*
 * LudwigFont
 *
 * Base class for user visible font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <locale.h>
#include <libart_lgpl/art_affine.h>
#include "ludwig-font.h"

static void ludwig_font_class_init (LudwigFontClass * klass);
static void ludwig_font_init (LudwigFont * group);

static void ludwig_font_destroy (GtkObject * object);

static LudwigVFontClass * parent_class = NULL;

gboolean ludwig_font_init_library(void)
{
   return ludwig_font_real_init_library();
}

GtkType
ludwig_font_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"LudwigFont",
			sizeof (LudwigFont),
			sizeof (LudwigFontClass),
			(GtkClassInitFunc) ludwig_font_class_init,
			(GtkObjectInitFunc) ludwig_font_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (ludwig_vfont_get_type (), &group_info);
	}
	return group_type;
}

static void
ludwig_font_class_init (LudwigFontClass * klass)
{
	GtkObjectClass * object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class (ludwig_vfont_get_type ());

	object_class->destroy = ludwig_font_destroy;
}

static void
ludwig_font_init (LudwigFont * font)
{
	font->face = NULL;
	art_affine_identity (font->affine);
}

static void
ludwig_font_destroy (GtkObject * object)
{
	LudwigFont * font;

	font = (LudwigFont *) object;

	if (font->face) {
		ludwig_font_face_unref (font->face);
		font->face = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* Methods */

const LudwigFontFamily *
ludwig_font_get_family (const LudwigFont * font)
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT (font), NULL);

	return ludwig_font_face_get_family (font->face);
}

/* NB! Unlike face internal functions, these return name in current locale */

const gchar *
ludwig_font_get_family_name (LudwigFont * font)
{
	gchar * loc;

	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT (font), NULL);

	loc = setlocale (LC_MESSAGES, NULL);

	return ludwig_font_face_get_family_name (font->face, loc);
}

const gchar *
ludwig_font_get_type_name (LudwigFont * font)
{
	gchar * loc;

	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT (font), NULL);

	loc = setlocale (LC_MESSAGES, NULL);

	return ludwig_font_face_get_type_name (font->face, loc);
}

const gchar *
ludwig_font_get_canonical_name (LudwigFont * font)
{
	gchar * loc;

	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT (font), NULL);

	loc = setlocale (LC_MESSAGES, NULL);

	return ludwig_font_face_get_canonical_name (font->face, loc);
}

const gchar *
ludwig_font_get_ps_name (const LudwigFont * font)
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT (font), NULL);

	return ludwig_font_face_get_ps_name (font->face);
}

LudwigRFont *
ludwig_font_get_rfont (LudwigFont * font, gdouble affine[])
{
	g_return_val_if_fail (font != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT (font), NULL);
	g_return_val_if_fail (affine != NULL, NULL);

	if (LUDWIG_FONT_GET_CLASS (font)->get_rfont)
        {
		return (* LUDWIG_FONT_GET_CLASS (font)->get_rfont) (font, affine);
	}
        else
        {
		g_warning ("Font does not implement get_rfont method!");
	}

	return NULL;
}

