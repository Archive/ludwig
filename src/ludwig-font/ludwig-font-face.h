#ifndef __LUDWIG_FONT_FACE_H__
#define __LUDWIG_FONT_FACE_H__

/*
 * LudwigFontFace
 *
 * Abstract, unsized typeface handle. Note, that this is also abstract base
 * class for inheriting implementations of different font file formats
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#define LUDWIG_TYPE_FONT_FACE		(ludwig_font_face_get_type ())
#define LUDWIG_FONT_FACE(o)             (GTK_CHECK_CAST ((o), LUDWIG_TYPE_FONT_FACE, LudwigFontFace))
#define LUDWIG_FONT_FACE_CLASS(k)       (GTK_CHECK_CLASS_CAST ((k), LUDWIG_TYPE_FONT_FACE, LudwigFontFaceClass))
#define LUDWIG_IS_FONT_FACE(o)	        (GTK_CHECK_TYPE ((o), LUDWIG_TYPE_FONT_FACE))
#define LUDWIG_IS_FONT_FACE_CLASS(k)    (GTK_CHECK_CLASS_TYPE ((k), LUDWIG_TYPE_FONT_FACE))
#define LUDWIG_FONT_FACE_GET_CLASS(o)   ((LudwigFontFaceClass *)(((GtkObject *) o)->klass))

typedef struct _LudwigFontFace		LudwigFontFace;
typedef struct _LudwigFontFaceClass	LudwigFontFaceClass;
typedef struct _LudwigFontFacePrivate	LudwigFontFacePrivate;
typedef struct _LudwigFontFaceDesc	LudwigFontFaceDesc;

#include "ludwig-font-family.h"
#include "ludwig-vfont.h"
#include "ludwig-font.h"

typedef enum
{
	LUDWIG_FONT_ENCODING_NONE,
	LUDWIG_FONT_ENCODING_SYMBOL,
	LUDWIG_FONT_ENCODING_UNICODE,
	LUDWIG_FONT_ENCODING_LATIN2,
	LUDWIG_FONT_ENCODING_SJIS,
	LUDWIG_FONT_ENCODING_GB2312,
	LUDWIG_FONT_ENCODING_BIG5,
	LUDWIG_FONT_ENCODING_WANSUNG,
	LUDWIG_FONT_ENCODING_JOHAB,
	LUDWIG_FONT_ENCODING_ADOBE_STANDARD,
	LUDWIG_FONT_ENCODING_ADOBE_EXPERT,
	LUDWIG_FONT_ENCODING_ADOBE_CUSTOM,
	LUDWIG_FONT_ENCODING_APPLE_ROMAN
} LudwigFontEncoding;

struct _LudwigFontFace
{
	LudwigVFont            vfont;
	GSList                *fonts;
	LudwigFontEncoding     encoding;
	LudwigFontEncoding    *encodings;
	LudwigFontFacePrivate *private;
};

struct _LudwigFontFaceClass
{
	LudwigVFontClass parent_class;

	/* get fresh allocated face description in given language */
	LudwigFontFaceDesc * (* description)  (LudwigFontFace *face, const gchar * language);

	/* Get member font given master resolution */
	LudwigFont         * (* get_font)     (LudwigFontFace *face, gdouble size, gdouble transform[]);

	/* Set encoding ID to given encoding name, TRUE, if successful */
	gboolean             (* set_encoding) (LudwigFontFace *face, gint id, LudwigFontEncoding encoding);

	/* Return matching glyph in given encoding table */
	gint                 (* lookup)       (LudwigFontFace *face, gint encoding, gint code);
};

GtkType ludwig_font_face_get_type (void);

LudwigFontFace *ludwig_font_face_new_from_file (const gchar *filename,
                                                const gchar *afm,
                                                gint face);

#define ludwig_font_face_ref(f)   gtk_object_ref   (GTK_OBJECT (f))
#define ludwig_font_face_unref(f) gtk_object_unref (GTK_OBJECT (f))

LudwigFontFamily *ludwig_font_face_get_family (const LudwigFontFace *face);

/* These are UTF-8 */

const gchar *ludwig_font_face_get_family_name    (LudwigFontFace *face, const gchar *language);
const gchar *ludwig_font_face_get_type_name      (LudwigFontFace *face, const gchar *language);
const gchar *ludwig_font_face_get_canonical_name (LudwigFontFace *face, const gchar *language);

/* ASCII */

const gchar *ludwig_font_face_get_ps_name (LudwigFontFace *face);

/* Font */

LudwigFont * ludwig_font_face_get_font      (LudwigFontFace *face, gdouble size, gdouble xres, gdouble yres);
LudwigFont * ludwig_font_face_get_font_full (LudwigFontFace *face, gdouble size, gdouble transform[]);

/* Encode */

/* NB! This is experimental and probably will be removed */
/* Also we may want to get listing of TT/OT charmaps before */

gboolean ludwig_font_face_set_encoding (LudwigFontFace *face, gint id, const gchar *name, LudwigFontEncoding encoding);

/* NB! This should be moved to get_glyph probably */

gint ludwig_font_face_lookup (LudwigFontFace *face, gint encoding, gint code);

#endif
