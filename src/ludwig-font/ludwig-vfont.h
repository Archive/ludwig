#ifndef __LUDWIG_VFONT_H__
#define __LUDWIG_VFONT_H__

/*
 * LudwigVFont
 *
 * Abstract virtual base class for all glyph collection types
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#define LUDWIG_TYPE_VFONT	     (ludwig_vfont_get_type ())
#define LUDWIG_VFONT(obj)	     (GTK_CHECK_CAST ((obj), LUDWIG_TYPE_VFONT, LudwigVFont))
#define LUDWIG_VFONT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), LUDWIG_TYPE_VFONT, LudwigVFontClass))
#define LUDWIG_IS_VFONT(obj)	     (GTK_CHECK_TYPE ((obj), LUDWIG_TYPE_VFONT))
#define LUDWIG_IS_VFONT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), LUDWIG_TYPE_VFONT))
#define LUDWIG_VFONT_GET_CLASS(o)    ((LudwigVFontClass *)(((GtkObject *) o)->klass))

typedef struct _LudwigVFont	 LudwigVFont;
typedef struct _LudwigVFontClass LudwigVFontClass;

#include <gtk/gtk.h>
#include "ludwig-font-glyph.h"

struct _LudwigVFont
{
	GtkObject object;
	GHashTable * glyphs;
};

struct _LudwigVFontClass
{
	GtkObjectClass parent_class;

	LudwigFontGlyph * (* get_glyph) (LudwigVFont *vfont, gint code);
	ArtPoint        * (* kerning)   (LudwigVFont *vfont, gint c1, gint c2, ArtPoint *p);
};

GtkType ludwig_vfont_get_type (void);

#define ludwig_vfont_ref(f)   gtk_object_ref (GTK_OBJECT (f))
#define ludwig_vfont_unref(f) gtk_object_unref (GTK_OBJECT (f))

/* Methods */

LudwigFontGlyph *ludwig_vfont_get_glyph (LudwigVFont *font, gint code);

#endif
