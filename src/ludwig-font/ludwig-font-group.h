#ifndef LUDWIG_FONT_GROUP_H
#define LUDWIG_FONT_GROUP_H

#include <gtk/gtk.h>

/* FontGroup */

#define LUDWIG_TYPE_FONT_GROUP		  (ludwig_font_group_get_type ())
#define LUDWIG_FONT_GROUP(obj)		  (GTK_CHECK_CAST ((obj), LUDWIG_TYPE_FONT_GROUP, LudwigFontGroup))
#define LUDWIG_FONT_GROUP_CLASS(klass)	  (GTK_CHECK_CLASS_CAST ((klass), LUDWIG_TYPE_FONT_GROUP, LudwigFontGroupClass))
#define LUDWIG_IS_FONT_GROUP(obj)	  (GTK_CHECK_TYPE ((obj), LUDWIG_TYPE_FONT_GROUP))
#define LUDWIG_IS_FONT_GROUP_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), LUDWIG_TYPE_FONT_GROUP))

typedef struct _LudwigFontGroup		LudwigFontGroup;
typedef struct _LudwigFontGroupClass	LudwigFontGroupClass;
typedef struct _LudwigFontGroupPrivate	LudwigFontGroupPrivate;

struct _LudwigFontGroup
{
	GtkObject object;
	LudwigFontGroupPrivate * private;
};

struct _LudwigFontGroupClass
{
	GtkObjectClass parent_class;
};

GtkType          ludwig_font_group_get_type (void);

void             ludwig_font_group_ref      (LudwigFontGroup *group);
void             ludwig_font_group_unref    (LudwigFontGroup *group);

LudwigFontGroup *ludwig_font_get_group      (const gchar *name);
const GSList    *ludwig_font_group_list     (const gchar *language);

#endif
