#ifndef __LUDWIG_FONT_CONFIG_H__
#define __LUDWIG_FONT_CONFIG_H__

#include <glib.h>

void ludwig_font_config_try_file (const gchar *path);

#endif
