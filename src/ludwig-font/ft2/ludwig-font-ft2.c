#define LUDWIG_FONT_FT2_C

/*
 * LudwigFontFT2
 *
 * FreeType font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <math.h>
#include "gf-freetype2.h"
#include "ludwig-font-face-ft2.h"
#include "ludwig-font-ft2.h"
#include "ludwig-font-ft2-private.h"
#include "ludwig-rfont-ft2.h"
#include "ludwig-rfont-ft2-private.h"
#include "ludwig-font-glyph-ft2.h"
#include "ludwig-font-glyph-ft2-private.h"

static void ludwig_font_ft2_class_init (LudwigFontFT2Class *klass);
static void ludwig_font_ft2_init (LudwigFontFT2 *group);

static void ludwig_font_ft2_destroy (GtkObject *object);

static LudwigFontGlyph *ludwig_font_ft2_get_glyph (LudwigVFont *vfont, gint code);
static ArtPoint *ludwig_font_ft2_kerning (LudwigVFont *vfont, gint c1, gint c2, ArtPoint * p);

static LudwigRFont *ludwig_font_ft2_get_rfont (LudwigFont *font, gdouble affine[]);

static LudwigFontClass *parent_class = NULL;

GtkType
ludwig_font_ft2_get_type (void)
{
	static GtkType group_type = 0;

	if (!group_type)
        {
		GtkTypeInfo group_info =
                {
			"LudwigFontFT2",
			sizeof (LudwigFontFT2),
			sizeof (LudwigFontFT2Class),
			(GtkClassInitFunc) ludwig_font_ft2_class_init,
			(GtkObjectInitFunc) ludwig_font_ft2_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};

		group_type = gtk_type_unique (ludwig_font_get_type (), &group_info);
	}

	return group_type;
}

static void
ludwig_font_ft2_class_init (LudwigFontFT2Class *klass)
{
	GtkObjectClass *object_class;
	LudwigVFontClass *vfont_class;
	LudwigFontClass *font_class;

	object_class = (GtkObjectClass *) klass;
	vfont_class = (LudwigVFontClass *) klass;
	font_class = (LudwigFontClass *) klass;

	parent_class = gtk_type_class (ludwig_font_get_type ());

	object_class->destroy = ludwig_font_ft2_destroy;

	vfont_class->get_glyph = ludwig_font_ft2_get_glyph;
	vfont_class->kerning = ludwig_font_ft2_kerning;

	font_class->get_rfont = ludwig_font_ft2_get_rfont;
}

static void
ludwig_font_ft2_init (LudwigFontFT2 * font)
{
	font->private = g_new0 (LudwigFontFT2Private, 1);
}

static void
ludwig_font_ft2_destroy (GtkObject *object)
{
	LudwigFontFT2 *font;

	font = (LudwigFontFT2 *) object;

	if (font->private)
        {
		LudwigFontFT2Private * priv;

		priv = font->private;

		FT_Done_Face (priv->ft_face);

		g_free (font->private);
		font->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* fixme: */

static LudwigFontGlyph *
ludwig_font_ft2_get_glyph (LudwigVFont * vfont, gint code)
{
	LudwigFontFT2 * ftfont;
	LudwigFontFT2Private * priv;
	LudwigFontGlyph * glyph;

	ftfont = (LudwigFontFT2 *) vfont;
	priv = ftfont->private;

	g_return_val_if_fail (code < priv->ft_face->num_glyphs, NULL);

	glyph = ludwig_font_glyph_ft2_new (vfont,
		priv->ft_face,
		code,
		LUDWIG_FONT_GLYPH_FT_TYPE_FONT);

	return glyph;
}

static ArtPoint *
ludwig_font_ft2_kerning (LudwigVFont * vfont, gint c1, gint c2, ArtPoint * p)
{
	LudwigFontFT2 * ftfont;
	LudwigFontFT2Private * priv;
	FT_Vector kv;

	ftfont = (LudwigFontFT2 *) vfont;
	priv = ftfont->private;

	FT_Get_Kerning (priv->ft_face, c1, c2, ft_kerning_default, &kv);

	p->x = (gdouble) kv.x * priv->dsize / priv->ft_face->units_per_EM;
	p->y = (gdouble) kv.y * priv->dsize / priv->ft_face->units_per_EM;

	return p;
}

/* fixme: */

static LudwigRFont *
ludwig_font_ft2_get_rfont (LudwigFont * font, gdouble affine[])
{
	LudwigFontFT2 * ftfont;
	LudwigFontFT2Private * priv;
	LudwigRFont * rfont;

	ftfont = (LudwigFontFT2 *) font;
	priv = ftfont->private;

	rfont = ludwig_rfont_ft2_new (font, affine);
	g_return_val_if_fail (rfont != NULL, NULL);

	return rfont;
}

/* Methods */

LudwigFont *
ludwig_font_ft2_new (LudwigFontFace * face, gdouble size, gdouble transform[])
{
	LudwigFontFaceFT2 * ft2face;
	LudwigFont * font;
	LudwigFontFT2 * ft2font;
	LudwigFontFT2Private * priv;
	FT_Error result;

	g_return_val_if_fail (size >= (1 / 72.0), NULL);

	ft2face = (LudwigFontFaceFT2 *) face;

	font = gtk_type_new (LUDWIG_TYPE_FONT_FT2);
	ft2font = (LudwigFontFT2 *) font;
	priv = ft2font->private;

	font->face = face;
	font->size = size;
	memcpy (font->affine, transform, 4 * sizeof (double));
	font->affine[4] = font->affine[5] = 0.0;

	/* fixme: add rounding remainder to transformation matrix */

	priv->size = (gint) (font->size * 64.0 + 0.5);
	priv->dsize = (gdouble) priv->size / 64.0;

	/* Find best approximation of resolution */

	priv->dresx = rint (hypot (transform[0], transform[1]) * 72.0);
	priv->dresy = rint (hypot (transform[2], transform[3]) * 72.0);
	priv->resx = (gint) priv->dresx;
	priv->resy = (gint) priv->dresy;

	/* Find remainder matrix */

	priv->drm[0] = transform[0] * 72.0 / priv->dresx;
	priv->drm[1] = transform[1] * 72.0 / priv->dresx;
	priv->drm[2] = transform[2] * 72.0 / priv->dresy;
	priv->drm[3] = transform[3] * 72.0 / priv->dresy;
	priv->drm[4] = priv->drm[5] = 0.0;

	priv->tt_m.xx = rint (priv->drm[0] * 65536.0);
	priv->tt_m.xy = rint (priv->drm[1] * 65536.0);
	priv->tt_m.yx = rint (priv->drm[2] * 65536.0);
	priv->tt_m.yy = rint (priv->drm[3] * 65536.0);

	/* Now we have font transformation matrixes set up :) */

	/* fixme: error checking */

	result = FT_New_Face (ludwig_font_get_ft2_library(),
		ft2face->path,
		ft2face->index,
		&priv->ft_face);

	FT_Set_Char_Size (priv->ft_face, priv->size, priv->size, priv->resx, priv->resy);
	FT_Select_Charmap (priv->ft_face, ft_encoding_unicode);

	return font;
}
