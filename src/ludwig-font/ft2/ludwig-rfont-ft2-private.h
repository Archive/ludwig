#ifndef LUDWIG_RFONT_FT2_PRIVATE_H
#define LUDWIG_RFONT_FT2_PRIVATE_H

#include <freetype/freetype.h>
#include <ludwig-font/ludwig-font.h>

struct _LudwigRFontFT2Private {

	/* FreeType Face object */

	FT_Face ft_face;

	/*
	 * Font size for FT2 library
	 */

	gdouble dsize;
	FT_F26Dot6 size;

	/*
	 * Resolution values for for FreeType library
	 * pixel_size = font->size * resolution / 72.0
	 */

	gdouble dresx, dresy;
	FT_UShort resx, resy;

	/*
	 * Remainder transformation matrix
	 * pixel_size * reminder = actual transform
	 */

	gdouble drm[6];
	FT_Matrix tt_m;
};

LudwigRFont *ludwig_rfont_ft2_new (LudwigFont *font, gdouble affine[]);

#endif
