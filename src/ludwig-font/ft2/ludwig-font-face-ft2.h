#ifndef LUDWIG_FONT_FACE_FT2_H
#define LUDWIG_FONT_FACE_FT2_H

/*
 * LudwigFontFaceFT
 *
 * Standard TTF file handler
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <freetype/freetype.h>
#include <ludwig-font/ludwig-font-face.h>

/* FontFaceFT2 */

#define LUDWIG_TYPE_FONT_FACE_FT2		(ludwig_font_face_ft2_get_type ())
#define LUDWIG_FONT_FACE_FT2(obj)		(GTK_CHECK_CAST ((obj), LUDWIG_TYPE_FONT_FACE_FT2, LudwigFontFaceFT2))
#define LUDWIG_FONT_FACE_FT2_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), LUDWIG_TYPE_FONT_FACE_FT2, LudwigFontFaceFT2Class))
#define LUDWIG_IS_FONT_FACE_FT2(obj)		(GTK_CHECK_TYPE ((obj), LUDWIG_TYPE_FONT_FACE_FT2))
#define LUDWIG_IS_FONT_FACE_FT2_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), LUDWIG_TYPE_FONT_FACE_FT2))

typedef struct _LudwigFontFaceFT2		LudwigFontFaceFT2;
typedef struct _LudwigFontFaceFT2Class		LudwigFontFaceFT2Class;

struct _LudwigFontFaceFT2
{
	LudwigFontFace      face;
	gchar             *path;
	gint               index;
	FT_Face            ft_face;
};

struct _LudwigFontFaceFT2Class
{
	LudwigFontFaceClass parent_class;
};

GtkType ludwig_font_face_ft2_get_type               (void);
LudwigFontFace *ludwig_font_face_real_new_from_file (const gchar *path,
                                                     const gchar *unused,
                                                     gint face);

#endif
