#ifndef LUDWIG_FONT_GLYPH_FT2_H
#define LUDWIG_FONT_GLYPH_FT2_H

/*
 * LudwigFontGlyphFT2
 *
 * FreeType glyph
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <ludwig-font/ludwig-font-glyph.h>

/* FontGlyphFT2 */

#define LUDWIG_TYPE_FONT_GLYPH_FT2		(ludwig_font_glyph_ft2_get_type ())
#define LUDWIG_FONT_GLYPH_FT2(obj)		(GTK_CHECK_CAST ((obj), LUDWIG_TYPE_FONT_GLYPH_FT2, LudwigFontGlyphFT2))
#define LUDWIG_FONT_GLYPH_FT2_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), LUDWIG_TYPE_FONT_GLYPH_FT2, LudwigFontGlyphFT2Class))
#define LUDWIG_IS_FONT_GLYPH_FT2(obj)		(GTK_CHECK_TYPE ((obj), LUDWIG_TYPE_FONT_GLYPH_FT2))
#define LUDWIG_IS_FONT_GLYPH_FT2_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), LUDWIG_TYPE_FONT_GLYPH_FT2))

typedef struct _LudwigFontGlyphFT2	LudwigFontGlyphFT2;
typedef struct _LudwigFontGlyphFT2Class	LudwigFontGlyphFT2Class;
typedef struct _LudwigFontGlyphFT2Private	LudwigFontGlyphFT2Private;

struct _LudwigFontGlyphFT2
{
	LudwigFontGlyph glyph;
	LudwigFontGlyphFT2Private *private;
};

struct _LudwigFontGlyphFT2Class
{
	LudwigFontGlyphClass parent_class;
};

GtkType ludwig_font_glyph_ft2_get_type (void);

#endif
