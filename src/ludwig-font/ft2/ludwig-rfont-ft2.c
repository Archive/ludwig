#define LUDWIG_RFONT_FT2_C

/*
 * LudwigRFontFT2
 *
 * Freetype rasterized font implementation
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <math.h>
#include <libart_lgpl/art_affine.h>
#include "gf-freetype2.h"
#include "ludwig-font-face-ft2.h"
#include "ludwig-font-ft2.h"
#include "ludwig-font-ft2-private.h"
#include "ludwig-rfont-ft2.h"
#include "ludwig-rfont-ft2-private.h"
#include "ludwig-font-glyph-ft2.h"
#include "ludwig-font-glyph-ft2-private.h"

static void ludwig_rfont_ft2_class_init (LudwigRFontFT2Class * klass);
static void ludwig_rfont_ft2_init (LudwigRFontFT2 * group);

static void ludwig_rfont_ft2_destroy (GtkObject * object);

static LudwigFontGlyph * ludwig_rfont_ft2_get_glyph (LudwigVFont * vfont, gint code);
static ArtPoint * ludwig_rfont_ft2_kerning (LudwigVFont * vfont, gint c1, gint c2, ArtPoint * p);

static LudwigRFontClass * parent_class = NULL;

GtkType
ludwig_rfont_ft2_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"LudwigRFontFT2",
			sizeof (LudwigRFontFT2),
			sizeof (LudwigRFontFT2Class),
			(GtkClassInitFunc) ludwig_rfont_ft2_class_init,
			(GtkObjectInitFunc) ludwig_rfont_ft2_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (ludwig_rfont_get_type (), &group_info);
	}
	return group_type;
}

static void
ludwig_rfont_ft2_class_init (LudwigRFontFT2Class * klass)
{
	GtkObjectClass * object_class;
	LudwigVFontClass * vfont_class;

	object_class = (GtkObjectClass *) klass;
	vfont_class = (LudwigVFontClass *) klass;

	parent_class = gtk_type_class (ludwig_rfont_get_type ());

	object_class->destroy = ludwig_rfont_ft2_destroy;

	vfont_class->get_glyph = ludwig_rfont_ft2_get_glyph;
	vfont_class->kerning = ludwig_rfont_ft2_kerning;
}

static void
ludwig_rfont_ft2_init (LudwigRFontFT2 * font)
{
	font->private = g_new0 (LudwigRFontFT2Private, 1);
}

static void
ludwig_rfont_ft2_destroy (GtkObject * object)
{
	LudwigRFontFT2 * font;

	font = (LudwigRFontFT2 *) object;

	if (font->private) {
		LudwigRFontFT2Private * priv;

		priv = font->private;

		FT_Done_Face (priv->ft_face);

		g_free (priv);
		font->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* fixme: */

static LudwigFontGlyph *
ludwig_rfont_ft2_get_glyph (LudwigVFont * vfont, gint code)
{
	LudwigRFontFT2 * rfontft2;
	LudwigRFontFT2Private * priv;
	LudwigFontGlyph * glyph;

	/* We can rely on having instance, hopefully :) */
	/* But we NEED dictionary still :( */
	/* And here we need grid-fitting too */

	rfontft2 = (LudwigRFontFT2 *) vfont;
	priv = rfontft2->private;

	g_return_val_if_fail (code < priv->ft_face->num_glyphs, NULL);

	glyph = ludwig_font_glyph_ft2_new (vfont,
		priv->ft_face,
		code,
		LUDWIG_FONT_GLYPH_FT_TYPE_RFONT);

	return glyph;
}

static ArtPoint *
ludwig_rfont_ft2_kerning (LudwigVFont * vfont, gint c1, gint c2, ArtPoint * p)
{
	LudwigRFontFT2 * rfontft2;
	LudwigRFontFT2Private * priv;
	FT_Vector kv;
	ArtPoint k;

	rfontft2 = (LudwigRFontFT2 *) vfont;
	priv = rfontft2->private;

	FT_Get_Kerning (priv->ft_face, c1, c2, ft_kerning_default, &kv);

	k.x = (gdouble) kv.x * priv->dsize * priv->dresx / 72.0 / priv->ft_face->units_per_EM;
	k.y = (gdouble) kv.y * priv->dsize * priv->dresx / 72.0 / priv->ft_face->units_per_EM;
	art_affine_point (p, &k, priv->drm);

	return p;
}

/* Methods */

LudwigRFont *
ludwig_rfont_ft2_new (LudwigFont *font, gdouble affine[])
{
	LudwigRFont * rfont;
	LudwigRFontFT2 * rfontft2;
	LudwigRFontFT2Private * priv;
	FT_Error result;

	rfontft2 = gtk_type_new (LUDWIG_TYPE_RFONT_FT2);
	rfont = (LudwigRFont *) rfontft2;
	priv = rfontft2->private;

	/* fixme: ref font */

	/* Fill device values in LudwigRFont struct */

	rfont->font = font;
	memcpy (rfont->affine, affine, 4 * sizeof (gdouble));
	rfont->affine[4] = rfont->affine[5] = 0.0;

	/* Fill FT2 device values in LudwigRFontFT2 struct */

	/* Find best approximation of font size */

	/* fixme: add rounding remainder to transformation matrix */

	priv->size = (gint) (font->size * 64.0 + 0.5);
	priv->dsize = (gdouble) priv->size / 64.0;

	/* Find best approximation of resolution */

	priv->dresx = rint (hypot (affine[0], affine[1]) * 72.0);
	priv->dresy = rint (hypot (affine[2], affine[3]) * 72.0);
	priv->resx = (gint) priv->dresx;
	priv->resy = (gint) priv->dresy;

	/* Find remainder matrix */

	priv->drm[0] = affine[0] * 72.0 / priv->dresx;
	priv->drm[1] = affine[1] * 72.0 / priv->dresx;
	priv->drm[2] = affine[2] * 72.0 / priv->dresy;
	priv->drm[3] = affine[3] * 72.0 / priv->dresy;
	priv->drm[4] = priv->drm[5] = 0.0;

	priv->tt_m.xx = rint (priv->drm[0] * 65536.0);
	priv->tt_m.xy = rint (priv->drm[1] * 65536.0);
	priv->tt_m.yx = rint (priv->drm[2] * 65536.0);
	priv->tt_m.yy = rint (priv->drm[3] * 65536.0);

	/* Now we have font transformation matrixes set up :) */

	/* fixme: error checking */

	result = FT_New_Face (ludwig_font_get_ft2_library(),
		((LudwigFontFaceFT2 *) font->face)->path,
		((LudwigFontFaceFT2 *) font->face)->index,
		&priv->ft_face);

	FT_Set_Char_Size (priv->ft_face, priv->size, priv->size, priv->resx, priv->resy);
	FT_Select_Charmap (priv->ft_face, ft_encoding_unicode);

	return rfont;
}


