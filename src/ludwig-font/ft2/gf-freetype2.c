#define GF_FREETYPE2_C

/*
 * FreeType library plug
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <string.h>
#include <unicode.h>
#include <libart_lgpl/art_affine.h>
#include "gf-freetype2.h"

#include <freetype/ttnameid.h>
#include <freetype/ftoutln.h>

static FT_Library library;
static gboolean   initialized;

#if 0
static gchar * gfft_strdup_mac_utf8 (FT_String * src, FT_UShort length, GFFT_Locale * loc);
static gchar * gfft_strdup_utf16_utf8 (FT_String * src, FT_UShort length, GFFT_Locale * loc);
#endif /* 0 */

const FT_Library *ludwig_font_get_ft2_library (void)
{
   return (FT_Library *) library;
}

gboolean ludwig_font_real_init_library(void)
{
	FT_Error result;

	if(initialized)
		return TRUE;

	result = FT_Init_FreeType (&library);
	if (result != FT_Err_Ok)
		return FALSE;

	initialized = TRUE;

	return TRUE;
}

#if 0
/* fixme: either implement or rethink this */

static GFFT_Locale gfl[] = {
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_DEFAULT, TT_MAC_LANGID_ENGLISH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_UNICODE_2_0, TT_MAC_LANGID_ENGLISH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_UNICODE_1_1, TT_MAC_LANGID_ENGLISH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_ISO_10646, TT_MAC_LANGID_ENGLISH},

	{TT_PLATFORM_MACINTOSH, TT_MAC_ID_ROMAN, TT_MAC_LANGID_ENGLISH},

	{TT_PLATFORM_MICROSOFT, TT_MS_ID_UNICODE_CS, TT_MS_LANGID_ENGLISH_UNITED_STATES}
};
gint gfl_length = (sizeof (gfl) / sizeof (gfl[0]));

static GFFT_Locale gfl_fr_FR[] = {
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_DEFAULT, TT_MAC_LANGID_FRENCH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_UNICODE_2_0, TT_MAC_LANGID_FRENCH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_UNICODE_1_1, TT_MAC_LANGID_FRENCH},
	{TT_PLATFORM_APPLE_UNICODE, TT_APPLE_ID_ISO_10646, TT_MAC_LANGID_FRENCH},

	{TT_PLATFORM_MACINTOSH, TT_MAC_ID_ROMAN, TT_MAC_LANGID_FRENCH},

	{TT_PLATFORM_MICROSOFT, TT_MS_ID_UNICODE_CS, TT_MS_LANGID_FRENCH_FRANCE}
};
gint gfl_fr_FR_length = (sizeof (gfl_fr_FR) / sizeof (gfl_fr_FR[0]));

const GSList *
gf_freetype_locale_preference_list (const gchar * language)
{
	static GSList * l = NULL;
	gint i;

	/* Fake locale preferences */

	if (strcmp (language, "fr_FR") == 0) {
		for (i = gfl_fr_FR_length - 1; i >= 0; i--) {
			l = g_slist_prepend (l, &gfl_fr_FR[i]);
		}
		return l;
	}

	if (l == NULL) {
		for (i = gfl_length - 1; i >= 0; i--) {
			l = g_slist_prepend (l, &gfl[i]);
		}
	}

	return l;
}

/* fixme: */

gchar *
gf_freetype_strdup_utf8 (FT_String * src, FT_UShort length, GFFT_Locale * loc)
{
	g_assert (loc != NULL);

	switch (loc->PlatformID) {
	case TT_PLATFORM_APPLE_UNICODE:
	case TT_PLATFORM_MICROSOFT:
	case TT_PLATFORM_ISO:
		return gfft_strdup_utf16_utf8 (src, length, loc);
		break;
	case TT_PLATFORM_MACINTOSH:
		return gfft_strdup_mac_utf8 (src, length, loc);
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	return NULL;
}

/* fixme: do real thing */

static gchar *
gfft_strdup_mac_utf8 (FT_String * src, FT_UShort length, GFFT_Locale * loc)
{
	gchar * dst;

	dst = g_new (gchar, (length + 1) * sizeof (gchar));
	memcpy (dst, src, length * sizeof (gchar));
	* (dst + length) = '\0';

	return dst;
}

/* fixme: */

static gchar *
gfft_strdup_utf16_utf8 (FT_String * src, FT_UShort length, GFFT_Locale * loc)
{
	static gboolean uni_initialized = FALSE;
	static unicode_iconv_t uiconv = NULL;
	gchar * tmp, * dst;
	const char * ip;
	char * op;
	size_t il, ol;

	if (!uni_initialized)
        {
		unicode_init ();
		uiconv = unicode_iconv_open ("UTF-8", "UTF-16");
		if (uiconv == (unicode_iconv_t) -1)
                {
			g_warning ("Error creating UTF-16 -> UTF-8 converter");
			return NULL;
		}

		uni_initialized = TRUE;
	}

	ip = src;
	il = length;

	tmp = g_new0 (gchar, length * 8 + 1);
	op = tmp;
	ol = length * 8;

	unicode_iconv (uiconv, &ip, &il, &op, &ol);

	dst = g_strdup (tmp);

	g_free (tmp);

	return dst;
}

/* Bpath methods */

static ArtBpath *
gfgft_moveto (ArtBpath * p, gdouble x, gdouble y)
{
	p->code = ART_MOVETO;
	p->x3 = x;
	p->y3 = y;

	return ++p;
}

static ArtBpath *
gfgft_lineto (ArtBpath * p, gdouble x, gdouble y)
{
	p->code = ART_LINETO;
	p->x3 = x;
	p->y3 = y;

	return ++p;
}

static ArtBpath *
gfgft_curveto (ArtBpath * p,
	gdouble sx, gdouble sy,
	gdouble midx, gdouble midy,
	gdouble x, gdouble y)
{
	gdouble x0, y0, x1, y1, x2, y2, x3, y3;
	gdouble mx, my;

	x0 = sx;
	y0 = sy;

	mx = midx;
	my = midy;

	x3 = x;
	y3 = y;

	x1 = x0 + 2 * (mx - x0) / 3;
	y1 = y0 + 2 * (my - y0) / 3;

	x2 = mx + (x3 - mx) / 3;
	y2 = my + (y3 - my) / 3;

	p->code = ART_CURVETO;
	p->x1 = x1;
	p->y1 = y1;
	p->x2 = x2;
	p->y2 = y2;
	p->x3 = x3;
	p->y3 = y3;

	return ++p;
}

static ArtBpath *
gfgft_end (ArtBpath * p)
{
	p->code = ART_END;

	return ++p;
}

/* 1 - on, 0 - off */

ArtBpath *
gf_freetype_bpath_from_outline (FT_Outline * ol, gdouble xscale, gdouble yscale)
{
	gint ctr, pt;
	gint idx;
	FT_Vector * v;
	ArtBpath * bp, * p;
	gint code, lastcode;
	gdouble x0, y0, startx, starty, lastx, lasty, midx, midy, x, y;

	g_assert (ol != NULL);

	bp = g_new (ArtBpath, ol->n_points * 2 + ol->n_contours + 1);

	idx = 0;
	p = bp;
	for (ctr = 0; ctr < ol->n_contours; ctr++)
        {
		v = ol->points + idx;
		x0 = startx = v->x * xscale;
		y0 = starty = v->y * yscale;
		p = gfgft_moveto (p, startx, starty);
		g_assert ((ol->flags[idx] & 0x1) != 0);
		lastcode = 1;
		lastx = startx;
		lasty = starty;
		idx++;
		for (pt = idx; pt <= ol->contours[ctr]; pt++)
                {
			v = ol->points + pt;
			code = ol->flags[pt];
			x = v->x * xscale;
			y = v->y * yscale;
			if (code != 0)
                        {
				if (lastcode == 1)
                                {
					/* Simple lineto */
					p = gfgft_lineto (p, x, y);
					startx = x;
					starty = y;
				}
                                else
                                {
					/* Curveto close */
					p = gfgft_curveto (p, startx, starty, lastx, lasty, x, y);
					startx = x;
					starty = y;
				}
			}
                        else
                        {
				if (lastcode == 1)
                                {
					/* Curveto open - nothing */
				}
                                else
                                {
					/* Midpoint, close, open */
					midx = (lastx + x) / 2;
					midy = (lasty + y) / 2;
					p = gfgft_curveto (p, startx, starty, lastx, lasty, midx, midy);
					startx = midx;
					starty = midy;
				}
			}

			lastcode = code;
			lastx = x;
			lasty = y;
		}

		if (lastcode == 1)
                {
			/* Simple lineto */
			p = gfgft_lineto (p, x0, y0);
		}
                else
                {
			/* Curveto close */
			p = gfgft_curveto (p, startx, starty, lastx, lasty, x0, y0);
		}

		idx = pt;
	}

	p = gfgft_end (p);
#if 0
	bp = g_renew (ArtBpath, bp, p - bp);
#endif
	return bp;
}
#endif /* 0 */

typedef struct
{
	ArtBpath *bp;
        gint start, end;
	gdouble *t;
} GFFT2OutlineData;

static int gfft2_move_to (FT_Vector *to, void *user)
{
	GFFT2OutlineData *od;
        ArtBpath *s;
        ArtPoint p;

	od = (GFFT2OutlineData *) user;

        s = &od->bp[od->end - 1];
        p.x = to->x * od->t[0] + to->y * od->t[2];
        p.y = to->x * od->t[1] + to->y * od->t[3];

        if ((p.x != s->x3) || (p.y != s->y3))
        {
                od->bp[od->end].code = ART_MOVETO;
                od->bp[od->end].x3 = to->x * od->t[0] + to->y * od->t[2];
                od->bp[od->end].y3 = to->x * od->t[1] + to->y * od->t[3];
                od->end++;
        }

	return 0;
}

static int gfft2_line_to (FT_Vector *to, void *user)
{
	GFFT2OutlineData *od;
        ArtBpath *s;
        ArtPoint  p;

	od = (GFFT2OutlineData *) user;

        s = &od->bp[od->end - 1];

        p.x = to->x * od->t[0] + to->y * od->t[2];
        p.y = to->x * od->t[1] + to->y * od->t[3];

        if ((p.x != s->x3) || (p.y != s->y3))
        {
                od->bp[od->end].code = ART_LINETO;
                od->bp[od->end].x3 = to->x * od->t[0] + to->y * od->t[2];
                od->bp[od->end].y3 = to->x * od->t[1] + to->y * od->t[3];
                od->end++;
        }

	return 0;
}

static int gfft2_conic_to (FT_Vector * control, FT_Vector * to, void * user)
{
	GFFT2OutlineData *od;
        ArtBpath *s, *e;
	ArtPoint c;

	od = (GFFT2OutlineData *) user;
        g_return_val_if_fail (od->end > 0, -1);

        s = &od->bp[od->end - 1];
        e = &od->bp[od->end];

        e->code = ART_CURVETO;

        c.x = control->x * od->t[0] + control->y * od->t[2];
        c.y = control->x * od->t[1] + control->y * od->t[3];
        e->x3 = to->x * od->t[0] + to->y * od->t[2];
        e->y3 = to->x * od->t[1] + to->y * od->t[3];

        od->bp[od->end].x1 = c.x - (c.x - s->x3) / 3;
        od->bp[od->end].y1 = c.y - (c.y - s->y3) / 3;
        od->bp[od->end].x2 = c.x + (e->x3 - c.x) / 3;
        od->bp[od->end].y2 = c.y + (e->y3 - c.y) / 3;
        od->end++;

	return 0;
}

static int gfft2_cubic_to (FT_Vector * control1, FT_Vector * control2, FT_Vector * to, void * user)
{
	GFFT2OutlineData *od;
	ArtPoint p;

	od = (GFFT2OutlineData *) user;

        od->bp[od->end].code = ART_CURVETO;
        od->bp[od->end].x1 = control1->x * od->t[0] + control1->y * od->t[2];
        od->bp[od->end].y1 = control1->y * od->t[1] + control1->y * od->t[3];
        od->bp[od->end].x2 = control2->x * od->t[0] + control2->y * od->t[2];
        od->bp[od->end].y2 = control2->y * od->t[1] + control2->y * od->t[3];
        od->bp[od->end].x3 = to->x * od->t[0] + to->y * od->t[2];
        od->bp[od->end].y3 = to->x * od->t[1] + to->y * od->t[3];
        od->end++;

	return 0;
}

FT_Outline_Funcs gfft2_outline_funcs =
{
	gfft2_move_to,
	gfft2_line_to,
	gfft2_conic_to,
	gfft2_cubic_to,
	0, 0
};

ArtBpath *
gf_freetype2_bpath_from_outline (FT_Outline * ol, gdouble transform[])
{
	GFFT2OutlineData od;
        int i;

	od.bp = g_new (ArtBpath, ol->n_points * 2 + ol->n_contours + 1);
        od.start = od.end = 0;
        od.t = transform;

	FT_Outline_Decompose (ol, &gfft2_outline_funcs, &od);

        od.bp[od.end].code = ART_END;

	return od.bp;
}
