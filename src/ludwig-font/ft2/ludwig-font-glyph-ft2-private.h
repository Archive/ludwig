#ifndef LUDWIG_FONT_GLYPH_FT2_PRIVATE_H
#define LUDWIG_FONT_GLYPH_FT2_PRIVATE_H

#include <glib.h>
#include <freetype/freetype.h>
#include <ludwig-font/ludwig-vfont.h>
#include <ludwig-font/ludwig-font-glyph.h>

typedef enum
{
	LUDWIG_FONT_GLYPH_FT_TYPE_NONE,
	LUDWIG_FONT_GLYPH_FT_TYPE_FACE,
	LUDWIG_FONT_GLYPH_FT_TYPE_FONT,
	LUDWIG_FONT_GLYPH_FT_TYPE_RFONT
} LudwigFontGlyphFTType;

struct _LudwigFontGlyphFT2Private
{
	LudwigFontGlyphFTType type;
	guint has_metrics : 1;
	guint err_metrics : 1;
	guint can_outline : 1;
	guint err_outline : 1;
	guint can_graymap : 1;
	guint err_graymap : 1;
#if 0
	FT_Glyph tt_glyph;
	FT_Big_Glyph_Metrics tt_bigmetrics;
#endif
	ArtDRect bbox;
	ArtPoint advance;
	ArtBpath * bpath;

	/*
	 * This is remainder matrix, transforming already scaled glyph
	 * to real raster (rotation etc.)
	 */

	LudwigFontGlyphGrayMap graymap;
	LudwigFontGlyphGdkMap gdkmap;
};

LudwigFontGlyph *ludwig_font_glyph_ft2_new (LudwigVFont *vfont, FT_Face face, gint code, LudwigFontGlyphFTType type);

#endif
