#define LUDWIG_FONT_FACE_FT2_C

/*
 * LudwigFontFaceFT2
 *
 * Standard TTF font file handler
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <string.h>
#include <freetype/freetype.h>
#include "gf-freetype2.h"
#include <ludwig-font/ludwig-font-face.h>
#include <ludwig-font/ludwig-font-face-private.h>
#include "ludwig-font-face-ft2.h"
#include "ludwig-font-ft2.h"
#include "ludwig-font-ft2-private.h"
#include "ludwig-font-glyph-ft2.h"
#include "ludwig-font-glyph-ft2-private.h"

static void gft_class_init (LudwigFontFaceFT2Class * klass);
static void gft_init (LudwigFontFaceFT2 * face);

static void gft_destroy (GtkObject * object);

/* LudwigVFont methods */

static LudwigFontGlyph * gft_get_glyph (LudwigVFont * vfont, gint code);

/* LudwigFontFace methods */

static LudwigFontFaceDesc * gft_description (LudwigFontFace *face, const gchar *language);
static LudwigFont * gft_get_font (LudwigFontFace *face, gdouble size, gdouble transform[]);
static gboolean gft_set_encoding (LudwigFontFace *face, gint id, LudwigFontEncoding encoding);
static gint gft_lookup (LudwigFontFace *face, gint encoding, gint code);

static GHashTable * pathdict = NULL;
static LudwigFontFaceClass * parent_class = NULL;

GtkType
ludwig_font_face_ft2_get_type (void)
{
	static GtkType face_type = 0;
	if (!face_type)
	{
		GtkTypeInfo face_info =
		{
			"LudwigFontFaceFT2",
			sizeof (LudwigFontFaceFT2),
			sizeof (LudwigFontFaceFT2Class),
			(GtkClassInitFunc) gft_class_init,
			(GtkObjectInitFunc) gft_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};

		face_type = gtk_type_unique (ludwig_font_face_get_type (), &face_info);
	}

	return face_type;
}

static void
gft_class_init (LudwigFontFaceFT2Class * klass)
{
	GtkObjectClass * object_class;
	LudwigVFontClass * vfont_class;
	LudwigFontFaceClass * face_class;

	object_class = (GtkObjectClass *) klass;
	vfont_class = (LudwigVFontClass *) klass;
	face_class = (LudwigFontFaceClass *) klass;

	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = gft_destroy;

	vfont_class->get_glyph = gft_get_glyph;

	face_class->description = gft_description;
	face_class->get_font = gft_get_font;
	face_class->set_encoding = gft_set_encoding;
	face_class->lookup = gft_lookup;
}

static void
gft_init (LudwigFontFaceFT2 * face)
{
	face->path = NULL;
}

static void
gft_destroy (GtkObject *object)
{
	LudwigFontFaceFT2 *face;

	face = (LudwigFontFaceFT2 *) object;

	if (face->path)
	{
		g_hash_table_remove (pathdict, face->path);
		g_free (face->path);
	}

	FT_Done_Face (face->ft_face);

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* LudwigVFont methods */

static LudwigFontGlyph *
gft_get_glyph (LudwigVFont *vfont, gint code)
{
	LudwigFontFaceFT2 * ftface;
	LudwigFontGlyph * glyph;

	ftface = (LudwigFontFaceFT2 *) vfont;

	g_return_val_if_fail (code < ftface->ft_face->num_glyphs, NULL);

	glyph = ludwig_font_glyph_ft2_new (vfont,
		ftface->ft_face,
		code,
		LUDWIG_FONT_GLYPH_FT_TYPE_FACE);

	return glyph;
}

static LudwigFontFaceDesc *
gft_description (LudwigFontFace * face, const gchar * language)
{
	LudwigFontFaceFT2 * ftface;
	LudwigFontFaceDesc * desc;

	ftface = (LudwigFontFaceFT2 *) face;

	desc = g_new0 (LudwigFontFaceDesc, 1);

	desc->language = g_strdup (language);
	desc->family = g_strdup (ftface->ft_face->family_name);
	desc->species = g_strdup (ftface->ft_face->style_name);
	desc->name = g_strconcat (desc->family, desc->species, NULL);

	return desc;
}

static LudwigFont *
gft_get_font (LudwigFontFace * face, gdouble size, gdouble transform[])
{
	LudwigFontFaceFT2 * ftface;
	LudwigFont * font;

	ftface = (LudwigFontFaceFT2 *) face;

	/* fixme: implement caching */

	font = ludwig_font_ft2_new (face, size, transform);
	g_return_val_if_fail (font != NULL, NULL);

	return font;
}

/* NB! HIGHLY EXPERIMENTAL */

static LudwigFontEncoding
translate_encoding (FT_Encoding charmap)
{
	switch (charmap)
	{
		case ft_encoding_symbol:
			return LUDWIG_FONT_ENCODING_SYMBOL;
			break;
		case ft_encoding_unicode:
			return LUDWIG_FONT_ENCODING_UNICODE;
			break;
		case ft_encoding_latin_2:
			return LUDWIG_FONT_ENCODING_LATIN2;
			break;
		case ft_encoding_sjis:
			return LUDWIG_FONT_ENCODING_SJIS;
			break;
		case ft_encoding_gb2312:
			return LUDWIG_FONT_ENCODING_GB2312;
			break;
		case ft_encoding_big5:
			return LUDWIG_FONT_ENCODING_BIG5;
			break;
		case ft_encoding_wansung:
			return LUDWIG_FONT_ENCODING_WANSUNG;
			break;
		case ft_encoding_johab:
			return LUDWIG_FONT_ENCODING_JOHAB;
			break;
		case ft_encoding_adobe_standard:
			return LUDWIG_FONT_ENCODING_ADOBE_STANDARD;
			break;
		case ft_encoding_adobe_expert:
			return LUDWIG_FONT_ENCODING_ADOBE_EXPERT;
			break;
		case ft_encoding_adobe_custom:
			return LUDWIG_FONT_ENCODING_ADOBE_CUSTOM;
			break;
		case ft_encoding_apple_roman:
			return LUDWIG_FONT_ENCODING_APPLE_ROMAN;
			break;
		default:
			return LUDWIG_FONT_ENCODING_NONE;
			break;
	}
}

static FT_Encoding
translate_encoding_ludwig_to_ft (LudwigFontEncoding encoding)
{
	switch (encoding)
	{
		case LUDWIG_FONT_ENCODING_SYMBOL:
			return ft_encoding_symbol;
			break;
		case LUDWIG_FONT_ENCODING_UNICODE:
			return ft_encoding_unicode;
			break;
		case LUDWIG_FONT_ENCODING_LATIN2:
			return ft_encoding_latin_2;
			break;
		case LUDWIG_FONT_ENCODING_SJIS:
			return ft_encoding_sjis;
			break;
		case LUDWIG_FONT_ENCODING_GB2312:
			return ft_encoding_gb2312;
			break;
		case LUDWIG_FONT_ENCODING_BIG5:
			return ft_encoding_big5;
			break;
		case LUDWIG_FONT_ENCODING_WANSUNG:
			return ft_encoding_wansung;
			break;
		case LUDWIG_FONT_ENCODING_JOHAB:
			return ft_encoding_johab;
			break;
		case LUDWIG_FONT_ENCODING_ADOBE_STANDARD:
			return ft_encoding_adobe_standard;
			break;
		case LUDWIG_FONT_ENCODING_ADOBE_EXPERT:
			return ft_encoding_adobe_expert;
			break;
		case LUDWIG_FONT_ENCODING_ADOBE_CUSTOM:
			return ft_encoding_adobe_custom;
			break;
		case LUDWIG_FONT_ENCODING_APPLE_ROMAN:
			return ft_encoding_apple_roman;
			break;
		default:
			return ft_encoding_none;
	}
}

static void
gft_init_encodings (LudwigFontFaceFT2 *ftface)
{
	LudwigFontFace *face;
	FT_Encoding ft_encoding;
	LudwigFontEncoding encoding;
	gint i;

	g_return_if_fail (ftface->ft_face->num_charmaps != 0);

	face = LUDWIG_FONT_FACE (ftface);

	face->encodings = g_new0 (LudwigFontEncoding, ftface->ft_face->num_charmaps);

	for (i = 0; i < ftface->ft_face->num_charmaps; i++)
	{
		encoding = translate_encoding (ftface->ft_face->charmaps[i]->encoding);
		if (encoding != LUDWIG_FONT_ENCODING_NONE)
			face->encodings[i] = encoding;
	}
}

static gboolean
gft_set_encoding (LudwigFontFace *face, gint id, LudwigFontEncoding encoding)
{
	LudwigFontFaceFT2 *ftface;
	FT_Encoding ft_encoding;
	gint i, j;

	ftface = LUDWIG_FONT_FACE_FT2 (face);
	if (!face->encodings)
		gft_init_encodings (ftface);

	for (i = 0; i < sizeof (face->encodings); i++)
	{
		if (face->encodings[i] == encoding)
		{
			ft_encoding = translate_encoding_ludwig_to_ft (encoding);

			for (j = 0; j < ftface->ft_face->num_charmaps; j++)
			{
				if (ftface->ft_face->charmaps[j]->encoding == ft_encoding)
				{
					FT_Set_Charmap (ftface->ft_face,
							ftface->ft_face->charmaps[j]);
				}
			}

			face->encoding = encoding;
			return TRUE;
		}
	}

	ft_encoding = translate_encoding_ludwig_to_ft (encoding);

	return TRUE;
}

/* fixme: API test */

static gint
gft_lookup (LudwigFontFace * face, gint encoding, gint code)
{
	LudwigFontFaceFT2 * ftface;
	FT_UInt index;

	ftface = (LudwigFontFaceFT2 *) face;

	index = FT_Get_Char_Index (ftface->ft_face, code);

	return index;
}

/* Private & temporary methods */

LudwigFontFace *
ludwig_font_face_real_new_from_file (const gchar *path,
                                    const gchar *unused,
                                    gint index)
{
	LudwigFontFaceFT2 *face;
	FT_Face ft_face;
	FT_Error result;

	g_return_val_if_fail (path != NULL, NULL);
	g_return_val_if_fail (index == 0, NULL);

	/* Test, if given path is already loaded */

	if (pathdict == NULL) {
		pathdict = g_hash_table_new (g_str_hash, g_str_equal);
	}

	face = g_hash_table_lookup (pathdict, path);

	if (face != NULL)
	{
		g_assert (LUDWIG_IS_FONT_FACE_FT2 (face));
		ludwig_font_face_ref ((LudwigFontFace *) face);
		return (LudwigFontFace *) face;
	}

	/* Ensure FreeType engine is loaded */

	ludwig_font_real_init_library ();

	/* Open FreeType face handle */

	result = FT_New_Face (ludwig_font_get_ft2_library(),
                              path,
                              index,
                              &ft_face);

	if (result != FT_Err_Ok)
	{
		g_warning ("Cannot open typeface file %s", path);
		return NULL;
	}

	/* Set attributes */

	FT_Set_Char_Size (ft_face, 6400, 6400, 720, 720);
	FT_Select_Charmap (ft_face, ft_encoding_unicode);

	/* Create Ludwig face object */

	face = gtk_type_new (LUDWIG_TYPE_FONT_FACE_FT2);

	face->ft_face = ft_face;
	face->path = g_strdup (path);
	face->index = index;

	g_hash_table_insert (pathdict, face->path, face);

	/* fixme: register loaded face */
	/* fixme: handle collections */
	/* fixme: close face on destroy */
	/* fixme: kill glyphs on destroy */

	return (LudwigFontFace *) face;
}
