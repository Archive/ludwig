#ifndef LUDWIG_FONT_FT2_H
#define LUDWIG_FONT_FT2_H

/*
 * LudwigFontFT2
 *
 * Freetype font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <ludwig-font/ludwig-font.h>

#define LUDWIG_TYPE_FONT_FT2		(ludwig_font_ft2_get_type ())
#define LUDWIG_FONT_FT2(obj)		(GTK_CHECK_CAST ((obj), LUDWIG_TYPE_FONT_FT2, LudwigFontFT2))
#define LUDWIG_FONT_FT2_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), LUDWIG_TYPE_FONT_FT2, LudwigFontFT2Class))
#define LUDWIG_IS_FONT_FT2(obj)		(GTK_CHECK_TYPE ((obj), LUDWIG_TYPE_FONT_FT2))
#define LUDWIG_IS_FONT_FT2_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), LUDWIG_TYPE_FONT_FT2))

typedef struct _LudwigFontFT2		LudwigFontFT2;
typedef struct _LudwigFontFT2Class	LudwigFontFT2Class;
typedef struct _LudwigFontFT2Private	LudwigFontFT2Private;

struct _LudwigFontFT2
{
	LudwigFont font;
	LudwigFontFT2Private *private;
};

struct _LudwigFontFT2Class
{
	LudwigFontClass parent_class;
};

GtkType ludwig_font_ft2_get_type (void);

#endif
