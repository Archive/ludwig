#define LUDWIG_FONTGLYPH_FT2_C

/*
 * LudwigFontGlyphFT2
 *
 * FreeType glyph
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <libart_lgpl/art_affine.h>
#include "gf-freetype2.h"
#include "ludwig-font-glyph-ft2.h"
#include "ludwig-font-glyph-ft2-private.h"
#include "ludwig-font-face-ft2.h"
#include "ludwig-font-ft2.h"
#include "ludwig-font-ft2-private.h"
#include "ludwig-rfont-ft2.h"
#include "ludwig-rfont-ft2-private.h"

static void ludwig_font_glyph_ft2_class_init (LudwigFontGlyphFT2Class *klass);
static void ludwig_font_glyph_ft2_init (LudwigFontGlyphFT2 *glyph);

static void ludwig_font_glyph_ft2_destroy (GtkObject *object);

static const ArtBpath *gfgft2_bpath (LudwigFontGlyph *glyph, gboolean with_transform);
static void gfgft2_metrics (LudwigFontGlyph *glyph, ArtDRect *bbox, ArtPoint *a);
static const LudwigFontGlyphGrayMap *gfgft2_graymap (LudwigFontGlyph *glyph);

/* Private */

static FT_Face gfgft2_ft_face (LudwigFontGlyphFT2 *glyph);
static void gfgft2_transform (LudwigFontGlyphFT2 *glyph, gdouble transform[]);

static LudwigFontGlyphClass *parent_class = NULL;

GtkType
ludwig_font_glyph_ft2_get_type (void)
{
	static GtkType glyph_type = 0;

	if (!glyph_type)
        {
		GtkTypeInfo glyph_info =
                {
			"LudwigFontGlyphFT2",
			sizeof (LudwigFontGlyphFT2),
			sizeof (LudwigFontGlyphFT2Class),
			(GtkClassInitFunc) ludwig_font_glyph_ft2_class_init,
			(GtkObjectInitFunc) ludwig_font_glyph_ft2_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};

		glyph_type = gtk_type_unique (ludwig_font_glyph_get_type (), &glyph_info);
	}

	return glyph_type;
}

static void
ludwig_font_glyph_ft2_class_init (LudwigFontGlyphFT2Class *klass)
{
	GtkObjectClass *object_class;
	LudwigFontGlyphClass *glyph_class;

	object_class = (GtkObjectClass *) klass;
	glyph_class = (LudwigFontGlyphClass *) klass;

	parent_class = gtk_type_class (ludwig_font_glyph_get_type ());

	object_class->destroy = ludwig_font_glyph_ft2_destroy;

	glyph_class->bpath = gfgft2_bpath;
	glyph_class->metrics = gfgft2_metrics;
	glyph_class->graymap = gfgft2_graymap;
}

static void
ludwig_font_glyph_ft2_init (LudwigFontGlyphFT2 *glyph)
{
	glyph->private = g_new0 (LudwigFontGlyphFT2Private, 1);
}

static void
ludwig_font_glyph_ft2_destroy (GtkObject *object)
{
	LudwigFontGlyphFT2 *glyph;

	glyph = (LudwigFontGlyphFT2 *) object;

	/* fixme: */

	if (glyph->private)
        {
		LudwigFontGlyphFT2Private *priv;

		priv = glyph->private;

		if (priv->bpath) g_free (priv->bpath);

		g_free (glyph->private);
		glyph->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

static const ArtBpath *
gfgft2_bpath (LudwigFontGlyph *glyph, gboolean invert)
{
	LudwigFontGlyphFT2 *gft2;
	LudwigFontGlyphFT2Private *priv;

	gft2 = (LudwigFontGlyphFT2 *) glyph;
	priv = gft2->private;

	if (priv->err_outline)
                return NULL;

	if (priv->bpath == NULL)
        {
		FT_Face ft_face;
		gdouble transform[6];

		ft_face = gfgft2_ft_face (gft2);
		g_assert (ft_face != NULL);

		switch (priv->type)
                {
		case LUDWIG_FONT_GLYPH_FT_TYPE_FACE:
			FT_Load_Glyph (ft_face, glyph->code,
				FT_LOAD_NO_SCALE | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP);
			gfgft2_transform (gft2, transform);
			break;
		case LUDWIG_FONT_GLYPH_FT_TYPE_FONT:
			FT_Load_Glyph (ft_face, glyph->code,
				FT_LOAD_NO_BITMAP);
			gfgft2_transform (gft2, transform);
			break;
		case LUDWIG_FONT_GLYPH_FT_TYPE_RFONT:
			FT_Load_Glyph (ft_face, glyph->code,
				FT_LOAD_NO_BITMAP);
			gfgft2_transform (gft2, transform);
			break;
		default:
			g_assert_not_reached ();
			break;
		}

		if (invert == TRUE)
			transform[3] = 0.0 - transform[3];

		priv->bpath = gf_freetype2_bpath_from_outline (&ft_face->glyph->outline, transform);

		if (!priv->bpath)
                        priv->err_outline = TRUE;
	}

	return priv->bpath;
}

static void
gfgft2_metrics (LudwigFontGlyph *glyph, ArtDRect *bbox, ArtPoint *a)
{
	LudwigFontGlyphFT2 * gft2;
	LudwigFontGlyphFT2Private * priv;

	gft2 = (LudwigFontGlyphFT2 *) glyph;
	priv = gft2->private;

	if (!priv->has_metrics)
        {
		FT_Face ft_face;
		gdouble transform[6];
		ArtPoint p0, p1, p;

		ft_face = gfgft2_ft_face (gft2);
		g_assert (ft_face != NULL);

		switch (priv->type)
                {
		case LUDWIG_FONT_GLYPH_FT_TYPE_FACE:
			FT_Load_Glyph (ft_face, glyph->code,
				FT_LOAD_NO_SCALE | FT_LOAD_NO_HINTING | FT_LOAD_NO_BITMAP);
			gfgft2_transform (gft2, transform);
			break;
		case LUDWIG_FONT_GLYPH_FT_TYPE_FONT:
			FT_Load_Glyph (ft_face, glyph->code,
				FT_LOAD_NO_BITMAP);
			gfgft2_transform (gft2, transform);
			break;
		case LUDWIG_FONT_GLYPH_FT_TYPE_RFONT:
			FT_Load_Glyph (ft_face, glyph->code,
				FT_LOAD_NO_BITMAP);
			gfgft2_transform (gft2, transform);
			break;
		default:
			g_assert_not_reached ();
			break;
		}

		p0.x = -ft_face->glyph->metrics.horiBearingX;
		p0.y = ft_face->glyph->metrics.horiBearingY - ft_face->glyph->metrics.height;
		p1.x = ft_face->glyph->metrics.width - ft_face->glyph->metrics.horiBearingX;
		p1.y = ft_face->glyph->metrics.horiBearingY;
		art_affine_point (&p0, &p0, transform);
		art_affine_point (&p1, &p1, transform);
		priv->bbox.x0 = MIN (p0.x, p1.x);
		priv->bbox.y0 = MIN (p0.y, p1.y);
		priv->bbox.x1 = MAX (p0.x, p1.x);
		priv->bbox.y1 = MAX (p0.y, p1.y);
		p.x = ft_face->glyph->metrics.horiAdvance;
		p.y = 0.0;
		art_affine_point (&priv->advance, &p, transform);
		priv->has_metrics = TRUE;
	}

	* bbox = priv->bbox;
	* a = priv->advance;
}

static const LudwigFontGlyphGrayMap *
gfgft2_graymap (LudwigFontGlyph *glyph)
{
	LudwigFontGlyphFT2 *gft2;
	LudwigFontGlyphFT2Private *priv;
	LudwigRFontFT2 *rfont;
	LudwigRFontFT2Private *rpriv;

	gft2 = (LudwigFontGlyphFT2 *) glyph;
	priv = gft2->private;

	if (priv->err_graymap)
                return NULL;
	if (priv->type != LUDWIG_FONT_GLYPH_FT_TYPE_RFONT)
                return NULL;

	rfont = (LudwigRFontFT2 *) glyph->vfont;
	rpriv = rfont->private;

	if (priv->graymap.pixels == NULL)
        {
		FT_Outline ol;
		FT_BBox cb;
		FT_Bitmap bm;
		gint x0, y0, x1, y1, w, h;

		/* Get outline */

		FT_Load_Glyph (rpriv->ft_face, glyph->code, FT_LOAD_NO_BITMAP);

		FT_Outline_New (ludwig_font_get_ft2_library(),
			rpriv->ft_face->glyph->outline.n_points,
			rpriv->ft_face->glyph->outline.n_contours,
			&ol);

		FT_Outline_Copy (&rpriv->ft_face->glyph->outline, &ol);

		/* Transform outline with remainder matrix */

		FT_Outline_Transform (&ol, &rpriv->tt_m);

		/* Find new bbox */

		FT_Outline_Get_CBox (&ol, &cb);

		/* Get outline bbox in pixels */

		x0 = (cb.xMin & -64) >> 6;
		y0 = (cb.yMin & -64) >> 6;
		x1 = (cb.xMax & -64) >> 6;
		y1 = (cb.yMax & -64) >> 6;

		/* Blah blah - setup raster map */

		w = ((x1 - x0) + 4) & 0xfffffffc;
		h = (y1 - y0) + 1;

		priv->graymap.pixels = g_new0 (gchar, w * h);
		priv->graymap.width = w;
		priv->graymap.height = h;
		priv->graymap.rowstride = w;
		priv->graymap.x0 = x0;
		priv->graymap.y0 = y0;

		/* Blah blah - setup FreeType raster map */

		bm.rows = h;
		bm.width = w;
		bm.pitch = -w;
		bm.buffer = priv->graymap.pixels;
		bm.num_grays = 256;
		bm.pixel_mode = ft_pixel_mode_grays;
		bm.palette_mode = 0;
		bm.palette = NULL;

		/* Blah, blah - translate outline to bbox */

		FT_Outline_Translate (&ol, (-x0) * 64, (-y0) * 64);

		FT_Outline_Get_Bitmap (ludwig_font_get_ft2_library(),
                                       &ol, &bm);

		FT_Outline_Done (ludwig_font_get_ft2_library(), &ol);
	}

	return &priv->graymap;
}

/* Constructor */

LudwigFontGlyph *
ludwig_font_glyph_ft2_new (LudwigVFont * vfont, FT_Face face, gint code, LudwigFontGlyphFTType type)
{
	LudwigFontGlyphFT2 * glyph;

	glyph = gtk_type_new (LUDWIG_TYPE_FONT_GLYPH_FT2);

	/* fixme: */

	glyph->glyph.vfont = vfont;
	glyph->glyph.code = code;

	glyph->private->type = type;

	glyph->private->can_outline = TRUE;
	glyph->private->can_graymap = TRUE;

#if 0
	result = TT_New_Glyph (face, &glyph->private->tt_glyph);
	g_return_val_if_fail (result == TT_Err_Ok, NULL);

	/* fixme: use font units for face glyphs */

	flags = 0;

	switch (type)
        {
	case LUDWIG_FONT_GLYPH_FT2_TYPE_FACE:
		flags = 0;
		break;
	case LUDWIG_FONT_GLYPH_FT2_TYPE_FONT:
	case LUDWIG_FONT_GLYPH_FT2_TYPE_RFONT:
		flags = TTLOAD_SCALE_GLYPH | TTLOAD_HINT_GLYPH;
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	result = TT_Load_Glyph (inst, glyph->private->tt_glyph, code, flags);
	g_return_val_if_fail (result == TT_Err_Ok, NULL);

	TT_Get_Glyph_Big_Metrics (glyph->private->tt_glyph, &glyph->private->tt_bigmetrics);
#endif

	return (LudwigFontGlyph *) glyph;
}

/* Private */

static FT_Face
gfgft2_ft_face (LudwigFontGlyphFT2 *glyph)
{
	switch (glyph->private->type)
        {
	case LUDWIG_FONT_GLYPH_FT_TYPE_FACE:
		return ((LudwigFontFaceFT2 *)glyph->glyph.vfont)->ft_face;
	case LUDWIG_FONT_GLYPH_FT_TYPE_FONT:
		return ((LudwigFontFT2 *)glyph->glyph.vfont)->private->ft_face;
	case LUDWIG_FONT_GLYPH_FT_TYPE_RFONT:
		return ((LudwigRFontFT2 *)glyph->glyph.vfont)->private->ft_face;
	default:
		g_assert_not_reached ();
		break;
	}

	return NULL;
}

static void
gfgft2_transform (LudwigFontGlyphFT2 *glyph, gdouble transform[])
{
	FT_Face ft_face;
	gdouble ds[] = {1 / 64.0, 0.0, 0.0, 1 / 64.0, 0.0, 0.0};

	switch (glyph->private->type)
        {
	case LUDWIG_FONT_GLYPH_FT_TYPE_FACE:
		ft_face = ((LudwigFontFaceFT2 *) glyph->glyph.vfont)->ft_face;
		art_affine_scale (transform,
			1000.0 / ft_face->units_per_EM,
			1000.0 / ft_face->units_per_EM);
		break;
	case LUDWIG_FONT_GLYPH_FT_TYPE_FONT:
#if 0
		art_affine_multiply (transform, ds,
			((LudwigFontFT2 *) glyph->glyph.vfont)->private->drm);
#endif
		art_affine_scale (transform,
			72.0 / 64.0 / ((LudwigFontFT2 *) glyph->glyph.vfont)->private->dresx,
			72.0 / 64.0 / ((LudwigFontFT2 *) glyph->glyph.vfont)->private->dresy);
		break;
	case LUDWIG_FONT_GLYPH_FT_TYPE_RFONT:
		art_affine_multiply (transform, ds,
			((LudwigRFontFT2 *) glyph->glyph.vfont)->private->drm);
		break;
	default:
		g_assert_not_reached ();
		break;
	}
}


