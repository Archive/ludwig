#ifndef LUDWIG_RFONT_FT2_H
#define LUDWIG_RFONT_FT2_H

/*
 * LudwigRFontFT2
 *
 * Freetype rasterized font implementation
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <ludwig-font/ludwig-rfont.h>

#define LUDWIG_TYPE_RFONT_FT2		 (ludwig_rfont_ft2_get_type ())
#define LUDWIG_RFONT_FT2(obj)		 (GTK_CHECK_CAST ((obj), LUDWIG_TYPE_RFONT_FT2, LudwigRFontFT2))
#define LUDWIG_RFONT_FT2_CLASS(klass)	 (GTK_CHECK_CLASS_CAST ((klass), LUDWIG_TYPE_RFONT_FT2, LudwigRFontFT2Class))
#define LUDWIG_IS_RFONT_FT2(obj)	 (GTK_CHECK_TYPE ((obj), LUDWIG_TYPE_RFONT_FT2))
#define LUDWIG_IS_RFONT_FT2_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), LUDWIG_TYPE_RFONT_FT2))

typedef struct _LudwigRFontFT2		LudwigRFontFT2;
typedef struct _LudwigRFontFT2Class	LudwigRFontFT2Class;
typedef struct _LudwigRFontFT2Private	LudwigRFontFT2Private;

struct _LudwigRFontFT2
{
	LudwigRFont rfont;
	LudwigRFontFT2Private *private;
};

struct _LudwigRFontFT2Class
{
	LudwigRFontClass parent_class;
};

GtkType ludwig_rfont_ft2_get_type (void);

#endif
