#ifndef LUDWIG_FONT_FT2_PRIVATE_H
#define LUDWIG_FONT_FT2_PRIVATE_H

#include <freetype/freetype.h>
#include <ludwig-font/ludwig-font-face.h>

struct _LudwigFontFT2Private
{

	/* FreeType Face object */

	FT_Face ft_face;

	/*
	 * Font size for FT2 library
	 */

	gdouble dsize;
	FT_F26Dot6 size;

	/*
	 * Resolution values for FreeType library
	 * pixels_size = font_size * resolution / 72.0
	 */

	gdouble dresx, dresy;
	FT_UShort resx, resy;

	/*
	 * Remainder transformation matrix
	 * pixel_size * reminder = actual transform
	 */

	gdouble drm[6];
	FT_Matrix tt_m;
};

#define GF_FONT_FT2_FT_FACE(f) (((LudwigFontFaceFT2 *)((LudwigFont *) f)->face)->private->ft_face)

LudwigFont *ludwig_font_ft2_new (LudwigFontFace *face, gdouble size, gdouble transform[]);

#endif
