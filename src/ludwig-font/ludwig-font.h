#ifndef __LUDWIG_FONT_H__
#define __LUDWIG_FONT_H__

/*
 * LudwigFont
 *
 * Base class for user visible font
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#define LUDWIG_TYPE_FONT	    (ludwig_font_get_type ())
#define LUDWIG_FONT(obj)	    (GTK_CHECK_CAST ((obj), LUDWIG_TYPE_FONT, LudwigFont))
#define LUDWIG_FONT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), LUDWIG_TYPE_FONT, LudwigFontClass))
#define LUDWIG_IS_FONT(obj)	    (GTK_CHECK_TYPE ((obj), LUDWIG_TYPE_FONT))
#define LUDWIG_IS_FONT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), LUDWIG_TYPE_FONT))
#define LUDWIG_FONT_GET_CLASS(o)    ((LudwigFontClass *)(((GtkObject *) o)->klass))

typedef struct _LudwigFont	LudwigFont;
typedef struct _LudwigFontClass	LudwigFontClass;

#include "ludwig-font-family.h"
#include "ludwig-vfont.h"
#include "ludwig-font-face.h"
#include "ludwig-rfont.h"

struct _LudwigFont
{
	LudwigVFont vfont;
	LudwigFontFace *face;
	GSList *RFonts;
	gdouble size;
	gdouble affine[6];
};

struct _LudwigFontClass
{
	LudwigVFontClass parent_class;

	LudwigRFont * (* get_rfont) (LudwigFont *font, gdouble affine[]);
};

/* This should be in a better place.. */
gboolean ludwig_font_init_library (void);
GtkType  ludwig_font_get_type     (void);

#define  ludwig_font_ref(f)   gtk_object_ref (GTK_OBJECT (f))
#define  ludwig_font_unref(f) gtk_object_unref (GTK_OBJECT (f))

/* Methods */

const LudwigFontFamily *ludwig_font_get_family (const LudwigFont *font);

/* These are UTF-8 and try to return name in current LC_MESSAGES locale */

const gchar *ludwig_font_get_family_name    (LudwigFont *font);
const gchar *ludwig_font_get_type_name      (LudwigFont *font);
const gchar *ludwig_font_get_canonical_name (LudwigFont *font);

/* This is ASCII (and not localized :) */
const gchar *ludwig_font_get_ps_name (const LudwigFont *font);

/* Get Rasterized font handler, given base -> device transformation */
LudwigRFont *ludwig_font_get_rfont   (LudwigFont *font, gdouble affine[]);

#endif
