#define LUDWIG_FONT_GLYPH_C

/*
 * LudwigFontGlyph
 *
 * Abstract base class for separate glyphs
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include "ludwig-font-glyph.h"

static void ludwig_font_glyph_class_init (LudwigFontGlyphClass *klass);
static void ludwig_font_glyph_init (LudwigFontGlyph *glyph);

static void ludwig_font_glyph_destroy (GtkObject *object);

static GtkObjectClass *parent_class = NULL;

GtkType
ludwig_font_glyph_get_type (void)
{
	static GtkType glyph_type = 0;
	if (!glyph_type)
        {
		GtkTypeInfo glyph_info =
                {
			"LudwigFontGlyph",
			sizeof (LudwigFontGlyph),
			sizeof (LudwigFontGlyphClass),
			(GtkClassInitFunc) ludwig_font_glyph_class_init,
			(GtkObjectInitFunc) ludwig_font_glyph_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};

		glyph_type = gtk_type_unique (gtk_object_get_type (), &glyph_info);
	}

	return glyph_type;
}

static void
ludwig_font_glyph_class_init (LudwigFontGlyphClass *klass)
{
	GtkObjectClass * object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = ludwig_font_glyph_destroy;
}

static void
ludwig_font_glyph_init (LudwigFontGlyph *glyph)
{
	/* Nothing */
}

static void
ludwig_font_glyph_destroy (GtkObject *object)
{
	LudwigFontGlyph * glyph;

	glyph = (LudwigFontGlyph *) object;

	/* If we have CACHED flag, user is mangled ref-ing :( */

	g_assert (!(GTK_OBJECT_FLAGS (glyph) & LUDWIG_FONT_GLYPH_CACHED));

	ludwig_vfont_unref (glyph->vfont);

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

const ArtBpath *
ludwig_font_glyph_get_bpath (LudwigFontGlyph *glyph, gboolean with_transform)
{
	g_return_val_if_fail (glyph != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_GLYPH (glyph), NULL);

	if (LUDWIG_FONT_GLYPH_GET_CLASS (glyph)->bpath)
        {
		return (* LUDWIG_FONT_GLYPH_GET_CLASS (glyph)->bpath) (glyph, with_transform);
	}
        else
        {
		g_warning ("Glyph does not implement ::bpath() method");
	}

	return NULL;
}

ArtDRect *
ludwig_font_glyph_get_dimensions (LudwigFontGlyph *glyph, ArtDRect *box)
{
	ArtPoint a;

	g_return_val_if_fail (glyph != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_GLYPH (glyph), NULL);
	g_return_val_if_fail (box != NULL, NULL);

	if (LUDWIG_FONT_GLYPH_GET_CLASS (glyph)->metrics)
        {
		(* LUDWIG_FONT_GLYPH_GET_CLASS (glyph)->metrics) (glyph, box, &a);
	}
        else
        {
		g_warning ("Glyph does not implement ::metrics() method");
	}

	return box;
}

ArtPoint *
ludwig_font_glyph_get_stdadvance (LudwigFontGlyph *glyph, ArtPoint *p)
{
	ArtDRect box;

	g_return_val_if_fail (glyph != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_GLYPH (glyph), NULL);
	g_return_val_if_fail (p != NULL, NULL);

	if (LUDWIG_FONT_GLYPH_GET_CLASS (glyph)->metrics)
        {
		(* LUDWIG_FONT_GLYPH_GET_CLASS (glyph)->metrics) (glyph, &box, p);
	}
        else
        {
		g_warning ("Glyph does not implement ::metrics() method");
	}

	return p;
}

const LudwigFontGlyphGrayMap *
ludwig_font_glyph_get_graymap (LudwigFontGlyph *glyph)
{
	g_return_val_if_fail (glyph != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_GLYPH (glyph), NULL);

	if (LUDWIG_FONT_GLYPH_GET_CLASS (glyph)->graymap)
        {
		return (* LUDWIG_FONT_GLYPH_GET_CLASS (glyph)->graymap) (glyph);
	}
        else
        {
		g_warning ("Glyph does not implement ::graymap() method");
	}

	return NULL;
}

const LudwigFontGlyphGdkMap *
ludwig_font_glyph_get_gdkmap (LudwigFontGlyph *glyph)
{
	g_return_val_if_fail (glyph != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_GLYPH (glyph), NULL);

	if (LUDWIG_FONT_GLYPH_GET_CLASS (glyph)->gdkmap)
        {
		(* LUDWIG_FONT_GLYPH_GET_CLASS (glyph)->gdkmap) (glyph);
	}
        else
        {
		g_warning ("Glyph does not implement ::gdkmap() method");
	}

	return NULL;
}

ArtPoint *
ludwig_font_glyph_get_kerning (LudwigFontGlyph *a, LudwigFontGlyph *b, ArtPoint *p)
{
	g_return_val_if_fail (a != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_GLYPH (a), NULL);
	g_return_val_if_fail (b != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_GLYPH (b), NULL);
	g_return_val_if_fail (a->vfont == b->vfont, NULL);
	g_return_val_if_fail (p != NULL, NULL);

	if (LUDWIG_VFONT_GET_CLASS (a->vfont)->kerning)
        {
		return (* LUDWIG_VFONT_GET_CLASS (a->vfont)->kerning) (a->vfont, a->code, b->code, p);
	}
        else
        {
		g_warning ("VFont does not implement ::kerning() method");
	}

	p->x = 0.0;
	p->y = 0.0;

	return p;
}

