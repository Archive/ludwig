#define LUDWIG_FONT_GROUP_C

#include <config.h>
#include <glib.h>
#include <gnome-xml/parser.h>
#include "ludwig-font-group.h"

struct _LudwigFontGroupPrivate
{
	gpointer dummy;
};

static void ludwig_font_group_class_init (LudwigFontGroupClass * klass);
static void ludwig_font_group_init (LudwigFontGroup * group);

static void ludwig_font_group_destroy (GtkObject * object);

/* Private & temporary */

static GSList     *gfglist = NULL;
static GHashTable *gdghash = NULL;

static xmlDocPtr gfg_read_groups (void);

static GtkObjectClass * parent_class = NULL;

GtkType
ludwig_font_group_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"LudwigFontGroup",
			sizeof (LudwigFontGroup),
			sizeof (LudwigFontGroupClass),
			(GtkClassInitFunc) ludwig_font_group_class_init,
			(GtkObjectInitFunc) ludwig_font_group_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (gtk_object_get_type (), &group_info);
	}
	return group_type;
}

static void
ludwig_font_group_class_init (LudwigFontGroupClass * klass)
{
	GtkObjectClass * object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = ludwig_font_group_destroy;
}

static void
ludwig_font_group_init (LudwigFontGroup * group)
{
	group->private = g_new (LudwigFontGroupPrivate, 1);
}

static void
ludwig_font_group_destroy (GtkObject * object)
{
	LudwigFontGroup * group;

	group = (LudwigFontGroup *) object;

	if (group->private) {
		g_free (group->private);
		group->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* Basic methods */

void
ludwig_font_group_ref (LudwigFontGroup * group)
{
	g_return_if_fail (group != NULL);
	g_return_if_fail (LUDWIG_IS_FONT_GROUP (group));

	gtk_object_ref ((GtkObject *) group);
}

void
ludwig_font_group_unref (LudwigFontGroup * group)
{
	g_return_if_fail (group != NULL);
	g_return_if_fail (LUDWIG_IS_FONT_GROUP (group));

	gtk_object_unref ((GtkObject *) group);
}

/* Access */

LudwigFontGroup *
ludwig_font_get_group (const gchar * name)
{
	return NULL;
}

/* Private & temporary */
/* fixme: all things */

const GSList *
ludwig_font_group_list (const gchar * language)
{
	static xmlDocPtr xml = NULL;
	static GSList * list = NULL;
	xmlNodePtr root, node;

	if (xml == NULL) {
		xml = gfg_read_groups ();
	}

	g_return_val_if_fail (xml != NULL, NULL);

	if (list != NULL) g_slist_free (list);
	list = NULL;

	root = xml->root;
	g_assert (root != NULL);

	for (node = root->childs; node != NULL; node = node->next) {
		xmlAttrPtr attr;
		for (attr = node->properties; attr != NULL; attr = attr->next) {
			if (strcmp (attr->name, "name") == 0) {
				list = g_slist_prepend (list, attr->val->content);
				break;
			}
		}
	}

	return list;
}

static xmlDocPtr
gfg_read_groups (void)
{
	static xmlDocPtr xml = NULL;

	if (xml == NULL) {
		static gchar * fontfile;
		fontfile = "fonts";
		xml = xmlParseFile (fontfile);
		g_free (fontfile);
	}

	return xml;
}
