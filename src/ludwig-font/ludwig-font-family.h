#ifndef __LUDWIG_FONT_FAMILY_H__
#define __LUDWIG_FONT_FAMILY_H__

#include <gtk/gtk.h>

/* FontFamily */

#define LUDWIG_TYPE_FONTFAMILY		(ludwig_fontfamily_get_type ())
#define LUDWIG_FONTFAMILY(obj)		(GTK_CHECK_CAST ((obj), LUDWIG_TYPE_FONTFAMILY, LudwigFontFamily))
#define LUDWIG_FONTFAMILY_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), LUDWIG_TYPE_FONTFAMILY, LudwigFontFamilyClass))
#define LUDWIG_IS_FONTFAMILY(obj)		(GTK_CHECK_TYPE ((obj), LUDWIG_TYPE_FONTFAMILY))
#define LUDWIG_IS_FONTFAMILY_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), LUDWIG_TYPE_FONTFAMILY))

typedef struct _LudwigFontFamily	LudwigFontFamily;
typedef struct _LudwigFontFamilyClass	LudwigFontFamilyClass;
typedef struct _LudwigFontFamilyPrivate	LudwigFontFamilyPrivate;

struct _LudwigFontFamily
{
	GtkObject object;
	LudwigFontFamilyPrivate *private;
};

struct _LudwigFontFamilyClass
{
	GtkObjectClass parent_class;
};

GtkType ludwig_fontfamily_get_type (void);

#endif
