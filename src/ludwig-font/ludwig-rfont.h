#ifndef __LUDWIG_RFONT_H__
#define __LUDWIG_RFONT_H__

/*
 * LudwigRFont
 *
 * Base class for rasterized fonts, i.e. the ones scaled to real output device
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#define LUDWIG_TYPE_RFONT		(ludwig_rfont_get_type ())
#define LUDWIG_RFONT(obj)		(GTK_CHECK_CAST ((obj), LUDWIG_TYPE_RFONT, LudwigRFont))
#define LUDWIG_RFONT_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), LUDWIG_TYPE_RFONT, LudwigRFontClass))
#define LUDWIG_IS_RFONT(obj)		(GTK_CHECK_TYPE ((obj), LUDWIG_TYPE_RFONT))
#define LUDWIG_IS_RFONT_CLASS(klass) 	(GTK_CHECK_CLASS_TYPE ((klass), LUDWIG_TYPE_RFONT))

typedef struct _LudwigRFont		LudwigRFont;
typedef struct _LudwigRFontClass		LudwigRFontClass;

#include "ludwig-vfont.h"
#include "ludwig-font.h"

struct _LudwigRFont {
	LudwigVFont vfont;

	/* Out master font */

	LudwigFont * font;

	/* Base -> device affine transformation. NB! We use 3x2 matrix,
	 * to keep it usable from libart */

	gdouble affine[6];
};

struct _LudwigRFontClass {
	LudwigVFontClass parent_class;
};

GtkType ludwig_rfont_get_type (void);

#define ludwig_rfont_ref(f) gtk_object_ref (GTK_OBJECT (f))
#define ludwig_rfont_unref(f) gtk_object_unref (GTK_OBJECT (f))

#endif
