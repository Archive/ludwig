#ifndef LUDWIG_FONT_FACE_PRIVATE_H
#define LUDWIG_FONT_FACE_PRIVATE_H

/*
 * LudwigFontFace
 *
 * Abstract, unsized typeface handle. Note, that this is also abstract base
 * class for inheriting implementations of different font file formats
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include "ludwig-font-face.h"

/* fixme: this should go to implementations */

struct _LudwigFontFaceDesc
{
        /* ASCII */
        gchar *language;

        /* Following are UTF-8 or NULL */
        gchar *family;
        gchar *species;
        gchar *name;
};

#endif
