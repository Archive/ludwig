#define LUDWIG_FONT_FACE_C

/*
 * LudwigFontFace
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

/*
 * TODO:
 *
 * Subclass it? Partially done.
 *
 */

#include <config.h>
#include "ludwig-font.h"
#include "ludwig-font-face.h"
#include "ludwig-font-face-private.h"

struct _LudwigFontFacePrivate
{
	GHashTable * descriptions;

	/* fixme: some fonts does not have it :( */

	gchar * psname;
};

static void gff_class_init (LudwigFontFaceClass *klass);
static void gff_init (LudwigFontFace * face);

static void gff_destroy (GtkObject * object);

static LudwigFontFaceDesc * gff_req_description (LudwigFontFace * face, const gchar * language);
static gboolean gff_free_dict_desc (gpointer key, gpointer value, gpointer data);

static LudwigVFontClass * parent_class = NULL;

GtkType
ludwig_font_face_get_type (void)
{
	static GtkType gff_type = 0;
	if (!gff_type) {
		GtkTypeInfo gff_info = {
			"LudwigFontFace",
			sizeof (LudwigFontFace),
			sizeof (LudwigFontFaceClass),
			(GtkClassInitFunc) gff_class_init,
			(GtkObjectInitFunc) gff_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		gff_type = gtk_type_unique (ludwig_vfont_get_type (), &gff_info);
	}
	return gff_type;
}

LudwigFontFace *ludwig_font_face_new_from_file(const gchar *filename,
                                             const gchar *afm,
                                             gint face)
{
   return ludwig_font_face_real_new_from_file(filename, afm, face);
}

static void
gff_class_init (LudwigFontFaceClass *klass)
{
	GtkObjectClass * object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class (ludwig_vfont_get_type ());

	object_class->destroy = gff_destroy;
}

static void
gff_init (LudwigFontFace * face)
{
	face->private = g_new (LudwigFontFacePrivate, 1);

	face->private->descriptions = g_hash_table_new (g_str_hash, g_str_equal);

	face->private->psname = NULL;
}

static void
gff_destroy (GtkObject * object)
{
	LudwigFontFace * face;

	face = (LudwigFontFace *) object;

	if (face->private) {

		/*
		 * We can safely destroy dictionary here, cause all names
		 * should be freed by implementations
		 */

		if (face->private->descriptions) {
			g_hash_table_foreach_remove (face->private->descriptions, gff_free_dict_desc, NULL);
		}

		g_free (face->private);
		face->private = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* Basic methods */

/* fixme: */

LudwigFontFamily *
ludwig_font_face_get_family (const LudwigFontFace * face)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_FACE (face), NULL);
	g_return_val_if_fail (face->private != NULL, NULL);

	return NULL;
}

/* fixme: */

const gchar *
ludwig_font_face_get_family_name (LudwigFontFace * face, const gchar * language)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_FACE (face), NULL);
	g_return_val_if_fail (face->private != NULL, NULL);

	return NULL;
}

/* fixme: use intelligence here :) */

const gchar *
ludwig_font_face_get_type_name (LudwigFontFace * face, const gchar * language)
{
	LudwigFontFaceDesc * desc;

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_FACE (face), NULL);
	g_return_val_if_fail (language != NULL, NULL);
	g_return_val_if_fail (face->private != NULL, NULL);

	desc = gff_req_description (face, language);
	g_assert (desc != NULL);

	return desc->species;
}

/* fixme: use intelligence here :) */

const gchar *
ludwig_font_face_get_canonical_name (LudwigFontFace * face, const gchar * language)
{
	LudwigFontFaceDesc * desc;

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_FACE (face), NULL);
	g_return_val_if_fail (language != NULL, NULL);
	g_return_val_if_fail (face->private != NULL, NULL);

	desc = gff_req_description (face, language);
	g_assert (desc != NULL);

	return desc->name;
}

const gchar *
ludwig_font_face_get_ps_name (LudwigFontFace * face)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_FACE (face), NULL);
	g_return_val_if_fail (face->private != NULL, NULL);

	return NULL;
}

LudwigFont *
ludwig_font_face_get_font (LudwigFontFace * face, gdouble size, gdouble xres, gdouble yres)
{
	gdouble transform[4];

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_FACE (face), NULL);

	transform[0] = xres / 72.0;
	transform[3] = yres / 72.0;
	transform[1] = transform[2] = 0.0;

	return ludwig_font_face_get_font_full (face, size, transform);
}

LudwigFont *
ludwig_font_face_get_font_full (LudwigFontFace * face, gdouble size, gdouble transform[])
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_FACE (face), NULL);

	if (LUDWIG_FONT_FACE_GET_CLASS (face)->get_font)
		return (* LUDWIG_FONT_FACE_GET_CLASS (face)->get_font) (face, size, transform);

	g_warning ("Face does not implement get_font method");

	return NULL;
}

/* EXPERIMENTAL */

gboolean
ludwig_font_face_set_encoding (LudwigFontFace * face, gint id, const gchar * name, LudwigFontEncoding encoding)
{
	gboolean ret;

	g_return_val_if_fail (face != NULL, FALSE);
	g_return_val_if_fail (LUDWIG_IS_FONT_FACE (face), FALSE);
	g_return_val_if_fail (id >= 0, FALSE);
	/* NB! 256 encodings per font is arbitrary - hope you do not need more */
	g_return_val_if_fail (id < 256, FALSE);
	g_return_val_if_fail (encoding != NULL, FALSE);

	ret = FALSE;

	if (LUDWIG_FONT_FACE_GET_CLASS (face)->set_encoding)
	{
		ret = (* LUDWIG_FONT_FACE_GET_CLASS (face)->set_encoding) (face, id, encoding);
	}
	else
	{
		g_warning ("Face does not implement set_encoding method!");
	}

	/* fixme: define name->id mapping */

	return ret;
}

gint
ludwig_font_face_lookup (LudwigFontFace * face, gint encoding, gint code)
{
	g_return_val_if_fail (face != NULL, FALSE);
	g_return_val_if_fail (LUDWIG_IS_FONT_FACE (face), FALSE);

	if (LUDWIG_FONT_FACE_GET_CLASS (face)->lookup)
	{
		return (* LUDWIG_FONT_FACE_GET_CLASS (face)->lookup) (face, encoding, code);
	}
	else
	{
		g_warning ("Face does not implement lookup method!");
	}

	return 0;
}

/* Helpers */

static LudwigFontFaceDesc *
gff_req_description (LudwigFontFace * face, const gchar *language)
{
	LudwigFontFaceClass *klass;
	LudwigFontFaceDesc *desc;

	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_FONT_FACE (face), NULL);
	g_return_val_if_fail (language != NULL, NULL);
	g_return_val_if_fail (face->private != NULL, NULL);

	desc = g_hash_table_lookup (face->private->descriptions, language);

	if (desc != NULL) return desc;

	klass = LUDWIG_FONT_FACE_GET_CLASS (face);

	if (!klass->description) return NULL;

	desc = (* klass->description) (face, language);

	if (desc == NULL) desc = (* klass->description) (face, "C");

	g_assert (desc != NULL);
	g_assert (desc->language);
	g_assert (desc->species);
	g_assert (desc->name);

	g_hash_table_insert (face->private->descriptions, desc->language, desc);

	return desc;
}

static gboolean
gff_free_dict_desc (gpointer key, gpointer value, gpointer data)
{
	LudwigFontFaceDesc * desc;

	desc = (LudwigFontFaceDesc *) value;

	/* Language menber serves also as key */

	g_free (desc->language);
	g_free (desc->species);
	g_free (desc->name);

	g_free (desc);

	return TRUE;
}


