#define LUDWIG_VFONT_C

/*
 * LudwigVFont
 *
 * Abstract virtual base class for all glyph collection types
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <config.h>
#include <locale.h>
#include "ludwig-vfont.h"

static void ludwig_vfont_class_init (LudwigVFontClass * klass);
static void ludwig_vfont_init       (LudwigVFont * group);

static void ludwig_vfont_destroy (GtkObject * object);

static gboolean gvf_remove_glyph (gpointer key, gpointer value, gpointer data);

static GtkObjectClass * parent_class = NULL;

GtkType
ludwig_vfont_get_type (void)
{
	static GtkType group_type = 0;
	if (!group_type) {
		GtkTypeInfo group_info = {
			"LudwigVFont",
			sizeof (LudwigVFont),
			sizeof (LudwigVFontClass),
			(GtkClassInitFunc) ludwig_vfont_class_init,
			(GtkObjectInitFunc) ludwig_vfont_init,
			NULL, NULL,
			(GtkClassInitFunc) NULL
		};
		group_type = gtk_type_unique (gtk_object_get_type (), &group_info);
	}
	return group_type;
}

static void
ludwig_vfont_class_init (LudwigVFontClass * klass)
{
	GtkObjectClass * object_class;

	object_class = (GtkObjectClass *) klass;

	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = ludwig_vfont_destroy;
}

static void
ludwig_vfont_init (LudwigVFont * vfont)
{
	vfont->glyphs = g_hash_table_new (NULL, NULL);
}

static void
ludwig_vfont_destroy (GtkObject * object)
{
	LudwigVFont * vfont;

	vfont = (LudwigVFont *) object;

	if (vfont->glyphs) {
		g_hash_table_foreach_remove (vfont->glyphs, gvf_remove_glyph, NULL);
		vfont->glyphs = NULL;
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(*((GtkObjectClass *) parent_class)->destroy) (object);
}

/* Methods */

LudwigFontGlyph *
ludwig_vfont_get_glyph (LudwigVFont * vfont, gint code)
{
	LudwigFontGlyph * glyph;

	g_return_val_if_fail (vfont != NULL, NULL);
	g_return_val_if_fail (LUDWIG_IS_VFONT (vfont), NULL);
	g_return_val_if_fail (vfont->glyphs != NULL, NULL);

	glyph = g_hash_table_lookup (vfont->glyphs, GINT_TO_POINTER (code));

	/* If we are in hash, then we can count our CACHED flag is set :) */

	if (glyph) return glyph;

	if (LUDWIG_VFONT_GET_CLASS (vfont)->get_glyph) {
		glyph = (* LUDWIG_VFONT_GET_CLASS (vfont)->get_glyph) (vfont, code);

		g_return_val_if_fail (glyph != NULL, NULL);

		g_hash_table_insert (vfont->glyphs, GINT_TO_POINTER (code), glyph);

		GTK_OBJECT_SET_FLAGS (glyph, LUDWIG_FONT_GLYPH_CACHED);
		
		ludwig_vfont_ref (vfont);

		return glyph;
	} else {
		g_warning ("VFont does not implement get_glyph method!");
	}

	return NULL;
}

static gboolean
gvf_remove_glyph (gpointer key, gpointer value, gpointer data)
{
	LudwigFontGlyph * glyph;

	glyph = (LudwigFontGlyph *) value;

	g_assert (GTK_OBJECT_FLAGS (glyph) & LUDWIG_FONT_GLYPH_CACHED);

	GTK_OBJECT_UNSET_FLAGS (glyph, LUDWIG_FONT_GLYPH_CACHED);

	ludwig_font_glyph_unref (glyph);

	return TRUE;
}

