/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#ifndef __LUDWIG_INTERFACE_PREFS_DIALOG_H__
#define __LUDWIG_INTERFACE_PREFS_DIALOG_H__

#include <bonobo/bonobo-ui-component.h>
#include <libgnomeui/gnome-dialog.h>
#include <gal/e-table/e-table.h>
#include <gal/e-table/e-tree.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define LUDWIG_TYPE_INTERFACE_PREFS_DIALOG        (ludwig_interface_prefs_dialog_get_type ())
#define LUDWIG_INTERFACE_PREFS_DIALOG(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_INTERFACE_PREFS_DIALOG, LudwigInterfacePrefsDialog))
#define LUDWIG_INTERFACE_PREFS_DIALOG_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_INTERFACE_PREFS_DIALOG, LudwigInterfacePrefsDialogClass))
#define LUDWIG_IS_INTERFACE_PREFS_DIALOG(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_INTERFACE_PREFS_DIALOG))
#define LUDWIG_IS_INTERFACE_PREFS_DIALOG_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_INTERFACE_PREFS_DIALOG))

typedef struct _LudwigInterfacePrefsDialog      LudwigInterfacePrefsDialog;
typedef struct _LudwigInterfacePrefsDialogClass LudwigInterfacePrefsDialogClass;

struct _LudwigInterfacePrefsDialog
{
        GnomeDialog parent_object;

        BonoboUIComponent *component;

        GtkWidget *notebook;

        GSList       *tools_paths;
        ETreeModel   *tools_model;
        ETreeMemory  *tools_memory;
        ETree        *tools_tree;

        ETableModel  *verbs_model;
        ETableHeader *verbs_header;
        ETable       *verbs_table;
};

struct _LudwigInterfacePrefsDialogClass
{
        GnomeDialogClass parent_class;
};

GtkType ludwig_interface_prefs_dialog_get_type (void);
void    ludwig_interface_prefs_dialog_create   (BonoboUIComponent *component);
void    ludwig_interface_prefs_dialog_shutdown (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __LUDWIG_INTERFACE_PREFS_DIALOG_H__ */

