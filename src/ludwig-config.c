/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <gconf/gconf-client.h>
#include "ludwig-config.h"

static GConfClient *client = NULL;

static void
ensure_gconf (void)
{
	if (client == NULL)
	{
		client = gconf_client_get_default ();
		gconf_client_add_dir (client, "/apps/ludwig", GCONF_CLIENT_PRELOAD_NONE, NULL);
	}
}

void
ludwig_config_shutdown (void)
{
	gtk_object_unref (GTK_OBJECT (client));
}

gchar *
ludwig_config_query_profile (void)
{
	ensure_gconf ();

	gconf_client_get_string (client, "/config/profile", NULL);
}

void
ludwig_config_set_profile (const gchar *profile)
{
	ensure_gconf ();

	gconf_client_set_string (client, "/config/profile", profile, NULL);
}

GSList *
ludwig_config_query_all_profiles (void)
{
	ensure_gconf ();
}
