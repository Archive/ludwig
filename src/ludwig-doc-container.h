/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include "ludwig-doc-item.h"
#include "ludwig-types.h"

#ifndef __LUDWIG_DOC_CONTAINER_H__
#define __LUDWIG_DOC_CONTAINER_H__

#define LUDWIG_TYPE_DOC_CONTAINER        (ludwig_doc_container_get_type())
#define LUDWIG_DOC_CONTAINER(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_DOC_CONTAINER, LudwigDocContainer))
#define LUDWIG_DOC_CONTAINER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_DOC_CONTAINER, LudwigDocContainerClass))
#define LUDWIG_IS_DOC_CONTAINER(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_DOC_CONTAINER))
#define LUDWIG_IS_DOC_CONTAINER_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_DOC_CONTAINER))

struct _LudwigDocContainer
{
	LudwigDocItem parent_object;

        GList *items;
};

struct _LudwigDocContainerClass
{
	LudwigDocItemClass parent_class;
};

GtkType      ludwig_doc_container_get_type    (void);

#endif /* __LUDWIG_DOC_CONTAINER_H__ */
