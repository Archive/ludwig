/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include "ludwig-view-container.h"
#include "ludwig-view-staff.h"

#ifndef __LUDWIG_VIEW_MEASURE_H__
#define __LUDWIG_VIEW_MEASURE_H__

#define LUDWIG_TYPE_VIEW_MEASURE        (ludwig_view_measure_get_type())
#define LUDWIG_VIEW_MEASURE(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_VIEW_MEASURE, LudwigViewMeasure))
#define LUDWIG_VIEW_MEASURE_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_VIEW_MEASURE,  LudwigViewMeasureClass))
#define LUDWIG_IS_VIEW_MEASURE(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_VIEW_MEASURE))
#define LUDWIG_IS_VIEW_MEASURE_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_VIEW_MEASURE))

typedef struct _LudwigViewMeasure      LudwigViewMeasure;
typedef struct _LudwigViewMeasureClass LudwigViewMeasureClass;

struct _LudwigViewMeasure
{
        LudwigViewContainer  parent_object;

        GnomeCanvasGroup    *group;
        GSList              *staff_lines;
        GnomeCanvasItem     *barline;
        gboolean             complete;

        int                  num;
        double               len;

        LudwigViewMeasure *next;
        LudwigViewMeasure *prev;
};

struct _LudwigViewMeasureClass
{
        LudwigViewContainerClass parent_class;
};

GtkType            ludwig_view_measure_get_type        (void);
void               ludwig_view_measure_construct       (LudwigViewMeasure *measure);
LudwigViewStaff   *ludwig_view_measure_get_staff       (LudwigViewMeasure *measure);
LudwigViewMeasure *ludwig_view_measure_get_prev        (LudwigViewMeasure *measure);
void               ludwig_view_measure_install_timesig (LudwigViewMeasure *measure, int num, int denom);
void               ludwig_view_measure_insert_note     (LudwigViewMeasure *measure, int value,
                                                        gboolean is_dotted, gboolean is_double);
void               ludwig_view_measure_insert_rest     (LudwigViewMeasure *measure, int value,
                                                        gboolean is_dotted, gboolean is_double);

#endif /* __LUDWIG_VIEW_MEASURE_H__ */

