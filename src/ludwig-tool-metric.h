/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_TOOL_METRIC_H__
#define __LUDWIG_TOOL_METRIC_H__

#include "ludwig-tool.h"

#define LUDWIG_TYPE_TOOL_METRIC        (ludwig_tool_metric_get_type())
#define LUDWIG_TOOL_METRIC(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_TOOL_METRIC, LudwigToolMetric))
#define LUDWIG_TOOL_METRIC_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_TOOL_METRIC, LudwigToolMetricClass))
#define LUDWIG_IS_TOOL_METRIC(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_TOOL_METRIC))
#define LUDWIG_IS_TOOL_METRIC_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_TOOL_METRIC))

typedef struct _LudwigToolMetric      LudwigToolMetric;
typedef struct _LudwigToolMetricClass LudwigToolMetricClass;

struct _LudwigToolMetric
{
	LudwigTool parent_object;
};

struct _LudwigToolMetricClass
{
	LudwigToolClass parent_class;
};

GtkType     ludwig_tool_get_type (void);

#endif /* __LUDWIG_TOOL_METRIC_H__ */
