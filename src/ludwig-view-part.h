/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include "ludwig-view-container.h"
#include "ludwig-types.h"

#ifndef __LUDWIG_VIEW_PART_H__
#define __LUDWIG_VIEW_PART_H__

#define LUDWIG_TYPE_VIEW_PART        (ludwig_view_part_get_type())
#define LUDWIG_VIEW_PART(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_VIEW_PART, LudwigViewPart))
#define LUDWIG_VIEW_PART_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_VIEW_PART, LudwigViewPartClass))
#define LUDWIG_IS_VIEW_PART(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_VIEW_PART))
#define LUDWIG_IS_VIEW_PART_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_VIEW_PART))

typedef struct _LudwigViewPart      LudwigViewPart;
typedef struct _LudwigViewPartClass LudwigViewPartClass;

struct _LudwigViewPart
{
        LudwigViewContainer parent_object;

        gchar *name;

        GList *time_changes;       /* A list of measures that have had changes to time signature,
                                    * to what time signature they change.
                                    */

        GList *key_changes;        /* A list of measures containing changes in key, what beat
                                    * those changes occur on, and to what key they change.
                                    */

        double height;

        double max_length;
        double length;

        LudwigView     *view;

        LudwigViewPart *next;
        LudwigViewPart *prev;
};

struct _LudwigViewPartClass
{
        LudwigViewContainerClass parent_class;
};

GtkType  ludwig_view_part_get_type    (void);
void     ludwig_view_part_construct   (LudwigViewPart *part, LudwigClef clef);

#endif /* __LUDWIG_VIEW_PART_H__ */
