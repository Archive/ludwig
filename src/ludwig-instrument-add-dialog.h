/*
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_INSTRUMENT_ADD_DIALOG_H__
#define __LUDWIG_INSTRUMENT_ADD_DIALOG_H__

#include <gnome.h>
#include <gal/e-table/e-tree.h>

#include "ludwig-instrument-list.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define LUDWIG_TYPE_INSTRUMENT_ADD_DIALOG        (ludwig_instrument_add_dialog_get_type ())
#define LUDWIG_INSTRUMENT_ADD_DIALOG(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_INSTRUMENT_ADD_DIALOG, LudwigInstrumentAddDialog))
#define LUDWIG_INSTRUMENT_ADD_DIALOG_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_INSTRUMENT_ADD_DIALOG, LudwigInstrumentAddDialogClass))
#define LUDWIG_IS_INSTRUMENT_ADD_DIALOG(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_INSTRUMENT_ADD_DIALOG))
#define LUDWIG_IS_INSTRUMENT_ADD_DIALOG_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_INSTRUMENT_ADD_DIALOG))

typedef struct _LudwigInstrumentAddDialog      LudwigInstrumentAddDialog;
typedef struct _LudwigInstrumentAddDialogClass LudwigInstrumentAddDialogClass;

enum
{
        COL_CREATE,
        COL_SOLO,
        COL_NAME,

	COL_LAST,

        /* Invisible */
        COL_POSITION
};

struct _LudwigInstrumentAddDialog
{
	GnomeDialog parent_object;

        GSList      *paths;

	ETreeModel  *model;
        ETreeMemory *memory;
	ETree       *tree;

	ETreePath    selected_path;
	GtkWidget   *notebook;
	GHashTable  *notebook_pages;
	int          pages;
};

struct _LudwigInstrumentAddDialogClass
{
	GnomeDialogClass parent_class;
};

GtkType ludwig_instrument_add_dialog_get_type (void);
void    ludwig_instrument_add_dialog_create   (void);
void    ludwig_instrument_add_dialog_destroy  (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __LUDWIG_INSTRUMENT_ADD_DIALOG_H__ */
