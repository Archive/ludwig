/*
 * Copyright (C) 2000 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_DOCUMENT_H__
#define __LUDWIG_DOCUMENT_H__

#include <gtk/gtkobject.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>

#include "ludwig-instrument.h"
#include "ludwig-string.h"
#include "ludwig-util.h"
#include "ludwig-types.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define LUDWIG_TYPE_DOCUMENT        (ludwig_document_get_type())
#define LUDWIG_DOCUMENT(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_DOCUMENT, LudwigDocument))
#define LUDWIG_DOCUMENT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_DOCUMENT, LudwigDocumentClass))
#define LUDWIG_IS_DOCUMENT(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_DOCUMENT))
#define LUDWIG_IS_DOCUMENT_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_DOCUMENT))

struct _LudwigDocument
{
	GtkObject  object;

	xmlDocPtr     doc;
	LudwigString *title;
	LudwigString *composer;
	GHashTable   *parts;
	gint          n_parts;
	gchar        *filename;
	gboolean      modified;
};

/* This is really weak.  We need support for complex time signatures. */
struct _LudwigTimeSig
{
        short numerator;
        short denominator;
};

struct _LudwigDocumentClass
{
	GtkObjectClass parent_class;

        void (* instrument_added) (LudwigDocument *doc,
                                   const char *instrument,
                                   LudwigClef clef,
                                   LudwigView *view);
        void (* measure_added)    (LudwigDocument *doc,
                                   const char *part,
                                   LudwigView *view);
};

/* Public interfaces */
GtkType         ludwig_document_get_type        (void);
LudwigDocument *ludwig_document_new             (gchar *filename);
LudwigDocument *ludwig_document_new_from_stream (char *buffer, size_t sz);
void            ludwig_document_load            (LudwigDocument *doc);
void            ludwig_document_save            (LudwigDocument *doc);
LudwigDocPart  *ludwig_document_get_part        (LudwigDocument *doc, const gchar *name);
void            ludwig_document_add_part        (LudwigDocument *doc, LudwigInstrument *instrument, LudwigClef clef);
void            ludwig_document_remove_part     (LudwigDocument *doc, const gchar *name);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __LUDWIG_DOCUMENT_H__ */
