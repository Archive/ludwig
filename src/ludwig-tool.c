/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include "ludwig-tool.h"

enum
{
        INITIALIZE,
        BUTTON_PRESS,
        BUTTON_RELEASE,
        MOTION,
        LAST_SIGNAL
};

static void   ludwig_tool_class_init          (LudwigToolClass  *class);
static void   ludwig_tool_init                (LudwigTool       *tool);
static void   ludwig_tool_destroy             (GtkObject        *destroy);
static void   ludwig_tool_real_initialize     (LudwigTool       *tool,
                                               LudwigView       *view);
static void   ludwig_tool_real_control        (LudwigTool       *tool,
                                               ToolAction        tool_action,
                                               LudwigView       *view);
static void   ludwig_tool_real_button_press   (LudwigTool       *tool,
                                               GdkEventButton   *evt,
                                               LudwigView       *view);
static void   ludwig_tool_real_button_release (LudwigTool       *tool,
                                               GdkEventButton   *evt,
                                               LudwigView       *view);
static void   ludwig_tool_real_motion         (LudwigTool       *tool,
                                               GdkEventMotion   *evt,
                                               LudwigView       *view);
static void   ludwig_tool_real_arrow_key      (LudwigTool       *tool,
                                               GdkEventKey      *evt,
                                               LudwigView       *view);
static void   ludwig_tool_real_modifier_key   (LudwigTool       *tool,
                                               GdkEventKey      *evt,
                                               LudwigView       *view);
static void   ludwig_tool_real_cursor_update  (LudwigTool       *tool,
                                               GdkEventMotion   *evt,
                                               LudwigView       *view);
static void   ludwig_tool_real_oper_update    (LudwigTool       *tool,
                                               GdkEventMotion   *evt,
                                               LudwigView       *view);

static guint signals[LAST_SIGNAL] = { 0 };
static GtkObjectClass *parent_class = NULL;

GtkType
ludwig_tool_get_type (void)
{
        static GtkType type = 0;

        if (!type)
        {
                GtkTypeInfo info =
                {
                        "LudwigTool",
                        sizeof (LudwigTool),
                        sizeof (LudwigToolClass),
                        (GtkClassInitFunc) ludwig_tool_class_init,
                        (GtkObjectInitFunc) ludwig_tool_init,
                        NULL, /* Reserved 1 */
                        NULL, /* Reserved 2 */
                        (GtkClassInitFunc) NULL
                };

                type = gtk_type_unique (GTK_TYPE_OBJECT, &info);
        }

        return type;
}

static void
ludwig_tool_class_init (LudwigToolClass *class)
{
        GtkObjectClass *object_class;

        object_class = (GtkObjectClass *) class;

        parent_class = gtk_type_class (GTK_TYPE_OBJECT);

        signals[INITIALIZE] =
                gtk_signal_new ("initialize",
                                GTK_RUN_FIRST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (LudwigToolClass,
                                                   initialize),
                                gtk_marshal_NONE__POINTER, 
                                GTK_TYPE_NONE, 1,
                                GTK_TYPE_POINTER);

        signals[BUTTON_PRESS] =
                gtk_signal_new ("button_press",
                                GTK_RUN_FIRST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (LudwigToolClass,
				       button_press),
		    gtk_marshal_NONE__POINTER_POINTER,
		    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);

        signals[BUTTON_RELEASE] =
                gtk_signal_new ("button_release",
                                GTK_RUN_FIRST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (LudwigToolClass,
                                                   button_release),
                                gtk_marshal_NONE__POINTER_POINTER, 
                                GTK_TYPE_NONE, 2,
                                GTK_TYPE_POINTER,
                                GTK_TYPE_POINTER);

        signals[MOTION] =
                gtk_signal_new ("motion",
                                GTK_RUN_FIRST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (LudwigToolClass,
                                                   motion),
                                gtk_marshal_NONE__POINTER_POINTER, 
                                GTK_TYPE_NONE, 2,
                                GTK_TYPE_POINTER,
                                GTK_TYPE_POINTER);

        gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);  

        object_class->destroy = ludwig_tool_destroy;

        class->initialize     = ludwig_tool_real_initialize;
        class->button_press   = ludwig_tool_real_button_press;
        class->button_release = ludwig_tool_real_button_release;
        class->motion         = ludwig_tool_real_motion;
}

static void
ludwig_tool_init (LudwigTool *tool)
{
        tool->state = INACTIVE;
}

static void
ludwig_tool_destroy (GtkObject      *destroy)
{
}

void
ludwig_tool_initialize (LudwigTool *tool, 
                        LudwigView *view)
{
        g_return_if_fail (tool);
        g_return_if_fail (LUDWIG_IS_TOOL (tool));

        gtk_signal_emit (GTK_OBJECT (tool),
                         signals[INITIALIZE],
                         view);
}

void
ludwig_tool_button_press (LudwigTool *tool,
                          GdkEventButton *evt,
                          LudwigView *view)
{
        g_return_if_fail (tool);
        g_return_if_fail (LUDWIG_IS_TOOL (tool));

        gtk_signal_emit (GTK_OBJECT (tool),
                         signals[BUTTON_PRESS],
                         evt, view); 
}

void
ludwig_tool_button_release (LudwigTool *tool,
                            GdkEventButton *evt,
                            LudwigView *view)
{
        g_return_if_fail (tool);
        g_return_if_fail (LUDWIG_IS_TOOL (tool));

        gtk_signal_emit (GTK_OBJECT (tool),
                         signals[BUTTON_RELEASE],
                         evt, view);
}

void
ludwig_tool_motion (LudwigTool *tool,
                    GdkEventMotion *evt,
                    LudwigView *view)
{
        g_return_if_fail (tool);
        g_return_if_fail (LUDWIG_IS_TOOL (tool));

        gtk_signal_emit (GTK_OBJECT (tool),
                         signals[MOTION],
                         evt, view);
}

static void
ludwig_tool_real_initialize (LudwigTool *tool,
                             LudwigView *view)
{
}

static void
ludwig_tool_real_button_press (LudwigTool       *tool,
                               GdkEventButton *evt,
                               LudwigView *view)
{
        tool->view = view;
        tool->canvas = view->canvas;

        tool->state = ACTIVE;
}

static void
ludwig_tool_real_button_release (LudwigTool *tool,
                                 GdkEventButton *evt,
                                 LudwigView *view)
{
        tool->state = INACTIVE;
}

static void
ludwig_tool_real_motion (LudwigTool       *tool,
                         GdkEventMotion *evt,
                         LudwigView *view)
{
}

GdkPixbuf *
ludwig_tool_get_pixbuf (LudwigTool *tool)
{
        gdk_pixbuf_ref (tool->pixbuf);

        return tool->pixbuf;
}

void
ludwig_tool_set_pixbuf (LudwigTool *tool, GdkPixbuf *pixbuf)
{
        gdk_pixbuf_unref (tool->pixbuf);

        tool->pixbuf = pixbuf;

        gdk_pixbuf_ref (pixbuf);
}
