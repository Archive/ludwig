/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include "ludwig-view-part.h"
#include "ludwig-view-staff.h"

#include <ludwig-font/ludwig-font.h>
#include <ludwig-font/ludwig-font-glyph.h>
#include <ludwig-font/ludwig-rfont.h>

#include <canvas-items/gp-path.h>
#include <canvas-items/gnome-canvas-ttext.h>

enum
{
        ARG_0,
        ARG_VIEW,
        ARG_NAME
};

static void ludwig_view_part_class_init (LudwigViewPartClass *class);
static void ludwig_view_part_init       (LudwigViewPart      *part);
static void ludwig_view_part_destroy    (GtkObject           *object);
static void ludwig_view_part_set_arg    (GtkObject           *object,
                                         GtkArg              *arg,
                                         guint                arg_id);
static void ludwig_view_part_get_arg    (GtkObject           *object,
                                         GtkArg              *arg,
                                         guint                arg_id);

static LudwigViewContainerClass *parent_class = NULL;

GtkType
ludwig_view_part_get_type (void)
{
        static GtkType type = 0;

        if (!type)
        {
                GtkTypeInfo info =
                {
                        "LudwigViewPart",
                        sizeof (LudwigViewPart),
                        sizeof (LudwigViewPartClass),
                        (GtkClassInitFunc) ludwig_view_part_class_init,
                        (GtkObjectInitFunc) ludwig_view_part_init,
                        NULL, /* Reserved 1 */
                        NULL, /* Reserved 2 */
                        (GtkClassInitFunc) NULL
                };

                type = gtk_type_unique (ludwig_view_container_get_type (), &info);
        }

        return type;
}

void
ludwig_view_part_construct (LudwigViewPart *part, LudwigClef clef)
{
        GnomeCanvasItem *staff;
        GnomeCanvasItem *item;
        LudwigFont *font;
        LudwigFontGlyph *glyph;
        int code;
        int clef_code;
        GPPath *path;
        double affine[6];

        staff = gnome_canvas_item_new (GNOME_CANVAS_GROUP (part),
                                       ludwig_view_staff_get_type (),
                                       "x", 0.0,
                                       "y", 0.0,
                                       NULL);

        ludwig_view_staff_construct (LUDWIG_VIEW_STAFF (staff));

        switch (clef)
        {
        case CLEF_TREBLE:
                clef_code = 38;
                art_affine_scale (affine, 0.8, 0.8);
                affine[4] = 8.0;
                affine[5] = 27.0;
                affine[3] = 0.0 - affine[3];  /* Flip on x-axis */
                break;

        case CLEF_ALTO:
                clef_code = 66;
                art_affine_scale (affine, 0.76, 0.76);
                affine[4] = 4.0;
                affine[5] = 18.0;
                affine[3] = 0.0 - affine[3];
                break;

        case CLEF_TENOR:
                clef_code = 66;
                art_affine_scale (affine, 0.76, 0.76);
                affine[4] = 4.0;
                affine[5] = 9.0;
                affine[3] = 0.0 - affine[3];
                break;

        case CLEF_BASS:
                clef_code = 63;
                art_affine_scale (affine, 1.0, 1.0);
                affine[4] = 4.0;
                affine[5] = 11.0;
                affine[3] = 0.0 - affine[3];
                break;
        }

        font = (LudwigFont *) ludwig_get_standard_font ();
        code = ludwig_font_face_lookup (font->face, 0, clef_code);
        glyph = ludwig_vfont_get_glyph ((LudwigVFont *) font, code);
        path = gp_path_new_from_foreign_bpath (ludwig_font_glyph_get_bpath (glyph, TRUE));

        item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (part),
                                      gnome_canvas_bpath_get_type (),
                                      "bpath", path,
                                      "fill_color_rgba", 0x00ff,
                                      NULL);

        gnome_canvas_item_affine_absolute (item, affine);
}

static void
ludwig_view_part_class_init (LudwigViewPartClass *class)
{
        GtkObjectClass *object_class;

        object_class = (GtkObjectClass *) class;

        parent_class = gtk_type_class (ludwig_view_container_get_type ());

        gtk_object_add_arg_type ("LudwigViewPart::view",
                                 GTK_TYPE_POINTER,
                                 GTK_ARG_READWRITE,
                                 ARG_VIEW);
        gtk_object_add_arg_type ("LudwigViewPart::name",
                                 GTK_TYPE_STRING,
                                 GTK_ARG_READWRITE,
                                 ARG_NAME);

        object_class->destroy = ludwig_view_part_destroy;
        object_class->set_arg = ludwig_view_part_set_arg;
        object_class->get_arg = ludwig_view_part_get_arg;
};

static void
ludwig_view_part_init (LudwigViewPart *part)
{
        part->name = NULL;
        part->view = NULL;
}

static void
ludwig_view_part_destroy (GtkObject *object)
{
}

static void
ludwig_view_part_set_arg (GtkObject *object,
                          GtkArg    *arg,
                          guint      arg_id)
{
        GnomeCanvasItem *item;
        LudwigViewPart  *part;

        item = (GnomeCanvasItem *) object;
        part = (LudwigViewPart  *) object;

        switch (arg_id)
        {
        case ARG_VIEW:
                part->view = GTK_VALUE_POINTER (*arg);
                break;

        case ARG_NAME:
                if (part->name != NULL)
                {
                        g_free (part->name);
                }

                part->name = g_strdup (GTK_VALUE_STRING (*arg));
                break;
        }
}

static void
ludwig_view_part_get_arg (GtkObject *object,
                          GtkArg    *arg,
                          guint      arg_id)
{
        LudwigViewPart *part = (LudwigViewPart *) object;

        switch (arg_id)
        {
        case ARG_VIEW:
                GTK_VALUE_POINTER (*arg) = part->view;
                break;

        case ARG_NAME:
                GTK_VALUE_STRING (*arg) = g_strdup (part->name);
                break;
        }
}
