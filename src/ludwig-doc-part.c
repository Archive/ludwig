/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include "ludwig-doc-part.h"
#include "ludwig-doc-measure.h"

enum
{
        MEASURE_ADDED,
        LAST_SIGNAL
};

static LudwigDocContainerClass *parent_class = NULL;
static guint                    signals[LAST_SIGNAL];

static void
ludwig_doc_part_destroy (GtkObject *object)
{
}

static void
ludwig_doc_part_set_arg (GtkObject *object,
                         GtkArg    *arg,
                         guint      arg_id)
{
}

static void
ludwig_doc_part_get_arg (GtkObject *object,
                         GtkArg    *arg,
                         guint      arg_id)
{
}

static void
ludwig_doc_part_class_init (LudwigDocPartClass *class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;

	parent_class = gtk_type_class (ludwig_doc_container_get_type ());

        signals[MEASURE_ADDED] =
                gtk_signal_new ("measure_added",
                                GTK_RUN_FIRST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (LudwigDocPartClass,
                                                   measure_added),
                                gtk_marshal_NONE__POINTER,
                                GTK_TYPE_NONE,
                                1,
                                GTK_TYPE_STRING);

        gtk_object_class_add_signals (object_class,
                                      signals,
                                      LAST_SIGNAL);

	object_class->destroy = ludwig_doc_part_destroy;
	object_class->set_arg = ludwig_doc_part_set_arg;
	object_class->get_arg = ludwig_doc_part_get_arg;
}

static void
ludwig_doc_part_init (LudwigDocPart *part)
{
	part->doc = NULL;
	part->time_changes = NULL;
	part->key_changes = NULL;
	part->next = NULL;
	part->prev = NULL;
}

GtkType
ludwig_doc_part_get_type (void)
{
        static GtkType type = 0;

        if (!type)
        {
                GtkTypeInfo info =
                {
                        "LudwigDocPart",
                        sizeof (LudwigDocPart),
                        sizeof (LudwigDocPartClass),
                        (GtkClassInitFunc) ludwig_doc_part_class_init,
                        (GtkObjectInitFunc) ludwig_doc_part_init,
                        NULL, /* Reserved 1 */
                        NULL, /* Reserved 2 */
                        (GtkClassInitFunc) NULL
                };

                type = gtk_type_unique (ludwig_doc_container_get_type (), &info);
        }

        return type;
}

LudwigDocPart *
ludwig_doc_part_new (void)
{
        LudwigDocPart *ret;

        ret = gtk_type_new (ludwig_doc_part_get_type ());

        return ret;
}

void
ludwig_doc_part_add_measures (LudwigDocPart *part, gint n_measures)
{
        LudwigDocMeasure *measure;
        int n;

        for (n = 0; n < n_measures; n++)
        {
                measure = ludwig_doc_measure_new ();
                measure->measure_num = ++part->n_measures;
        }
}

void
ludwig_doc_part_remove_measure (LudwigDocPart *part, gint which)
{
}

LudwigDocMeasure
*ludwig_doc_part_get_measure (LudwigDocPart *part, gint n)
{
        GList *tmp;

        g_return_val_if_fail (LUDWIG_IS_DOC_PART (part), NULL);
        g_return_val_if_fail (part != NULL, NULL);

        tmp = g_list_nth (LUDWIG_DOC_CONTAINER (part)->items, n);
        if (!tmp)
                return NULL;
        else
                return tmp->data;
}
