/*
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_APP_H__
#define __LUDWIG_APP_H__

#include <gnome.h>
#include <bonobo.h>
#include <bonobo/bonobo-win.h>
#include <bonobo/bonobo-ui-util.h>

#include "ludwig-tool.h"
#include "ludwig-types.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define LUDWIG_TYPE_APP        (ludwig_app_get_type())
#define LUDWIG_APP(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_APP, LudwigApp))
#define LUDWIG_APP_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_APP, LudwigAppClass))
#define LUDWIG_IS_APP(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_APP))
#define LUDWIG_IS_APP_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_APP))

struct _LudwigApp
{
	BonoboWindow      parent_object;

	BonoboUIContainer *container;
        BonoboUIComponent *component;

        GtkWidget *standard_toolbar;
        GtkWidget *font_name_selector;
        GtkWidget *font_size_selector;
        GtkWidget *notebook;

        gboolean show_notebook_tabs;
        gboolean show_vertical_scrollbar;
        gboolean show_horizontal_scrollbar;

        LudwigDocument *doc;
        LudwigTool     *current_tool;

        GHashTable *views;
        gint        n_views;

        GArray     *profiles;
        gint        profile;

        LudwigZoomControl *zoom_control;
};

struct _LudwigAppClass
{
	BonoboWindowClass parent_class;
};

GtkType     ludwig_app_get_type            (void);
void        ludwig_app_create              (LudwigDocument *doc);
void        ludwig_app_exit                (void);
void        ludwig_app_close               (void);
void        ludwig_app_add_view            (LudwigView *view);
gint        ludwig_app_load                (const gchar *filename);
gint        ludwig_app_save                (const gchar *filename);

gchar      *ludwig_app_get_profile         (void);
void        ludwig_app_add_profile         (const char *profile);
void        ludwig_app_remove_profile      (const char *profile);

LudwigView *ludwig_app_get_current_view    (void);
LudwigDocument *ludwig_app_get_document    (void);
void        ludwig_app_create_canvas_popup (void);
void        ludwig_app_create_ruler_popup  (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __LUDWIG_APP_H__ */
