#ifndef GLYPHLIST_H
#define GLYPHLIST_H

/*
 * Device independent glyph placement language
 *
 * Author: Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright (C) 2000 Helix Code, Inc.
 *
 */

#include <ludwig-font/ludwig-font.h>

typedef GSList GFGlyphList;

typedef enum
{
   GFGL_INVALID,
   /* LudwigFont */
   GFGL_FONT,
   GFGL_LETTERSPACE,

   /* Glyph code */

   GFGL_GLYPH,
   GFGL_X,
   GFGL_Y,

   GFGL_PUSH_CP,
   GFGL_MAX_CP,

   /* Metrics */

   GFGL_ADVANCE,
   GFGL_KERNING
} GFGLCode;

typedef struct
{
   GFGLCode code;
   union
   {
      LudwigFont * font;
      gdouble letterspace;
      gint glyph;
      gint ival;
      gdouble dval;
      gdouble x;
      gdouble y;
   } value;
} GFGLElement;

typedef struct
{
   LudwigRFont * rfont;
   LudwigFontGlyph * glyph;
   gdouble letterspace;
   ArtPoint p, cp;
   GSList * cpstack;
} GFGLState;

typedef struct
{
   LudwigFontGlyph * glyph;
   gdouble x, y;
} GFGLPGlyph;

GFGlyphList *gfgl_text_to_list_simple       (LudwigFont *font,
                                             const gchar *text);
GFGlyphList *gfgl_text_to_list_simple_sized (LudwigFont *font,
                                             const gchar *text,
                                             gint length);
void         gfgl_free                      (GFGlyphList * list);
GFGlyphList *gfgl_duplicate                 (GFGlyphList *list);
GSList *gfgl_list_to_pglyphs                (GSList *glyphlist,
                                             gdouble affine[]);
void gfgl_pglyphs_free                      (GSList *pglyphs);

/* Debug */
void gfgl_list_describe                     (GFGlyphList * list);
void gfgl_pglyphs_describe                  (GSList * list);

#endif
