/*
 * ludwig-canvas-staff.c - Canvas item to draw a variable-lined staff.
 *
 * Copyright (C) 2000 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <config.h>
#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_svp_vpath_stroke.h>
#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-util.h>
#include "ludwig-canvas-staff.h"

#define DEFAULT_LINE_WIDTH  1.0   /* Make these customizeable */
#define DEFAULT_LINE_LENGTH 1100.0
#define DEFAULT_SPACE_WIDTH 12.0

typedef struct _LudwigCanvasStaffLine LudwigCanvasStaffLine;

struct _LudwigCanvasStaffLine
{
	GdkBitmap *stipple;
	ArtSVP    *fill_svp;
	guint32    fill_rgba;
};

struct _LudwigCanvasStaffPrivate
{
	GdkGC   *gc;
	ArtSVP  *fill_svp;
	guint32  fill_rgba;
	gdouble  coords[4];

	gint    n_lines;   /* How many lines is it? */
	GSList *lines;     /* The list of LudwigCanvasStaffLine */

	guint needs_update : 1;
};

enum
{
	ARG_0,
	ARG_NUM_LINES,
	ARG_X,
	ARG_Y
};

static void ludwig_canvas_staff_class_init (LudwigCanvasStaffClass *class);
static void ludwig_canvas_staff_init       (LudwigCanvasStaff      *staff);
static void ludwig_canvas_staff_destroy    (GtkObject              *object);
static void ludwig_canvas_staff_set_arg    (GtkObject              *object,
                                            GtkArg                 *arg,
                                            guint                   arg_id);
static void ludwig_canvas_staff_get_arg    (GtkObject              *object,
                                            GtkArg                 *arg,
                                            guint                   arg_id);
static void ludwig_canvas_staff_update     (GnomeCanvasItem        *item,
                                            double                 *affine,
                                            ArtSVP                 *clip_path,
                                            int                     flags);
static void ludwig_canvas_staff_realize    (GnomeCanvasItem        *item);
static void ludwig_canvas_staff_unrealize  (GnomeCanvasItem        *item);
static void ludwig_canvas_staff_draw       (GnomeCanvasItem        *item,
                                            GdkDrawable            *drawable,
                                            int                     x,
                                            int                     y,
                                            int                     width,
                                            int                     height);
static double ludwig_canvas_staff_point    (GnomeCanvasItem        *item,
                                            double                  x,
                                            double                  y,
                                            int                     cx,
                                            int                     cy,
                                            GnomeCanvasItem       **actual_item);
static void ludwig_canvas_staff_translate  (GnomeCanvasItem        *item,
                                            double                  dx,
                                            double                  dy);
static void ludwig_canvas_staff_bounds     (GnomeCanvasItem        *item,
                                            double                 *x1,
                                            double                 *y1,
                                            double                 *x2,
                                            double                 *y2);
static void ludwig_canvas_staff_render     (GnomeCanvasItem        *item,
                                            GnomeCanvasBuf         *buf);

static GnomeCanvasItemClass *parent_class;

GtkType
ludwig_canvas_staff_get_type (void)
{
	static GtkType staff_type = 0;

	if (!staff_type)
	{
		GtkTypeInfo staff_info =
		{
			"LudwigCanvasStaff",
			sizeof (LudwigCanvasStaff),
			sizeof (LudwigCanvasStaffClass),
			(GtkClassInitFunc)  ludwig_canvas_staff_class_init,
			(GtkObjectInitFunc) ludwig_canvas_staff_init,
			NULL, /* Reserved 1 */
			NULL, /* Reserved 2 */
			(GtkClassInitFunc) NULL
		};

		staff_type = gtk_type_unique (gnome_canvas_item_get_type(), &staff_info);
	}

	return staff_type;
}

static void
ludwig_canvas_staff_class_init (LudwigCanvasStaffClass *class)
{
	GtkObjectClass *object_class;
	GnomeCanvasItemClass *item_class;

	object_class = (GtkObjectClass *) class;
	item_class = (GnomeCanvasItemClass *) class;

	parent_class = gtk_type_class (gnome_canvas_item_get_type ());

	gtk_object_add_arg_type ("LudwigCanvasStaff::n_lines",
                                 GTK_TYPE_INT,
                                 GTK_ARG_READWRITE,
                                 ARG_NUM_LINES);
	gtk_object_add_arg_type ("LudwigCanvasStaff::x",
                                 GTK_TYPE_DOUBLE,
                                 GTK_ARG_READWRITE,
                                 ARG_X);
	gtk_object_add_arg_type ("LudwigCanvasStaff::y",
                                 GTK_TYPE_DOUBLE,
                                 GTK_ARG_READWRITE,
                                 ARG_Y);

	object_class->destroy = ludwig_canvas_staff_destroy;
	object_class->set_arg = ludwig_canvas_staff_set_arg;
	object_class->get_arg = ludwig_canvas_staff_get_arg;

	item_class->update = ludwig_canvas_staff_update;
	item_class->realize = ludwig_canvas_staff_realize;
	item_class->unrealize = ludwig_canvas_staff_unrealize;
	item_class->draw = ludwig_canvas_staff_draw;
	item_class->point = ludwig_canvas_staff_point;
	item_class->translate = ludwig_canvas_staff_translate;
	item_class->bounds = ludwig_canvas_staff_bounds;
	item_class->render = ludwig_canvas_staff_render;
}

static void
ludwig_canvas_staff_init (LudwigCanvasStaff *staff)
{
	staff->priv = g_new0 (LudwigCanvasStaffPrivate, 1);
	staff->priv->fill_rgba = 0x000000ff;
}

static void
ludwig_canvas_staff_destroy (GtkObject *object)
{
	LudwigCanvasStaff *staff;
	LudwigCanvasStaffPrivate *priv;
	LudwigCanvasStaffLine *line;
	GSList *slist;

	staff = (LudwigCanvasStaff *) object;
	priv = staff->priv;

	if (priv->lines)
	{
		slist = priv->lines;
		while (slist)
		{
			line = (LudwigCanvasStaffLine *) slist->data;

			gdk_bitmap_unref (line->stipple);
			g_free (line);
			slist = slist->next;
		}
	}

	g_slist_free (priv->lines);
	g_free (priv);

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
ludwig_canvas_staff_set_arg (GtkObject *object,
                             GtkArg *arg, guint arg_id)
{
	GnomeCanvasItem *item;
	LudwigCanvasStaff *staff;
	LudwigCanvasStaffPrivate *priv;
	LudwigCanvasStaffLine *line;
	int n;

	item = (GnomeCanvasItem *) object;
	staff = LUDWIG_CANVAS_STAFF (object);
	priv = staff->priv;

	switch(arg_id)
	{
		case ARG_NUM_LINES:
			priv->n_lines = GTK_VALUE_INT (*arg);
			for (n = 0; n < priv->n_lines; n++)
			{
				line = g_new0 (LudwigCanvasStaffLine, 1);
				line->stipple = NULL;
				line->fill_rgba = 0x000000ff;
				priv->lines = g_slist_prepend (priv->lines, line);
			}

			priv->needs_update = TRUE;
			gnome_canvas_item_request_update(item);

			break;

		case ARG_X:
			priv->coords[0] = GTK_VALUE_DOUBLE (*arg);
			priv->coords[2] = GTK_VALUE_DOUBLE (*arg) + DEFAULT_LINE_LENGTH;

			priv->needs_update = TRUE;
			gnome_canvas_item_request_update (item);

			break;

		case ARG_Y:
			priv->coords[1] = GTK_VALUE_DOUBLE (*arg);
			priv->coords[3] = GTK_VALUE_DOUBLE (*arg) + 5 * DEFAULT_SPACE_WIDTH;

			priv->needs_update = TRUE;
			gnome_canvas_item_request_update (item);

			break;

		default:
			break;
	}
}

static void
ludwig_canvas_staff_get_arg (GtkObject *object,
                             GtkArg *arg, guint arg_id)
{
	LudwigCanvasStaff *staff;
	LudwigCanvasStaffPrivate *priv;

	staff = (LudwigCanvasStaff *) object;
	priv = staff->priv;

	switch(arg_id)
	{
		case ARG_NUM_LINES:
			GTK_VALUE_INT (*arg) = priv->n_lines;
			break;

		case ARG_X:
			GTK_VALUE_DOUBLE(*arg) = priv->coords[0];
         		break;

		case ARG_Y:
			GTK_VALUE_DOUBLE(*arg) = priv->coords[1];
			break;

		default:
			break;
	}
}

static void
ludwig_canvas_staff_get_bounds (LudwigCanvasStaff *staff,
                                double *bx1, double *by1, double *bx2, double *by2)
{
	LudwigCanvasStaffPrivate *priv;
	double x1, y1, x2, y2;

	priv = staff->priv;

	x1 = priv->coords[0];
	y1 = priv->coords[1];
	x2 = priv->coords[0] + DEFAULT_LINE_LENGTH;
	y2 = priv->coords[1] + priv->n_lines * DEFAULT_SPACE_WIDTH;

	g_print("ludwig_canvas_staff_get_bounds()\n");
	g_print("     x1: %f\n     y1: %f\n     x2: %f\n     y2: %f\n",
		x1, y1, x2, y2);

	*bx1 = x1;
	*by1 = y1;
	*bx2 = x2;
	*by2 = y2;
}

static void
ludwig_canvas_staff_get_bounds_canvas (LudwigCanvasStaff *staff,
                                       double *bx1, double *by1, double *bx2, double *by2,
                                       double affine[6])
{
	GnomeCanvasItem *item;

	ArtDRect bbox_world;
	ArtDRect bbox_canvas;

	item = GNOME_CANVAS_ITEM (staff);

	ludwig_canvas_staff_get_bounds (staff, &bbox_world.x0, &bbox_world.y0,
                                        &bbox_world.x1, &bbox_world.y1);

	art_drect_affine_transform (&bbox_canvas, &bbox_world, affine);

	*bx1 = bbox_canvas.x0 - 1;
	*by1 = bbox_canvas.y0 - 1;
	*bx2 = bbox_canvas.x1 + 1;
	*by2 = bbox_canvas.y1 + 1;

	g_print ("get_bounds_canvas()\n");
	g_print ("   bx1: %f\n   by1: %f\n   bx2: %f\n   by1: %f\n",
		 *bx1, *by1, *bx2, *by2);
}

static void
ludwig_canvas_staff_recalc_bounds (LudwigCanvasStaff *staff)
{
	GnomeCanvasItem *item;
	double x1, y1, x2, y2;
	int cx1, cy1, cx2, cy2;
	double dx, dy;

	item = GNOME_CANVAS_ITEM (staff);

	if (staff->priv->n_lines == 0)
	{
		item->x1 = item->y1 = item->x2 = item->y2 = 0;
		return;
	}

	/* Get the bounds in world coordinates */
	ludwig_canvas_staff_get_bounds (staff, &x1, &y1, &x2, &y2);

	/* Convert to canvas pixel coords */
	dx = dy = 0.0;
	gnome_canvas_item_i2w (item, &dx, &dy);

	gnome_canvas_w2c (GNOME_CANVAS (staff), x1 + dx, y1 + dy, &cx1, &cy1);
	gnome_canvas_w2c (GNOME_CANVAS (staff), x2 + dx, y2 + dy, &cx2, &cy2);
	item->x1 = cx1;
	item->y1 = cy1;
	item->x2 = cx2;
	item->y2 = cy2;

	item->x1--;
	item->y1--;
	item->x2--;
	item->y2--;

	g_print ("recalc_bounds()\n");
	g_print ("   x1: %f\n   y1: %f\n   x2: %f\n   y2: %f\n",
		 item->x1, item->y1, item->x2, item->y2);

	gnome_canvas_group_child_bounds (GNOME_CANVAS_GROUP (item->parent), item);
}

static void
ludwig_canvas_staff_render (GnomeCanvasItem *item,
                            GnomeCanvasBuf *buf)
{
	LudwigCanvasStaff *staff;
	LudwigCanvasStaffPrivate *priv;
	LudwigCanvasStaffLine *line;
	GSList *tmp;
	int n;

	staff = LUDWIG_CANVAS_STAFF (item);
	priv = staff->priv;

	if (priv->fill_svp)
		gnome_canvas_render_svp (buf, priv->fill_svp, priv->fill_rgba);

	for (n = 0, tmp = priv->lines; tmp; tmp = tmp->next)
	{
		line = (LudwigCanvasStaffLine *) tmp->data;
		if (line->fill_svp != NULL)
		{
			gnome_canvas_render_svp (buf, line->fill_svp, line->fill_rgba);
		}
	}

}

static void
ludwig_canvas_staff_update (GnomeCanvasItem *item,
                            double *affine,
                            ArtSVP *clip_path,
                            int flags)
{
	LudwigCanvasStaff     *staff;
	LudwigCanvasStaffLine *line;
	ArtVpath               vpath[3];
        ArtVpath              *vpath2;
	ArtSVP                *svp;
	ArtPoint               pi, pc;
	int                    i;
	GSList                *tmp;
	int                    x1, y1, x2, y2;

	staff = LUDWIG_CANVAS_STAFF(item);
	x1 = y1 = x2 = y2 = 0;

	if(parent_class->update)
		(* parent_class->update) (item, affine, clip_path, flags);

	if(item->canvas->aa)
	{
		double space_width;
		double line_length;
		double line_width;

		gnome_canvas_item_reset_bounds(item);

		pi.x = staff->priv->coords[0];
		pi.y = staff->priv->coords[1];
		art_affine_point(&pc, &pi, affine);

		space_width = DEFAULT_SPACE_WIDTH * art_affine_expansion (affine);
		line_length = DEFAULT_LINE_LENGTH * art_affine_expansion (affine);
		line_width  = DEFAULT_LINE_WIDTH  * art_affine_expansion (affine);

		tmp = staff->priv->lines;
		i = 0;

		while(tmp != NULL)
		{
			vpath[0].code = ART_MOVETO;
			vpath[0].x = pc.x;
			vpath[0].y = pc.y + i * space_width;
			vpath[1].code = ART_LINETO;
			vpath[1].x = pc.x + line_length;
			vpath[1].y = pc.y + i * space_width;
			vpath[2].code = ART_END;
			vpath[2].x = 0;
			vpath[2].y = 0;

                        vpath2 = art_vpath_affine_transform (vpath, affine);

			line = (LudwigCanvasStaffLine *) tmp->data;

			svp = art_svp_vpath_stroke (vpath2,
                                                    gnome_canvas_join_gdk_to_art (GDK_JOIN_ROUND),
                                                    gnome_canvas_cap_gdk_to_art (GDK_CAP_BUTT),
                                                    line_width,
                                                    4,
                                                    0.25);

			gnome_canvas_item_update_svp_clip (item, &line->fill_svp, svp, clip_path);

			i++;
			tmp = g_slist_next (tmp);
		}

		if (((flags & GNOME_CANVAS_UPDATE_VISIBILITY) &&
			!(GTK_OBJECT_FLAGS(item) & GNOME_CANVAS_ITEM_VISIBLE))
			|| (flags & GNOME_CANVAS_UPDATE_AFFINE)
			|| staff->priv->needs_update == TRUE)
		{
			gnome_canvas_request_redraw (item->canvas, item->x1 - 1, item->y1 - 1, item->x2 + 1, item->y2 + 1);
		}
	}
}

static void
ludwig_canvas_staff_realize (GnomeCanvasItem *item)
{
	LudwigCanvasStaff *staff;

	staff = LUDWIG_CANVAS_STAFF(item);

	if (parent_class->realize)
		(* parent_class->realize) (item);

	if(!item->canvas->aa)
		staff->priv->gc = gdk_gc_new (item->canvas->layout.bin_window);
}

static void
ludwig_canvas_staff_unrealize (GnomeCanvasItem *item)
{
	LudwigCanvasStaff *staff;

	staff = LUDWIG_CANVAS_STAFF(item);

	gdk_gc_unref(staff->priv->gc);
	staff->priv->gc = NULL;

	if(parent_class->unrealize)
		(* parent_class->unrealize) (item);
}

static void
item_to_canvas (GnomeCanvas *canvas, double *item_coords,
                GdkPoint *canvas_coords, int num_points,
                int *num_drawn_points, double i2c[6])
{
}

static void
ludwig_canvas_staff_draw (GnomeCanvasItem *item,
                          GdkDrawable *drawable,
                          int x, int y,
                          int width, int height)
{
}

static double
ludwig_canvas_staff_point (GnomeCanvasItem *item,
                           double x, double y,
                           int cx, int cy,
                           GnomeCanvasItem **actual_item)
{
	LudwigCanvasStaff *staff;
	double best, dist;
	double dx, dy;

	staff = LUDWIG_CANVAS_STAFF (item);
	*actual_item = item;

	best = 1.0e36;

	/* Finish this.. blah blah blah. */

	return best;
}

static void
ludwig_canvas_staff_translate (GnomeCanvasItem *item,
                               double dx, double dy)
{
	LudwigCanvasStaff *staff;
	double *coords;

	staff = LUDWIG_CANVAS_STAFF (item);

	coords = staff->priv->coords;

	coords[0] += dx;
	coords[1] += dy;
	coords[2] += dx;
	coords[3] += dy;

	ludwig_canvas_staff_recalc_bounds (staff);
}

static void
ludwig_canvas_staff_bounds (GnomeCanvasItem *item,
                            double *x1, double *y1,
                            double *x2, double *y2)
{
	LudwigCanvasStaff *staff;

	staff = LUDWIG_CANVAS_STAFF (item);

	if (staff->priv->n_lines == 0)
	{
		*x1 = *y1 = *x2 = *y2 = 0.0;
		return;
	}

	ludwig_canvas_staff_get_bounds (staff, x1, y1, x2, y2);
}
