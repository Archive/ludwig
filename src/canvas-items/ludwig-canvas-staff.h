/*
 * ludwig-canvas-staff.c - Canvas item to draw a variable-lined staff.
 *
 * Copyright (C) 2000 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_CANVAS_STAFF_H__
#define __LUDWIG_CANVAS_STAFF_H__

#include <libgnomeui/gnome-canvas.h>

#define LUDWIG_TYPE_CANVAS_STAFF         (ludwig_canvas_staff_get_type())
#define LUDWIG_CANVAS_STAFF(o)           (GTK_CHECK_CAST((o), LUDWIG_TYPE_CANVAS_STAFF, LudwigCanvasStaff))
#define LUDWIG_CANVAS_STAFF_CLASS(k)     (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_CANVAS_STAFF, LudwigCanvasStaffClass))
#define LUDWIG_IS_CANVAS_STAFF(o)        (GTK_CHECK_TYPE((o), LUDWIG_TYPE_CANVAS_STAFF))
#define LUDWIG_IS_CANVAS_STAFF_CLASS(k)  (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_CANVAS_STAFF))
#define LUDWIG_CANVAS_STAFF_GET_CLASS(o) (GTK_CHECK_GET_CLASS((o), LUDWIG_TYPE_CANVAS_STAFF, LudwigCanvasStaffClass))

typedef struct _LudwigCanvasStaff        LudwigCanvasStaff;
typedef struct _LudwigCanvasStaffClass   LudwigCanvasStaffClass;
typedef struct _LudwigCanvasStaffPrivate LudwigCanvasStaffPrivate;

struct _LudwigCanvasStaff
{
	GnomeCanvasItem parent_object;

	LudwigCanvasStaffPrivate *priv;
};

struct _LudwigCanvasStaffClass
{
	GnomeCanvasItemClass parent_class;
};

/* Public interface */
GtkType ludwig_canvas_staff_get_type(void);
void    ludwig_canvas_staff_set_line_stipple (LudwigCanvasStaff *staff,
                                              gint line, GdkBitmap *stipple);
void    ludwig_canvas_staff_set_line_color   (LudwigCanvasStaff *staff,
                                              gint line, guint32 rgba);

#endif /* __LUDWIG_CANVAS_STAFF_H__ */
