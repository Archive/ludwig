#include "ludwig-display-metrics.h"

gdouble
ludwig_absolute_metric_to_metric (gdouble length_src, LudwigDisplayMetric metric_src, LudwigDisplayMetric metric_dest)
{
	gdouble src, dst;

	src = dst = 1.0;

	switch (metric_src)
	{
        case METRIC_MM:
                src = MM_PER_IN;
                break;

        case METRIC_CM:
                src = CM_PER_IN;
                break;

        case METRIC_IN:
                src = IN_PER_IN;
                break;

        case METRIC_PT:
                src = PT_PER_IN;
                break;

        case METRIC_NONE:
                src = 1.0;
                break;
        }

        switch (metric_dest)
        {
        case METRIC_MM:
                dst = MM_PER_IN;
                break;

        case METRIC_CM:
                dst = CM_PER_IN;
                break;

        case METRIC_IN:
                dst = IN_PER_IN;
                break;

        case METRIC_PT:
                dst = PT_PER_IN;
                break;

        case METRIC_NONE:
                dst = 1.0;
                break;
        }

        return length_src * (dst / src);
}

GString *
ludwig_display_metric_to_metric_string (gdouble length, LudwigDisplayMetric metric_src, LudwigDisplayMetric metric_dst, gboolean m)
{
        GString *str;
        gdouble len;

        len = ludwig_absolute_metric_to_metric (length, metric_src, metric_dst);
        str = g_string_new ("");

        switch (metric_dst)
        {
        case METRIC_MM:
                g_string_sprintf (str, "%0.2f%s", len, m?" mm":"");
                break;

        case METRIC_CM:
                g_string_sprintf (str, "%0.2f%s", len, m?" cm":"");
                break;

        case METRIC_IN:
                g_string_sprintf (str, "%0.2f%s", len, m?" \"":"");
                break;

        case METRIC_PT:
                g_string_sprintf (str, "%0.2f%s", len, m?" pt":"");
                break;

        case METRIC_NONE:
                g_string_sprintf (str, "%s", "ups!");
                break;
        }

        return str;
}
