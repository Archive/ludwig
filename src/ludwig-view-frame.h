/*
 * Copyright (C) 2000 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_VIEW_FRAME_H__
#define __LUDWIG_VIEW_FRAME_H__

#include <gnome.h>
#include <bonobo.h>

#include "ludwig-types.h"

#define LUDWIG_TYPE_VIEW_FRAME        (ludwig_view_frame_get_type())
#define LUDWIG_VIEW_FRAME(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_VIEW_FRAME, LudwigViewFrame))
#define LUDWIG_VIEW_FRAME_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_VIEW_FRAME, LudwigViewFrameClass))
#define LUDWIG_IS_VIEW_FRAME(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_VIEW_FRAME))
#define LUDWIG_IS_VIEW_FRAME_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_VIEW_FRAME))

/* TODO: Implement a BonoboControl and BonoboPersistStream */

struct _LudwigViewFrame
{
	GtkScrolledWindow parent_object;

        GtkWidget *table;

        BonoboUIContainer     *uic;
	BonoboControl         *control;
	BonoboZoomableFrame   *zoomable;

        GtkWidget *hruler;
        GtkWidget *vruler;

	gchar     *name;
	GSList    *undo_cmds;
	GSList    *redo_cmds;

	gchar     *zoom_desc;
        float      zoom_level;
        gint       zoom_list_num;
};

struct _LudwigViewFrameClass
{
	GtkScrolledWindowClass parent_class;
};

GtkType          ludwig_view_frame_get_type      (void);
LudwigViewFrame *ludwig_view_frame_new           (const gchar *name, BonoboUIContainer *container, LudwigView *view);

#endif /* __LUDWIG_VIEW_FRAME_H__ */
