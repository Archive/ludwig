/*
 * Copyright (C) 2000 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_ELEMENT_H__
#define __LUDWIG_ELEMENT_H__

#include <gtk/gtkobject.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define LUDWIG_TYPE_ELEMENT        (ludwig_element_get_type())
#define LUDWIG_ELEMENT(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_ELEMENT, LudwigElement))
#define LUDWIG_ELEMENT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_ELEMENT, LudwigElementClass))
#define LUDWIG_IS_ELEMENT(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_ELEMENT))
#define LUDWIG_IS_ELEMENT_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_ELEMENT))

typedef struct _LudwigElement      LudwigElement;
typedef struct _LudwigElementClass LudwigElementClass;

struct _LudwigElement
{
	GtkObject      object;

	LudwigElement *parent;
	GList         *children;
	gint           n_children;

        gint           layer;
};

struct _LudwigElementClass
{
	GtkObjectClass parent_class;

	void (* insert_element)  (LudwigElement *element, LudwigElement *new);
	void (* append_element)  (LudwigElement *element, LudwigElement *new);
};

/* Public interface */
GtkType        ludwig_element_get_type            (void);
LudwigElement *ludwig_element_new                 (LudwigElement *parent);
void           ludwig_element_add_child           (LudwigElement *element,
                                                   LudwigElement *new);
void           ludwig_element_remove_child        (LudwigElement *element,
                                                   LudwigElement *child);
void           ludwig_element_reparent            (LudwigElement *element,
                                                   LudwigElement *parent);
gboolean       ludwig_element_is_balanced         (LudwigElement *element);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __LUDWIG_ELEMENT_H__ */
