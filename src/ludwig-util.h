/*
 * Copyright (C) 2000 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_UTIL_H__
#define __LUDWIG_UTIL_H__

#include <glib.h>
#include <ludwig-font/ludwig-font.h>
#include "ludwig-types.h"
#include <gnome-xml/tree.h>

LudwigFont *ludwig_get_standard_font     (void);
guint       ludwig_strcase_hash          (gconstpointer p);
gint        ludwig_strcase_equal         (gconstpointer p, gconstpointer p2);
xmlNode    *ludwig_xml_get_children      (xmlNode *parent);
xmlNode    *ludwig_xml_get_root_children (xmlDoc *doc);
gchar      *ludwig_clef_to_string        (LudwigClef clef);
LudwigClef  ludwig_clef_from_string      (const char *str);

#endif /* __LUDWIG_UTIL_H__ */
