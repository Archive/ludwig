/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_TOOL_H__
#define __LUDWIG_TOOL_H__

#include <gtk/gtkobject.h>
#include <gtk/gtksignal.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "ludwig-types.h"
#include "ludwig-view.h"

#define LUDWIG_TYPE_TOOL        (ludwig_tool_get_type())
#define LUDWIG_TOOL(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_TOOL, LudwigTool))
#define LUDWIG_TOOL_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_TOOL, LudwigToolClass))
#define LUDWIG_IS_TOOL(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_TOOL))
#define LUDWIG_IS_TOOL_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_TOOL))

typedef struct _LudwigTool      LudwigTool;
typedef struct _LudwigToolClass LudwigToolClass;

typedef enum
{
        INACTIVE,
        ACTIVE,
        PAUSED
} ToolState;

typedef enum
{
        PAUSE,
        RESUME,
        HALT,
        DESTROY,
        RECREATE
} ToolAction;

typedef enum
{
        TARGET_ANY      = 1 << 0,
        TARGET_PART     = 1 << 1,
        TARGET_STAFF    = 1 << 2,
        TARGET_MEASURE  = 1 << 3,
} ToolTarget;

struct _LudwigTool
{
        GtkObject    parent_object;

        LudwigView  *view;
        GtkWidget   *canvas;
        ToolState    state;
        ToolTarget   target;

        GdkPixbuf   *pixbuf;

        gchar       *tool_name;
};

struct _LudwigToolClass
{
	GtkObjectClass parent_class;

	void (* initialize)     (LudwigTool *tool, LudwigView *view);
	void (* button_press)   (LudwigTool *tool,
                                 GdkEventButton *button,
                                 LudwigView *view);
	void (* button_release) (LudwigTool *tool,
                                 GdkEventButton *button,
                                 LudwigView *view);
	void (* motion)         (LudwigTool *tool,
                                 GdkEventMotion *motion,
                                 LudwigView *view);
};

/* Public API */
GtkType     ludwig_tool_get_type   (void);

GdkPixbuf  *ludwig_tool_get_pixbuf (LudwigTool *tool);
void        ludwig_tool_set_pixbuf (LudwigTool *tool, GdkPixbuf *pixbuf);

#endif /* __LUDWIG_TOOL_H__ */
