/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#ifndef __LUDWIG_DOC_ITEM_H__
#define __LUDWIG_DOC_ITEM_H__

#include <gtk/gtkobject.h>
#include <gtk/gtksignal.h>

#include "ludwig-types.h"

#define LUDWIG_TYPE_DOC_ITEM        (ludwig_doc_item_get_type())
#define LUDWIG_DOC_ITEM(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_DOC_ITEM, LudwigDocItem))
#define LUDWIG_DOC_ITEM_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_DOC_ITEM, LudwigDocItemClass))
#define LUDWIG_IS_DOC_ITEM(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_DOC_ITEM))
#define LUDWIG_IS_DOC_ITEM_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_DOC_ITEM))

struct _LudwigDocItem
{
	GtkObject parent_object;

	LudwigDocContainer *container;

	LudwigDocItem *next;
	LudwigDocItem *prev;
};

struct _LudwigDocItemClass
{
	GtkObjectClass parent_class;

	void (* get_bounds) (LudwigDocItem *item, double *x1, double *y1, double *x2, double *y2);
};

GtkType        ludwig_doc_item_get_type   (void);
LudwigDocItem *ludwig_doc_item_new        (LudwigDocContainer *container);
LudwigDocItem *ludwig_doc_item_root       (LudwigDocItem *item);
void           ludwig_doc_item_get_bounds (LudwigDocItem *item, double *x1, double *y1, double *x2, double *y2);

#endif /* __LUDWIG_DOC_ITEM_H__ */
