/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <ludwig-font/ludwig-font.h>
#include <ludwig-font/ludwig-font-glyph.h>
#include <ludwig-font/ludwig-rfont.h>

#include <gnome.h>

#include "ludwig-util.h"
#include "canvas-items/gp-path.h"
#include "canvas-items/gnome-canvas-bpath.h"

GtkWidget *canvas;
GnomeCanvasItem *item;
GnomeCanvasGroup *root;

void change_value (GtkAdjustment *adj, GtkWidget *spin)
{
	LudwigFont *font;
	LudwigFontGlyph *glyph;
	int code;
	GPPath *path;
	double affine[6];

	if (item)
	{
		gtk_object_destroy (GTK_OBJECT (item));
	}

	font = (LudwigFont *) ludwig_get_standard_font ();
	code = ludwig_font_face_lookup (font->face, 0, gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (spin)));
	g_print ("Code is %d\n", code);
	glyph = ludwig_vfont_get_glyph ((LudwigVFont *) font, code);
	path = gp_path_new_from_foreign_bpath (ludwig_font_glyph_get_bpath (glyph, TRUE));

	item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (root),
                                      gnome_canvas_bpath_get_type (),
                                      "bpath", path,
                                      "fill_color_rgba", 0x00ff,
                                      NULL);

	art_affine_scale (affine, 1.0, 1.0);

//	affine[3] = -1.0;
	affine[4] = 100.0;
	affine[5] = 100.0;
	gnome_canvas_item_affine_absolute (item, affine);

	gtk_widget_queue_draw (canvas);
}

int main (int argc, char *argv[])
{
	GtkWidget *window;
	GtkWidget *vbox;
	GtkWidget *spin;
	GtkAdjustment *adj;

	gnome_init ("LudwigTTFView", "0.1", argc, argv);

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (window), "Ludwig TTF Font Viewer");
	gtk_signal_connect (GTK_OBJECT (window), "delete_event",
                            GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

	canvas = gnome_canvas_new_aa ();
	gtk_widget_set_usize (canvas, 500, 500);
	root = gnome_canvas_root (GNOME_CANVAS (canvas));
	gtk_widget_show (canvas);
	gnome_canvas_set_pixels_per_unit (GNOME_CANVAS (canvas), 2);

	gnome_canvas_set_scroll_region (GNOME_CANVAS (canvas), 0, 0, 800, 800);

	adj = (GtkAdjustment *) gtk_adjustment_new (1.0, 0.0, 256.0, 1.0, 1.0, 256.0);

	spin = gtk_spin_button_new (adj, 1.0, 0);
	gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (spin), FALSE);
	gtk_spin_button_set_shadow_type (GTK_SPIN_BUTTON (spin), GTK_SHADOW_OUT);

	gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
                            GTK_SIGNAL_FUNC (change_value), spin);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), canvas, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), spin, FALSE, FALSE, 0);

	gtk_container_add (GTK_CONTAINER (window), vbox);

	gtk_widget_show_all (window);

	gtk_main ();

	return 0;
}
