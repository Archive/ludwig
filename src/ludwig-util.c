/*
 * Copyright (C) 2000 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include "ludwig-util.h"
#include <ludwig-font/ludwig-font.h>

#define FONTFILE  FONTDIR "fughetta.ttf"

LudwigFont *
ludwig_get_standard_font (void)
{
	LudwigFontFace *face;
	LudwigFont     *font;

	face = ludwig_font_face_new_from_file (FONTFILE,
                                               NULL, 0);
	if (!ludwig_font_face_set_encoding (face, 0, NULL, LUDWIG_FONT_ENCODING_APPLE_ROMAN))
	{
		g_print ("Cannot display that encoding\n");
	}

	font = ludwig_font_face_get_font (face, 48.0, 72.0, 72.0);

	return font;
}

guint
ludwig_strcase_hash (gconstpointer v)
{
	const guchar *s = (const guchar *) v;
	const guchar *p;
	guint h = 0, g;

	for (p = s; *p != '\0'; p += 1)
	{
		h = (h << 4) + tolower(*p);
		if ((g = h & 0xf0000000))
		{
			h = h ^ (g >> 24);
			h = h ^ g;
		}
	}

	return h;
}

gint
ludwig_strcase_equal (gconstpointer v, gconstpointer v2)
{
	return g_strcasecmp ((const gchar *) v, (const gchar *) v2) == 0;
}

xmlNode *
ludwig_xml_get_children (xmlNode *parent)
{
	return parent != NULL ? parent->childs : NULL;
}

xmlNode *
ludwig_xml_get_root_children (xmlDoc *doc)
{
	return ludwig_xml_get_children (xmlDocGetRootElement (doc));
}

gchar *
ludwig_clef_to_string (LudwigClef clef)
{
	switch (clef)
	{
		case CLEF_NONE:
			return "";

		case CLEF_TREBLE:
			return "treble";

		case CLEF_ALTO:
			return "alto";

		case CLEF_TENOR:
			return "tenor";

		case CLEF_BASS:
			return "bass";

		default:
			return "error";
	}
}

LudwigClef
ludwig_clef_from_string (const char *str)
{
	if (str == NULL)
		return CLEF_NONE;

	if (strcmp (str, "treble") == 0)
		return CLEF_TREBLE;
	else if (strcmp (str, "alto") == 0)
		return CLEF_ALTO;
	else if (strcmp (str, "tenor") == 0)
		return CLEF_TENOR;
	else if (strcmp (str, "bass") == 0)
		return CLEF_BASS;
	else
		return CLEF_NONE;
}
