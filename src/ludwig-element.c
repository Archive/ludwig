/*
 * Copyright (C) 2000 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <gtk/gtkobject.h>
#include <gtk/gtksignal.h>
#include "ludwig-document.h"
#include "ludwig-element.h"

enum
{
	INSERT_ELEMENT,
	APPEND_ELEMENT,
	LAST_SIGNAL
};

static GtkObjectClass *parent_class = NULL;
static guint signals[LAST_SIGNAL];

static void ludwig_element_destroy(GtkObject *object);

static void
ludwig_element_class_init (LudwigElementClass *class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	parent_class = gtk_type_class(gtk_object_get_type());

	object_class->destroy = ludwig_element_destroy;

	signals[INSERT_ELEMENT] =
		gtk_signal_new ("insert_element",
                                GTK_RUN_FIRST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (LudwigElementClass,
                                                   insert_element),
                                gtk_marshal_NONE__NONE,
                                GTK_TYPE_NONE, 0);
	signals[APPEND_ELEMENT] =
		gtk_signal_new ("append_element",
                                GTK_RUN_FIRST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (LudwigElementClass,
                                                   append_element),
                                gtk_marshal_NONE__NONE,
                                GTK_TYPE_NONE, 0);
	gtk_object_class_add_signals (object_class,
                                      signals,
                                       LAST_SIGNAL);
}

static void
ludwig_element_init (LudwigElement *element)
{
}

static void
ludwig_element_destroy (GtkObject *object)
{
	LudwigElement *element;

	g_return_if_fail(object != NULL);
	g_return_if_fail(LUDWIG_IS_ELEMENT(object));

	element = LUDWIG_ELEMENT (object);

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

/*
 * Public interfaces
 */

GtkType
ludwig_element_get_type (void)
{
	static GtkType element_type = 0;

	if (!element_type)
	{
		const GtkTypeInfo element_info =
		{
			"LudwigElement",
			sizeof (LudwigElement),
			sizeof (LudwigElementClass),
			(GtkClassInitFunc)  ludwig_element_class_init,
			(GtkObjectInitFunc) ludwig_element_init,
			NULL,  /* Reserved 1 */
			NULL,  /* Reserved 2 */
			(GtkClassInitFunc) NULL,
		};

		element_type = gtk_type_unique (gtk_object_get_type (),
                                                &element_info);
	}

	return element_type;
}

LudwigElement *
ludwig_element_new (LudwigElement *parent)
{
	LudwigElement *ret;

	ret = gtk_type_new (LUDWIG_TYPE_ELEMENT);
	ret->parent = parent;
	ret->children = NULL;

	return ret;
}

void 
ludwig_element_add_child (LudwigElement *element, LudwigElement *child)
{
}

void 
ludwig_element_reparent (LudwigElement *element, LudwigElement *parent)
{
}

void
ludwig_element_remove_child (LudwigElement *element, LudwigElement *child)
{
}

gboolean
ludwig_element_is_balanced (LudwigElement *element)
{
}
