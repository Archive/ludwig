/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_TOOL_MANAGER_H__
#define __LUDWIG_TOOL_MANAGER_H__

#define LUDWIG_TYPE_TOOL_MANAGER        (ludwig_tool_manager_get_type())
#define LUDWIG_TOOL_MANAGER(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_TOOL_MANAGER, LudwigToolManager))
#define LUDWIG_TOOL_MANAGER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_TOOL_MANAGER, LudwigToolManagerClass))
#define LUDWIG_IS_TOOL_MANAGER(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_TOOL_MANAGER))
#define LUDWIG_IS_TOOL_CLASS_MANAGER(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_TOOL_MANAGER))

#include <gtk/gtkobject.h>

#include "ludwig-types.h"
#include "ludwig-tool.h"

typedef struct _LudwigToolManager      LudwigToolManager;
typedef struct _LudwigToolManagerClass LudwigToolManagerClass;

struct _LudwigToolManager
{
	GtkObject parent_object;

        GHashTable *tools;
        GSList     *tool_stack;
        LudwigTool *current_tool;
};

struct _LudwigToolManagerClass
{
	GtkObjectClass parent_class;
};

GtkType     ludwig_tool_manager_get_type         (void);
void        ludwig_tool_manager_create           (void);
void        ludwig_tool_manager_shutdown         (void);
void        ludwig_tool_manager_select_tool      (LudwigTool *tool);
void        ludwig_tool_manager_push_tool        (LudwigTool *tool);
void        ludwig_tool_manager_pop_tool         (void);
LudwigTool *ludwig_tool_manager_get_tool         (void);
LudwigTool *ludwig_tool_manager_get_tool_by_name (const char *name);
int         ludwig_tool_manager_motion           (GtkWidget *widget,
                                                  GdkEventMotion *evt,
                                                  gpointer data);

#endif /* __LUDWIG_TOOL_MANAGER_H__ */
