/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#ifndef __LUDWIG_DOC_PART_H__
#define __LUDWIG_DOC_PART_H__

#include "ludwig-types.h"
#include "ludwig-doc-container.h"
#include "ludwig-document.h"
#include "ludwig-instrument.h"

#define LUDWIG_TYPE_DOC_PART        (ludwig_doc_part_get_type())
#define LUDWIG_DOC_PART(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_DOC_PART, LudwigDocPart))
#define LUDWIG_DOC_PART_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_DOC_PART, LudwigDocPartClass))
#define LUDWIG_IS_DOC_PART(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_DOC_PART))
#define LUDWIG_IS_DOC_PART_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_DOC_PART))

struct _LudwigDocPart
{
	LudwigDocContainer parent_object;

	LudwigDocument *doc;
        LudwigInstrument *instrument;

        gint n_measures;

        GList *time_changes;   /* A list of measures that have had changes
                                * to time signature and to what time
                                * signature they change.
                                */

        GList *key_changes;    /* A list of measures containing changes to
                                * key, what beat those changes occur on,
                                * and to what key they change.
                                */

	LudwigDocPart *next;
	LudwigDocPart *prev;
};

struct _LudwigDocPartClass
{
	LudwigDocContainerClass parent_class;

        void (* measure_added)  (LudwigDocPart *part,
                                 gint measure_num,
                                 gpointer data);
};

GtkType           ludwig_doc_part_get_type       (void);
LudwigDocPart    *ludwig_doc_part_new            (void);
void              ludwig_doc_part_add_measures   (LudwigDocPart *part, gint n_measures);
void              ludwig_doc_part_remove_measure (LudwigDocPart *part, gint which);
LudwigDocMeasure *ludwig_doc_part_get_measure    (LudwigDocPart *part, gint n);

#endif /* __LUDWIG_DOC_PART_H__ */
