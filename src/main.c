/*
 * Copyright (C) 2000 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <bonobo.h>
#include <gconf/gconf.h>
#include "ludwig.h"
#include "ludwig-app.h"
#include "ludwig-document.h"

gboolean ludwig_font_init_library (void);

static void
delete_cb (GtkWidget *widget, gpointer data)
{
	gtk_widget_destroy (data);
	widget = NULL;
}

int
main (int argc, char *argv[])
{
	GtkWidget      *app;
	CORBA_ORB       orb;

	orb = oaf_init (argc, argv);
	gnome_init (PACKAGE, VERSION, argc, argv);
        gconf_init (argc, argv, NULL);

	bonobo_init (orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL);
	bonobo_activate ();
	ludwig_font_init_library ();

	/* Create the app */
	ludwig_app_create (NULL);
	ludwig_config_set_profile ("professional");

	gtk_main ();

        return 0;
}
