/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include "ludwig-config.h"
#include "ludwig-instrument.h"

static GtkObjectClass *parent_class = NULL;

static void
profiles_destroy (gpointer key, gpointer value, gpointer data)
{
	LudwigInstrumentProfile *profile = value;

	g_slist_free (profile->all_clefs);
	g_free (profile);
}

static void
names_destroy (gpointer key, gpointer value, gpointer data)
{
	LudwigInstrumentName *name = value;

	g_free (name->locale);
	g_free (name->name);
	g_free (name->abbreviation);
	g_free (name);
}

static void
ludwig_instrument_destroy (GtkObject *object)
{
	LudwigInstrument *instrument = (LudwigInstrument *) object;

	g_hash_table_foreach (instrument->profiles, &profiles_destroy, NULL);
	g_hash_table_foreach (instrument->names, &names_destroy, NULL);
	g_free (instrument->group);
}

static void
ludwig_instrument_init (GtkObject *object)
{
	LudwigInstrument *instrument = (LudwigInstrument *) object;

	instrument->profiles = g_hash_table_new (ludwig_strcase_hash, ludwig_strcase_equal);
	instrument->names = g_hash_table_new (ludwig_strcase_hash, ludwig_strcase_equal);
	instrument->group = NULL;
}

static void
ludwig_instrument_class_init (LudwigInstrumentClass *class)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (class);
	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = ludwig_instrument_destroy;
}

GtkType
ludwig_instrument_get_type (void)
{
	static GtkType type = 0;

	if (!type)
	{
		GtkTypeInfo info =
		{
			"LudwigInstrument",
			sizeof (LudwigInstrument),
			sizeof (LudwigInstrumentClass),
			(GtkClassInitFunc)  ludwig_instrument_class_init,
			(GtkObjectInitFunc) ludwig_instrument_init,
			NULL,  /* Reserved 1 */
			NULL,  /* Reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gtk_object_get_type (), &info);
	}

	return type;
}

LudwigInstrument *
ludwig_instrument_new (const char *id, int pos, const char *group)
{
	LudwigInstrument *ret;

	ret = gtk_type_new (LUDWIG_TYPE_INSTRUMENT);
	ret->group = g_strdup (group);
        ret->id = g_strdup (id);
        ret->pos = pos;

	return ret;
}

LudwigClef
ludwig_instrument_query_default_clef (LudwigInstrument *instrument)
{
        LudwigInstrumentProfile *profile;
        gchar *profile_name;

        profile_name = ludwig_config_query_profile ();
        profile = g_hash_table_lookup (instrument->profiles, profile_name);
        g_free (profile_name);

        return profile->default_clef;
}

gboolean
ludwig_instrument_is_valid_clef (LudwigInstrument *instrument, LudwigClef clef)
{
        LudwigInstrumentProfile *profile;
        gchar *profile_name;
        GSList *tmp;

        profile_name = ludwig_config_query_profile ();
        profile = g_hash_table_lookup (instrument->profiles, profile_name);
        g_free (profile_name);

        tmp = profile->all_clefs;
        while (tmp != NULL)
        {
                if (GPOINTER_TO_INT (tmp->data) == clef)
                        return TRUE;
        }

        return FALSE;
}

gboolean
ludwig_instrument_is_valid_pitch (LudwigInstrument *instrument, gint pitch)
{
        LudwigInstrumentProfile *profile;
        gchar *profile_name;

        profile_name = ludwig_config_query_profile ();
        profile = g_hash_table_lookup (instrument->profiles, profile_name);
        g_free (profile_name);

        if (pitch > profile->pitch_floor && pitch < profile->pitch_ceiling)
                return TRUE;
        else
                return FALSE;
}

gboolean
ludwig_instrument_is_i18n_synonym (LudwigInstrument *instrument, gchar *name)
{
}

gchar *
ludwig_instrument_query_group (LudwigInstrument *instrument)
{
}

GArray *
ludwig_instrument_query_i18n_names (LudwigInstrument *instrument)
{
}

void
ludwig_instrument_set_group (LudwigInstrument *instrument, const char *group)
{
        if (instrument->group != NULL)
                g_free (instrument->group);

        instrument->group = g_strdup (group);
}

void
ludwig_instrument_add_profile (LudwigInstrument *instrument, const char *profile_name)
{
        LudwigInstrumentProfile *profile;

        profile = g_new0 (LudwigInstrumentProfile, 1);

        g_hash_table_insert (instrument->profiles, g_strdup (profile_name), profile);
}

void
ludwig_instrument_set_default_clef (LudwigInstrument *instrument, const char *profile_name, LudwigClef default_clef)
{
        LudwigInstrumentProfile *profile;
        GSList *tmp;
        gboolean is_valid;

        is_valid = FALSE;
        profile = g_hash_table_lookup (instrument->profiles, profile_name);

        tmp = profile->all_clefs;
        while (tmp != NULL)
        {
                if (GPOINTER_TO_INT (tmp->data) == default_clef)
                        is_valid = TRUE;

                tmp = g_slist_next (tmp);
        }

        if (is_valid == FALSE)
        {
                g_warning ("ludwig_instrument_set_default_clef (): Attempting to set default clef to invalid clef.\n");
                return;
        }

        profile->default_clef = default_clef;
}

void
ludwig_instrument_add_valid_clef (LudwigInstrument *instrument, const char *profile_name, LudwigClef clef)
{
        LudwigInstrumentProfile *profile;
        GSList *tmp;
        gboolean already_added;

        already_added = FALSE;

        profile = g_hash_table_lookup (instrument->profiles, profile_name);

        tmp = profile->all_clefs;
        while (tmp != NULL)
        {
                if (GPOINTER_TO_INT (tmp->data) == clef)
                {
                        g_warning ("Instrument already contains that clef.\n");
                        return;
                }

                tmp = g_slist_next (tmp);
        }

        profile->all_clefs = g_slist_append (profile->all_clefs, (int) clef);
}

void
ludwig_instrument_set_pitch_ceiling (LudwigInstrument *instrument, const char *profile_name, int pitch)
{
        LudwigInstrumentProfile *profile;

        profile = g_hash_table_lookup (instrument->profiles, profile_name);
        profile->pitch_ceiling = pitch;
}

void
ludwig_instrument_set_pitch_floor (LudwigInstrument *instrument, const char *profile_name, int pitch)
{
        LudwigInstrumentProfile *profile;

        profile = g_hash_table_lookup (instrument->profiles, profile_name);
        profile->pitch_floor = pitch;
}
