/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include <gtk/gtk.h>
#include <libgnomeui/gnome-stock.h>

#include <gdk-pixbuf/gdk-pixbuf.h>

#include <gal/e-table/e-tree.h>
#include <gal/e-table/e-tree-scrolled.h>
#include <gal/e-table/e-tree-memory-callbacks.h>
#include <gal/e-table/e-tree-memory.h>
#include <gal/e-table/e-cell-text.h>
#include <gal/e-table/e-cell-tree.h>
#include <gal/e-table/e-table.h>
#include <gal/e-table/e-table-scrolled.h>

#include "ludwig-interface-prefs-dialog.h"

typedef struct _InterfaceTableRow InterfaceTableRow;
typedef struct _InterfaceTreeNode InterfaceTreeNode;

struct _InterfaceTableRow
{
        gchar *verb;
        GdkPixbuf *pixbuf;
};

struct _InterfaceTreeNode
{
        gboolean group;
        gchar *verb;
        GdkPixbuf *pixbuf;
};

enum
{
        COL_VERB,
        COL_LAST
};

static const gchar *tools_spec =
"<ETableSpecification cursor-mode=\"line\" selection-mode=\"browse\" draw-focus=\"true\">   \
   <ETableColumn model_col=\"0\" _title=\"Toolbars\" expansion=\"1.0\" minimum_width=\"40\" \
       resizable=\"true\" cell=\"render_tree\" compare=\"string\"/>                         \
   <ETableState>                                                                            \
     <column source=\"0\"/>                                                                 \
     <grouping></grouping>                                                                  \
   </ETableState>                                                                           \
 </ETableSpecification>";

static const gchar *verbs_spec =
"<ETableSpecification cursor-mode=\"line\">                                                 \
   <ETableColumn model_col=\"0\" _title=\"Verbs\" expansion=\"1.0\" minimum_width=\"110\"   \
                 cell=\"render_left\" compare=\"string\" resizable=\"false\"/>              \
   <ETableState>                                                                            \
     <column source=\"0\"/>                                                                 \
     <grouping></grouping>                                                                  \
   </ETableState>                                                                           \
 </ETableSpecification>";

static GnomeDialogClass *parent_class = NULL;
static LudwigInterfacePrefsDialog *singleton = NULL;
static GArray *verbs_array = NULL;

/* ETableSimple callbacks */
static int
ludwig_interface_prefs_dialog_verbs_col_count (ETableModel *model, void *data)
{
        return 1;
}

static int
ludwig_interface_prefs_dialog_verbs_row_count (ETableModel *model, void *data)
{
        return verbs_array->len;
}

static void *
ludwig_interface_prefs_dialog_verbs_value_at (ETableModel *model, int col, int row, void *data)
{
        InterfaceTableRow *trow;

        trow = g_array_index (verbs_array, InterfaceTableRow *, row);

        return trow->verb;
}

static void
ludwig_interface_prefs_dialog_verbs_set_value_at (ETableModel *model, int col, int row, void *value, void *data)
{
}

static gboolean
ludwig_interface_prefs_dialog_verbs_is_cell_editable (ETableModel *model, int col, int row, void *data)
{
        return FALSE;
}

static void *
ludwig_interface_prefs_dialog_verbs_duplicate_value (ETableModel *model, int col, void *value, void *data)
{
        return g_strdup (value);
}

static void
ludwig_interface_prefs_dialog_verbs_free_value (ETableModel *model, int col, void *value, void *data)
{
        g_free (value);
}

static void *
ludwig_interface_prefs_dialog_verbs_init_value (ETableModel *model, int col, void *data)
{
        return g_strdup ("");
}

static gboolean
ludwig_interface_prefs_dialog_verbs_value_is_empty (ETableModel *model, int col, void *value, void *data)
{
        return !(value && *(char *) value);
}

static char *
ludwig_interface_prefs_dialog_verbs_value_to_string (ETableModel *model, int col, void *value, void *data)
{
        return g_strdup (value);
}

/* ETable utility functions */
ETableExtras *
ludwig_interface_prefs_dialog_create_etable_extras (void)
{
        ETableExtras *extras;
        ECell *cell;

        extras = e_table_extras_new ();

        cell = e_cell_text_new (NULL, GTK_JUSTIFY_LEFT);
        e_table_extras_add_cell (extras, "render_left", cell);

        return extras;
}

static void
ludwig_interface_prefs_dialog_init_verbs_array (void)
{
        verbs_array = g_array_new (FALSE, FALSE, sizeof (InterfaceTableRow *));
}

/* ETreeSimple callbacks */
static GdkPixbuf *
ludwig_interface_prefs_dialog_icon_at (ETreeModel *model, ETreePath path, void *model_data)
{
        InterfaceTreeNode *node = e_tree_memory_node_get_data (E_TREE_MEMORY (model), path);

        if (node != NULL)
                return node->pixbuf;

        return NULL;
}

static gint
ludwig_interface_prefs_dialog_col_count (ETreeModel *model, void *data)
{
        return COL_LAST;
}

static void *
ludwig_interface_prefs_dialog_value_at (ETreeModel *model,
                                        ETreePath path,
                                        int col,
                                        void *model_data)
{
        InterfaceTreeNode *node;

        node = e_tree_memory_node_get_data (E_TREE_MEMORY (model), path);

        switch (col)
        {
        case COL_VERB:
                return node->verb;

        default:
                return NULL;
        }
}

static void
ludwig_interface_prefs_dialog_set_value_at (ETreeModel *model,
                                            ETreePath path,
                                            int col,
                                            const void *val,
                                            void *data)
{
        InterfaceTreeNode *node;

        node = e_tree_memory_node_get_data (E_TREE_MEMORY (model), path);

        switch (col)
        {
        case COL_VERB:
                if (node->verb != NULL)
                        g_free (node->verb);

                node->verb = g_strdup (val);
                break;

        default:
        }

        e_tree_memory_node_set_data (E_TREE_MEMORY (model), path, node);
}

static gboolean
ludwig_interface_prefs_dialog_is_editable (ETreeModel *model,
                                           ETreePath path,
                                           int col,
                                           void *model_data)
{
        return FALSE;
}

static void *
ludwig_interface_prefs_dialog_duplicate_value (ETreeModel *model,
                                               int col,
                                               const void *value,
                                               void *data)
{
        switch (col)
        {
        case COL_VERB:
                return g_strdup (value);

        default:
                return value;
        }
}

static void
ludwig_interface_prefs_dialog_free_value (ETreeModel *model,
                                          int col,
                                          void *value,
                                          void *data)
{
        switch (col)
        {
        case COL_VERB:
                g_free (value);

        default:
                return;
        }
}

static void *
ludwig_interface_prefs_dialog_init_value (ETreeModel *model,
                                          int col,
                                          void *data)
{
        switch (col)
        {
        case COL_VERB:
                return g_strdup ("");

        default:
                break;
        }
}

static gboolean
ludwig_interface_prefs_dialog_value_is_empty (ETreeModel *model,
                                              int col,
                                              const void *value,
                                              void *data)
{
        switch (col)
        {
        case COL_VERB:
                return !(value && *(char *) value);

        default:
                return TRUE;
        }
}

static char *
ludwig_interface_prefs_dialog_value_to_string (ETreeModel *model,
                                               int col,
                                               const void *value,
                                               void *data)
{
}

/* ETree utility functions */
static ETableExtras *
ludwig_interface_prefs_dialog_create_etree_extras (void)
{
        ETableExtras *extras;
        ECell *cell;

        extras = e_table_extras_new ();

        cell = e_cell_text_new (NULL, GTK_JUSTIFY_LEFT);

        e_table_extras_add_cell (extras, "render_tree",
                                 e_cell_tree_new (NULL, NULL, TRUE, cell));

        return extras;
}

static ETreePath
ludwig_interface_prefs_dialog_insert_item (ETreeModel *model,
                                           ETreePath parent_node,
                                           const char *name,
                                           gboolean is_group,
                                           int pos,
                                           GdkPixbuf *pixbuf)
{
        InterfaceTreeNode *node;
        ETreePath  parent;
        ETreePath  path;
        char      *id;

        if (parent_node == NULL)
                parent = e_tree_model_get_root (model);
        else
                parent = parent_node;

        node = g_new0 (InterfaceTreeNode, 1);
        node->verb = g_strdup (name);
        node->group = is_group;
        node->pixbuf = pixbuf;

        path = e_tree_memory_node_insert_id (singleton->tools_memory,
                                             parent,
                                             0,
                                             node,
                                             g_strdup (id));

        singleton->tools_paths = g_slist_append (singleton->tools_paths, path);

        return path;
}

/* GtkObject setup */
static void
ludwig_interface_prefs_dialog_destroy (GtkObject *object)
{
        LudwigInterfacePrefsDialog *dialog;

        dialog = (LudwigInterfacePrefsDialog *) object;

        gtk_widget_destroy (dialog->notebook);
}

static void
ludwig_interface_prefs_dialog_init (LudwigInterfacePrefsDialog *dialog)
{
        dialog->notebook = NULL;

        dialog->tools_paths = NULL;
        dialog->tools_model = NULL;
        dialog->tools_memory = NULL;
        dialog->tools_tree = NULL;
}

static void
ludwig_interface_prefs_dialog_class_init (LudwigInterfacePrefsDialogClass *class)
{
        GtkObjectClass *object_class;

        object_class = (GtkObjectClass *) class;
        object_class->destroy = ludwig_interface_prefs_dialog_destroy;

        parent_class = gtk_type_class (gnome_dialog_get_type ());
}

static void
dialog_ok_cb (GtkWidget *widget, gpointer data)
{
        gtk_widget_hide (GTK_WIDGET (singleton));
}

GtkType
ludwig_interface_prefs_dialog_get_type (void)
{
        static GtkType type = 0;

        if (!type)
        {
                const GtkTypeInfo info =
                {
                        "LudwigInterfacePrefsDialog",
                        sizeof (LudwigInterfacePrefsDialog),
                        sizeof (LudwigInterfacePrefsDialogClass),
                        (GtkClassInitFunc)  ludwig_interface_prefs_dialog_class_init,
                        (GtkObjectInitFunc) ludwig_interface_prefs_dialog_init,
                        NULL, /* Reserved 1 */
                        NULL, /* Reserved 2 */
                        (GtkClassInitFunc) NULL
                };

                type = gtk_type_unique (gnome_dialog_get_type (), &info);
        }

        return type;
}

static void
ludwig_interface_prefs_dialog_install_verb (const char *verb, int pos, gpointer user_data)
{
        ETreePath path = user_data;

        ludwig_interface_prefs_dialog_insert_item (singleton->tools_model,
                                                   path,
                                                   verb,
                                                   FALSE,
                                                   pos,
                                                   NULL);
}

static void
ludwig_interface_prefs_dialog_install_verbs_group (const char *group, int pos)
{
        ETreePath path;

        path = ludwig_interface_prefs_dialog_insert_item (singleton->tools_model,
                                                          e_tree_model_get_root (singleton->tools_model),
                                                          group,
                                                          TRUE,
                                                          pos,
                                                          NULL);

#if 0
        ludwig_verb_manager_foreach_verb_in_group (group,
                                                   &ludwig_interface_prefs_dialog_install_verb,
                                                   path);
#endif
}

static void
ludwig_interface_prefs_dialog_populate_verb_table (BonoboUINode *parent)
{
        InterfaceTableRow *trow;
        BonoboUINode *node;
        int row;
        char *name;

        for (node = bonobo_ui_node_children (parent); node != NULL; node = bonobo_ui_node_next (node))
        {
                trow = g_new0 (InterfaceTableRow, 1);
                name = bonobo_ui_node_get_attr (node, "name");
                if (name == NULL)
                {
                        name = bonobo_ui_node_get_name (node);
                }

                trow->verb = name;
                trow->pixbuf = bonobo_ui_util_xml_get_icon_pixbuf (node, FALSE);

                g_array_append_val (verbs_array, trow);
                row = verbs_array->len - 1;

                e_table_model_row_inserted (singleton->verbs_model, row);
                e_table_set_cursor_row (singleton->verbs_table, row);
        }
}

static void
ludwig_interface_prefs_dialog_populate_dockitem_node (BonoboUINode *parent, ETreePath parent_path)
{
        BonoboUINode *node;
        ETreePath path;
        GdkPixbuf *pixbuf;
        gchar *name = NULL;
        gchar *attr = NULL;

        for (node = bonobo_ui_node_children (parent); node != NULL; node = bonobo_ui_node_next (node))
        {
                name = bonobo_ui_node_get_name (node);
                attr = bonobo_ui_node_get_attr (node, "name");

                pixbuf = bonobo_ui_util_xml_get_icon_pixbuf (node, FALSE);

                path = ludwig_interface_prefs_dialog_insert_item (singleton->tools_model,
                                                                  parent_path,
                                                                  attr != NULL ? attr : name,
                                                                  bonobo_ui_node_children (node) != NULL,
                                                                  2,
                                                                  pixbuf);
        }
}

static void
ludwig_interface_prefs_dialog_populate_node (BonoboUINode *parent, ETreePath parent_path)
{
        BonoboUINode *node;
        ETreePath path;
        gchar *name = NULL;
        GdkPixbuf *pixbuf;

        for (node = bonobo_ui_node_children (parent); node != NULL; node = bonobo_ui_node_next (node))
        {
                name = bonobo_ui_node_get_name (node);
                if (strcmp (name, "commands") == 0)
                {
                        ludwig_interface_prefs_dialog_populate_verb_table (node);
                }
                else if (strcmp (name, "dockitem") == 0)
                {
                        pixbuf = bonobo_ui_util_xml_get_icon_pixbuf (node, FALSE);

                        path = ludwig_interface_prefs_dialog_insert_item (singleton->tools_model,
                                                                          parent_path,
                                                                          name,
                                                                          bonobo_ui_node_children (node) != NULL,
                                                                          2,
                                                                          pixbuf);

                        ludwig_interface_prefs_dialog_populate_dockitem_node (node, path);
                }

                pixbuf = NULL;
                name = NULL;

                ludwig_interface_prefs_dialog_populate_node (node, path);
        }
}

static void
ludwig_interface_prefs_dialog_populate_tree (void)
{
        BonoboUINode *root;
        BonoboUINode *node;
        CORBA_Environment ev;
        Bonobo_UIContainer *container;

        CORBA_exception_init (&ev);

        root = bonobo_ui_component_get_tree (singleton->component, "/", TRUE, &ev);

        CORBA_exception_free (&ev);

        ludwig_interface_prefs_dialog_populate_node (root, e_tree_model_get_root (singleton->tools_model));

#if 0
        for (node = bonobo_ui_node_children (root); node != NULL; node = bonobo_ui_node_next (node))
        {
                ludwig_interface_prefs_dialog_insert_item (singleton->tools_memory,
                                                           e_tree_model_get_root (singleton->tools_model),
                                                           bonobo_ui_node_get_name (node),
                                                           TRUE,
                                                           2);

                ludwig_interface_prefs_dialog_populate_root_child (node);
        }
#endif
}

void
ludwig_interface_prefs_dialog_create (BonoboUIComponent *component)
{
        ETableExtras *tools_extras;
        ETreePath     root_node;
        ECell        *cell_left_adjust;
        GtkWidget    *tools_scroll;
        GtkWidget    *hbox;

        ETableExtras *verbs_extras;
        GtkWidget    *verbs_scroll;

        /* Create the dialog */
        singleton = gtk_type_new (ludwig_interface_prefs_dialog_get_type ());
        gtk_window_set_title (GTK_WINDOW (singleton), "Customize Interface");

        gnome_dialog_append_button (GNOME_DIALOG (singleton), GNOME_STOCK_BUTTON_OK);
        gnome_dialog_button_connect (GNOME_DIALOG (singleton), 0,
                                     GTK_SIGNAL_FUNC (dialog_ok_cb),
                                     NULL);

        singleton->component = component;

        /* Create the notebook */
        singleton->notebook = gtk_notebook_new ();

        /* Initialize the array of data for ETable */
        ludwig_interface_prefs_dialog_init_verbs_array ();

        /* Create the ETable for holding verbs data */
        singleton->verbs_model = e_table_simple_new (ludwig_interface_prefs_dialog_verbs_col_count,
                                                     ludwig_interface_prefs_dialog_verbs_row_count,
                                                     ludwig_interface_prefs_dialog_verbs_value_at,
                                                     ludwig_interface_prefs_dialog_verbs_set_value_at,
                                                     ludwig_interface_prefs_dialog_verbs_is_cell_editable,
                                                     ludwig_interface_prefs_dialog_verbs_duplicate_value,
                                                     ludwig_interface_prefs_dialog_verbs_free_value,
                                                     ludwig_interface_prefs_dialog_verbs_init_value,
                                                     ludwig_interface_prefs_dialog_verbs_value_is_empty,
                                                     ludwig_interface_prefs_dialog_verbs_value_to_string,
                                                     NULL);

        singleton->verbs_header = e_table_header_new ();

        verbs_extras = ludwig_interface_prefs_dialog_create_etable_extras ();

        verbs_scroll = e_table_scrolled_new (E_TABLE_MODEL (singleton->verbs_model),
                                             verbs_extras,
                                             verbs_spec,
                                             NULL);
        gtk_widget_set_usize (verbs_scroll, 140, 150);
        singleton->verbs_table = e_table_scrolled_get_table (E_TABLE_SCROLLED (verbs_scroll));
        e_table_load_state (E_TABLE (singleton->verbs_table), "header_state");

        /* Create the ETree objects for holding the toolbar data */
        singleton->tools_model = e_tree_memory_callbacks_new (ludwig_interface_prefs_dialog_icon_at,
                                                              ludwig_interface_prefs_dialog_col_count,
                                                              NULL,
                                                              NULL,
                                                              NULL,
                                                              NULL,
                                                              ludwig_interface_prefs_dialog_value_at,
                                                              ludwig_interface_prefs_dialog_set_value_at,
                                                              ludwig_interface_prefs_dialog_is_editable,
                                                              ludwig_interface_prefs_dialog_duplicate_value,
                                                              ludwig_interface_prefs_dialog_free_value,
                                                              ludwig_interface_prefs_dialog_init_value,
                                                              ludwig_interface_prefs_dialog_value_is_empty,
                                                              ludwig_interface_prefs_dialog_value_to_string,
                                                              NULL);

        singleton->tools_memory = E_TREE_MEMORY (singleton->tools_model);

        root_node = e_tree_memory_node_insert (singleton->tools_memory,
                                               NULL,
                                               0,
                                               NULL);

        tools_extras = ludwig_interface_prefs_dialog_create_etree_extras ();

        tools_scroll = e_tree_scrolled_new (singleton->tools_model, tools_extras, tools_spec, NULL);
        singleton->tools_tree = e_tree_scrolled_get_tree (E_TREE_SCROLLED (tools_scroll));
        e_tree_load_state (E_TREE (singleton->tools_tree), "header_state");
        e_tree_load_expanded_state (E_TREE (singleton->tools_tree), "expanded_state");
        e_tree_root_node_set_visible (E_TREE (singleton->tools_tree), FALSE);

        gtk_widget_set_usize (tools_scroll, 240, 150);

        hbox = gtk_hbox_new (FALSE, 0);

        gtk_box_pack_start (GTK_BOX (hbox), verbs_scroll, FALSE, FALSE, 0);
        gtk_box_pack_start (GTK_BOX (hbox), tools_scroll, FALSE, FALSE, 0);

        gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (singleton)->vbox), hbox, FALSE, FALSE, 0);

        gtk_widget_show_all (GTK_WIDGET (singleton));

        ludwig_interface_prefs_dialog_populate_tree ();
}
