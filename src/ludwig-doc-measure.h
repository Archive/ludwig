/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#ifndef __LUDWIG_DOC_MEASURE_H__
#define __LUDWIG_DOC_MEASURE_H__

#include "ludwig-types.h"
#include "ludwig-doc-container.h"

#define LUDWIG_TYPE_DOC_MEASURE        (ludwig_doc_part_get_type ())
#define LUDWIG_DOC_MEASURE(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_DOC_MEASURE, LudwigDocMeasure))
#define LUDWIG_DOC_MEASURE_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_DOC_MEASURE, LudwigDocMeasureClass))
#define LUDWIG_IS_DOC_MEASURE(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_DOC_MEASURE))
#define LUDWIG_IS_DOC_MEASURE_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_DOC_MEASURE))

struct _LudwigDocMeasure
{
        LudwigDocContainer parent_object;

        LudwigDocPart *part;

        gint measure_num;

        LudwigDocMeasure *next;
        LudwigDocMeasure *prev;
};

struct _LudwigDocMeasureClass
{
        LudwigDocContainerClass parent_class;
};

GtkType           ludwig_doc_measure_get_type (void);
LudwigDocMeasure *ludwig_doc_measure_new      (void);

#endif /* __LUDWIG_DOC_MEASURE_H__ */
