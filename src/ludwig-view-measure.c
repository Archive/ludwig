/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-util.h>

#include "ludwig-view-measure.h"
#include "ludwig-view-metric.h"
#include "ludwig-view-staff.h"
#include "ludwig-util.h"

#include <ludwig-font/ludwig-font.h>
#include <ludwig-font/ludwig-font-glyph.h>
#include <ludwig-font/ludwig-rfont.h>

#include <canvas-items/gp-path.h>
#include <canvas-items/gnome-canvas-ttext.h>

static void ludwig_view_measure_class_init (LudwigViewMeasureClass *class);
static void ludwig_view_measure_init       (LudwigViewMeasure      *measure);
static void ludwig_view_measure_destroy    (GtkObject              *object);

static LudwigViewContainerClass *parent_class = NULL;

GtkType
ludwig_view_measure_get_type (void)
{
        static GtkType type = 0;

        if (!type)
        {
                GtkTypeInfo info =
                {
                        "LudwigViewMeasure",
                        sizeof (LudwigViewMeasure),
                        sizeof (LudwigViewMeasureClass),
                        (GtkClassInitFunc) ludwig_view_measure_class_init,
                        (GtkObjectInitFunc) ludwig_view_measure_init,
                        NULL, /* Reserved 1 */
                        NULL, /* Reserved 2 */
                        (GtkClassInitFunc) NULL
                };

                type = gtk_type_unique (ludwig_view_container_get_type (), &info);
        }

        return type;
}

void
ludwig_view_measure_construct (LudwigViewMeasure *measure)
{
        int i;
        GnomeCanvasItem *item;
        GnomeCanvasPoints *points;
        double affine[6];

        for (i = 0; i < 5; i++)
        {
                points = gnome_canvas_points_new (2);
                points->coords[0] = 0.0;
                points->coords[1] = 9.0 * i;
                points->coords[2] = measure->len;
                points->coords[3] = 9.0 * i;

                item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (measure),
                                              gnome_canvas_line_get_type (),
                                              "width_units", 1.0,
                                              "fill_color", "black",
                                              "points", points,
                                              NULL);

                gnome_canvas_points_unref (points);

                art_affine_scale (affine, 1.0, 1.0);
                gnome_canvas_item_affine_absolute (item, affine);

                measure->staff_lines = g_slist_prepend (measure->staff_lines, item);
        }

        /* Install barline */
        points = gnome_canvas_points_new (2);
        points->coords[0] = measure->len - 0.5;
        points->coords[1] = 0.0;
        points->coords[2] = measure->len - 0.5;
        points->coords[3] = 36.0;

        measure->barline = gnome_canvas_item_new (GNOME_CANVAS_GROUP (measure),
                                                  gnome_canvas_line_get_type (),
                                                  "width_units", 1.0,
                                                  "fill_color", "black",
                                                  "points", points,
                                                  NULL);

        gnome_canvas_points_unref (points);

        /* Install whole rest.. code == 183. */
        if (ludwig_view_measure_get_prev (measure) == NULL)
        {
                ludwig_view_measure_install_timesig (measure, 2, 2);
                ludwig_view_measure_insert_rest (measure, 1, FALSE, FALSE);
        }

        measure->complete = TRUE;
}

LudwigViewStaff *
ludwig_view_measure_get_staff (LudwigViewMeasure *measure)
{
        LudwigViewItem *item = (LudwigViewItem *) measure;

        return (LudwigViewStaff *) item->container;
}

LudwigViewMeasure *
ludwig_view_measure_get_prev (LudwigViewMeasure *measure)
{
        LudwigViewStaff *staff;
        GList *tmp;

        staff = ludwig_view_measure_get_staff (measure);

        if (measure->prev == NULL)
        {
                tmp = LUDWIG_VIEW_CONTAINER (staff)->items;
                while (LUDWIG_VIEW_MEASURE (tmp->data) != measure)
                {
                        tmp = tmp->next;
                };

                return LUDWIG_VIEW_MEASURE (tmp->data)->prev;
        }
        else
        {
                return measure->prev;
        }
}

void
ludwig_view_measure_install_timesig (LudwigViewMeasure *measure, int num, int denom)
{
        GnomeCanvasItem *item;
        LudwigFont *font;
        LudwigFontGlyph *glyph;
        GPPath *path;
        int code;
        double affine[6];

        if (num == 4 && denom == 4)
        {
                /* Code 99 */
                font = (LudwigFont *) ludwig_get_standard_font ();
                code = ludwig_font_face_lookup (font->face, 0, 99);
                glyph = ludwig_vfont_get_glyph ((LudwigVFont *) font, code);
                path = gp_path_new_from_foreign_bpath (ludwig_font_glyph_get_bpath (glyph, TRUE));

                item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (measure),
                                              gnome_canvas_bpath_get_type (),
                                              "bpath", path,
                                              "fill_color_rgba", 0x00ff,
                                              NULL);

                art_affine_scale (affine, 0.75, 0.75);
                affine[3] = 0.0 - affine[3];
                affine[4] = 40.0;
                affine[5] = 18.0;

                gnome_canvas_item_affine_absolute (item, affine);
        }
        else if (num == 2 && denom == 2)
        {
                /* Code 67 */
                font = (LudwigFont *) ludwig_get_standard_font ();
                code = ludwig_font_face_lookup (font->face, 0, 67);
                glyph = ludwig_vfont_get_glyph ((LudwigVFont *) font, code);
                path = gp_path_new_from_foreign_bpath (ludwig_font_glyph_get_bpath (glyph, TRUE));

                item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (measure),
                                              gnome_canvas_bpath_get_type (),
                                              "bpath", path,
                                              "fill_color_rgba", 0x00ff,
                                              NULL);

                art_affine_scale (affine, 0.75, 0.75);
                affine[3] = 0.0 - affine[3];
                affine[4] = 42.0;
                affine[5] = 18.0;

                gnome_canvas_item_affine_absolute (item, affine);
        }
        else
        {
                /* 0 == 48, 9 == 57 */
                font = (LudwigFont *) ludwig_get_standard_font ();
                code = ludwig_font_face_lookup (font->face, 0, 48 + num);
                glyph = ludwig_vfont_get_glyph ((LudwigVFont *) font, code);
                path = gp_path_new_from_foreign_bpath (ludwig_font_glyph_get_bpath (glyph, TRUE));

                item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (measure),
                                              gnome_canvas_bpath_get_type (),
                                              "bpath", path,
                                              "fill_color_rgba", 0x00ff,
                                              NULL);

                art_affine_scale (affine, 0.75, 0.75);
                affine[3] = 0.0 - affine[3];
                affine[4] = 42.0;
                affine[5] = 8.0;

                gnome_canvas_item_affine_absolute (item, affine);

                memset (affine, 0, sizeof (affine));
                code = ludwig_font_face_lookup (font->face, 0, 48 + denom);
                glyph = ludwig_vfont_get_glyph ((LudwigVFont *) font, code);
                path = gp_path_new_from_foreign_bpath (ludwig_font_glyph_get_bpath (glyph, TRUE));

                item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (measure),
                                              gnome_canvas_bpath_get_type (),
                                              "bpath", path,
                                              "fill_color_rgba", 0x00ff,
                                              NULL);

                art_affine_scale (affine, 0.75, 0.75);
                affine[3] = 0.0 - affine[3];
                affine[4] = 42.0;
                affine[5] = 26.0;

                gnome_canvas_item_affine_absolute (item, affine);
        }
}

void
ludwig_view_measure_insert_note (LudwigViewMeasure *measure, int value, gboolean is_dotted, gboolean is_double)
{
        LudwigViewMetric *metric;
        LudwigViewContainer *container;

        container = (LudwigViewContainer *) measure;
        metric = gnome_canvas_item_new (GNOME_CANVAS_GROUP (measure),
                                        ludwig_view_metric_get_type (),
                                        "x", 70.0,
                                        "y", 5.0,
                                        "container", LUDWIG_VIEW_ITEM (container),
                                        "metric_value", value,
                                        "dotted", is_dotted,
                                        "doubled", is_double,
                                        NULL);

        if (container->items == NULL)
        {
                g_print ("Inserting note into measure..\n");
                container->items = g_list_append (container->items, metric);
        }

        ludwig_view_metric_construct (metric);
}

void
ludwig_view_measure_insert_rest (LudwigViewMeasure *measure, int value, gboolean is_dotted, gboolean is_double)
{
        LudwigViewContainer *container = (LudwigViewContainer *) measure;
}

static void
ludwig_view_measure_class_init (LudwigViewMeasureClass *class)
{
        GtkObjectClass *object_class;

        object_class = (GtkObjectClass *) class;

        parent_class = gtk_type_class (ludwig_view_container_get_type ());

        object_class->destroy = ludwig_view_measure_destroy;
};

static void
ludwig_view_measure_init (LudwigViewMeasure *measure)
{
        measure->len = 160.0;
        measure->next = NULL;
        measure->prev = NULL;
}

static void
ludwig_view_measure_destroy (GtkObject *object)
{
}
