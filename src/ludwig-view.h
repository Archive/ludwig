/*
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#ifndef __LUDWIG_VIEW_H__
#define __LUDWIG_VIEW_H__

#include <bonobo.h>

#include "ludwig-types.h"
#include "ludwig-display-metrics.h"

#include "ludwig-view-part.h"
#include "ludwig-view-staff.h"

#define LUDWIG_TYPE_VIEW        (ludwig_view_get_type())
#define LUDWIG_VIEW(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_VIEW, LudwigView))
#define LUDWIG_VIEW_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_VIEW, LudwigViewClass))
#define LUDWIG_IS_VIEW(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_VIEW))
#define LUDWIG_IS_VIEW_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_VIEW))

struct _LudwigView
{
        GtkObject         parent_object;

        BonoboControl    *control;
        BonoboZoomable   *zoomable;
        LudwigViewFrame  *view_frame;

        LudwigZoomControl *zoom_control;
        LudwigDocument   *doc;

        GtkWidget        *canvas;
        GnomeCanvasGroup *root;
        GnomeCanvasItem  *page;
        GnomeCanvasItem  *page_group;

        GHashTable       *parts;
        int               n_parts;
	GList            *y_list;
	GArray           *y_array;

        LudwigDisplayMetric units;

        /* Properties */
        double            left_margin;
        double            right_margin;
        double            top_margin;
        double            bottom_margin;
        double            zoom_level;
        double            page_hsize;
        double            page_vsize;

        gboolean          show_hruler;
        gboolean          show_vruler;
};

struct _LudwigViewClass
{
        /* TODO:
         *   Make LudwigView/LudwigViewFrame derive from BonoboView/BonoboViewFrame.
         */

        GtkObjectClass parent_class;
};

#if 0
struct _LudwigViewStaff
{
        /* TODO:
         *   Make a LudwigViewContainer object with abstract mouse motion handling.
         *
         *   Derive objects LudwigViewStaff and LudwigViewMeasure from this.
         */

        /* TODO:
         *   Store list of measures, not lines.  Align the measures visually so that they
         *   look all in one line.
         *
         *   - This simplifies breaking lines and even breaking measures
         *   - This makes it simple to make staff visible only in non-empty measures
         *   - This simplifies input handling.  Staves don't worry about what measure the
         *     input goes to since the measure object deals with it entirely.
         */

        GnomeCanvasItem *item;
        GnomeCanvasItem *group;

        double x;
        double y;

        double len;
        double height;

        GSList *lines;
};

struct _LudwigViewPart
{
        LudwigViewStaff *staff;
        float vsize;
        int   pos;
};
#endif

GtkType     ludwig_view_get_type          (void);
LudwigView *ludwig_view_new               (LudwigDocument *doc, BonoboUIContainer *container, LudwigZoomControl *zoom, const gchar *name);
void        ludwig_view_set_left_margin   (LudwigView *view, double margin);
void        ludwig_view_set_right_margin  (LudwigView *view, double margin);
void        ludwig_view_set_top_margin    (LudwigView *view, double margin);
void        ludwig_view_set_bottom_margin (LudwigView *view, double margin);
void        ludwig_view_set_page_size     (LudwigView *view, double hsize, double vsize);
void        ludwig_view_add_staff         (LudwigView *view, gint n_measures);
void        ludwig_view_set_metric_units  (LudwigView *view, LudwigDisplayMetric unit);

#endif /* __LUDWIG_VIEW_H__ */
