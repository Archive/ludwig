/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

/*
 * LudwigViewContainer: Perhaps slightly misleadingly named, this object does not contain
 *                      LudwigView objects, but rather contains/groups renderable objects inside
 *                      a view.
 */

#include "ludwig-view-item.h"

#ifndef __LUDWIG_VIEW_CONTAINER_H__
#define __LUDWIG_VIEW_CONTAINER_H__

#define LUDWIG_TYPE_VIEW_CONTAINER        (ludwig_view_container_get_type())
#define LUDWIG_VIEW_CONTAINER(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_VIEW_CONTAINER, LudwigViewContainer))
#define LUDWIG_VIEW_CONTAINER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_VIEW_CONTAINER, LudwigViewContainerClass))
#define LUDWIG_IS_VIEW_CONTAINER(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_VIEW_CONTAINER))
#define LUDWIG_IS_VIEW_CONTAINER_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_VIEW_CONTAINER))

typedef struct _LudwigViewContainer      LudwigViewContainer;
typedef struct _LudwigViewContainerClass LudwigViewContainerClass;

struct _LudwigViewContainer
{
	LudwigViewItem parent_object;

        GList *items;
};

struct _LudwigViewContainerClass
{
	LudwigViewItemClass parent_class;
};

GtkType      ludwig_view_container_get_type    (void);

#endif /* __LUDWIG_VIEW_CONTAINER_H__ */
