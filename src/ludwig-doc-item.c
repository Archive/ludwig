/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include "ludwig-doc-item.h"

static GtkObjectClass *parent_class = NULL;

static void
ludwig_doc_item_destroy (GtkObject *object)
{
}

static void
ludwig_doc_item_set_arg (GtkObject *object,
                         GtkArg    *arg,
                         guint      arg_id)
{
}

static void
ludwig_doc_item_get_arg (GtkObject *object,
                         GtkArg    *arg,
                         guint      arg_id)
{
}

static void
ludwig_doc_item_class_init (LudwigDocItemClass *class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;

	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = ludwig_doc_item_destroy;
	object_class->set_arg = ludwig_doc_item_set_arg;
	object_class->get_arg = ludwig_doc_item_get_arg;
}

static void
ludwig_doc_item_init (LudwigDocItem *item)
{
}

GtkType
ludwig_doc_item_get_type (void)
{
	static GtkType type = 0;

	if (!type)
	{
		const GtkTypeInfo info =
		{
			"LudwigDocItem",
			sizeof (LudwigDocItem),
			sizeof (LudwigDocItemClass),
			(GtkClassInitFunc)  ludwig_doc_item_class_init,
			(GtkObjectInitFunc) ludwig_doc_item_init,
			NULL, /* Reserved 1 */
			NULL, /* Reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gtk_object_get_type (), &info);
	}

	return type;
}
