/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_INSTRUMENT_H__
#define __LUDWIG_INSTRUMENT_H__

#include <gtk/gtkobject.h>
#include <gtk/gtksignal.h>

#include "ludwig-types.h"
#include "ludwig-util.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define LUDWIG_TYPE_INSTRUMENT        (ludwig_instrument_get_type())
#define LUDWIG_INSTRUMENT(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_INSTRUMENT, LudwigInstrument))
#define LUDWIG_INSTRUMENT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_INSTRUMENT, LudwigInstrumentClass))
#define LUDWIG_IS_INSTRUMENT(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_INSTRUMENT))
#define LUDWIG_IS_INSTRUMENT_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_INSTRUMENT))

typedef struct _LudwigInstrument        LudwigInstrument;
typedef struct _LudwigInstrumentClass   LudwigInstrumentClass;
typedef struct _LudwigInstrumentName    LudwigInstrumentName;
typedef struct _LudwigInstrumentProfile LudwigInstrumentProfile;

struct _LudwigInstrumentName
{
	gchar *locale;
	gchar *name;
	gchar *abbreviation;
};

struct _LudwigInstrumentProfile
{
	LudwigClef  default_clef;
	GSList     *all_clefs;
	gint        pitch_ceiling;
        gint        pitch_floor;
};

struct _LudwigInstrument
{
	GtkObject parent_object;

	GHashTable *profiles;
	GHashTable *names;
        gchar      *id;
	gchar      *group;
        int         pos;
};

struct _LudwigInstrumentClass
{
        GtkObjectClass parent_class;
};

/* Public interfaces */
GtkType           ludwig_instrument_get_type (void);
LudwigInstrument *ludwig_instrument_new      (const char *id, int pos, const char *group);

/* Query methods */
LudwigClef        ludwig_instrument_query_default_clef (LudwigInstrument *instrument);
gboolean          ludwig_instrument_is_valid_clef      (LudwigInstrument *instrument, LudwigClef clef);
gboolean          ludwig_instrument_is_valid_pitch     (LudwigInstrument *instrument, gint pitch);
gboolean          ludwig_instrument_is_i18n_synonym    (LudwigInstrument *instrument, gchar *name);
gchar            *ludwig_instrument_query_group        (LudwigInstrument *instrument);
GArray           *ludwig_instrument_query_i18n_names   (LudwigInstrument *instrument);

/* Set methods */
void              ludwig_instrument_set_group          (LudwigInstrument *instrument, const gchar *group);
void              ludwig_instrument_add_profile        (LudwigInstrument *instrument, const gchar *profile);
void              ludwig_instrument_set_default_clef   (LudwigInstrument *instrument, const gchar *profile, LudwigClef clef);
void              ludwig_instrument_add_valid_clef     (LudwigInstrument *instrument, const gchar *profile, LudwigClef clef);
void              ludwig_instrument_set_pitch_ceiling  (LudwigInstrument *instrument, const gchar *profile, gint pitch);
void              ludwig_instrument_set_pitch_floor    (LudwigInstrument *instrument, const gchar *profile, gint pitch);

#ifdef __cplusplus
}
#endif /* __cpluplus */

#endif /* __LUDWIG_INSTRUMENT_H__ */
