/*
 * ludwig-app.c: Main user interface for a document.
 *
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <ludwig-font/ludwig-font.h>
#include <ludwig-font/ludwig-font-glyph.h>
#include <ludwig-font/ludwig-rfont.h>

#include <bonobo.h>
#include <bonobo/bonobo-ui-util.h>
#include <bonobo/bonobo-win.h>

#include <gal/widgets/gtk-combo-text.h>

#include "ludwig-app.h"
#include "ludwig-display-metrics.h"
#include "ludwig-util.h"
#include "ludwig-types.h"
#include "ludwig-view.h"
#include "ludwig-view-frame.h"
#include "ludwig-zoom-control.h"
#include "ludwig.h"
#include "canvas-items/gnome-canvas-ttext.h"
#include "canvas-items/ludwig-canvas-staff.h"
#include "canvas-items/gp-path.h"

static void ludwig_app_set_arg (GtkObject *object, GtkArg *arg, guint arg_id);
static void ludwig_app_get_arg (GtkObject *object, GtkArg *arg, guint arg_id);

/* Verb callbacks */
static void new_cb                 (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void open_cb                (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void save_cb                (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void save_as_cb             (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void revert_cb              (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void print_setup_cb         (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void print_preview_cb       (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void close_cb               (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void exit_cb                (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void cut_cb                 (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void copy_cb                (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void paste_cb               (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void clear_cb               (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void undo_cb                (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void redo_cb                (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void preferences_cb         (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void interface_layout_cb    (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void insert_instruments_cb  (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void insert_quarter_cb      (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void about_cb               (BonoboUIComponent *component, gpointer data,
                                    const char *cname);
static void page_properties_cb     (BonoboUIComponent *component, gpointer data,
                                    const char *cname);

static void metrics_inches_cb      (BonoboUIComponent            *component,
                                    const char                   *path,
                                    Bonobo_UIComponent_EventType  type,
                                    const char                   *state,
                                    gpointer                      user_data);
static void metrics_centimeters_cb (BonoboUIComponent            *component,
                                    const char                   *path,
                                    Bonobo_UIComponent_EventType  type,
                                    const char                   *state,
                                    gpointer                      user_data);
static void metrics_millimeters_cb (BonoboUIComponent            *component,
                                    const char                   *path,
                                    Bonobo_UIComponent_EventType  type,
                                    const char                   *state,
                                    gpointer                      user_data);
static void metrics_points_cb      (BonoboUIComponent            *component,
                                    const char                   *path,
                                    Bonobo_UIComponent_EventType  type,
                                    const char                   *state,
                                    gpointer                      user_data);

enum
{
	ARG_0,
	ARG_VIEW_HSCROLLBAR,
	ARG_VIEW_VSCROLLBAR,
	ARG_VIEW_TABS
};

static char *const document_dependent_verbs[] =
{
	"FileSave",
	"FileSaveAs",
	"FileRevert",
	"FilePrintPreview",
	"EditCut",
	"EditCopy",
	"EditPaste",
	"EditClear",
	"EditUndo",
	"EditRedo",
        "InsertInstruments",
        "InsertQuarter",
        "PageProperties",
        "MetricsInches",
        "MetricsCentimeters",
        "MetricsMillimeters",
        "MetricsPoints",
	NULL
};

static BonoboUIVerb verbs[] =
{
	BONOBO_UI_VERB ("FileNew", new_cb),
	BONOBO_UI_VERB ("FileOpen", open_cb),
	BONOBO_UI_VERB ("FileSave", save_cb),
	BONOBO_UI_VERB ("FileSaveAs", save_as_cb),
	BONOBO_UI_VERB ("FileRevert", revert_cb),
	BONOBO_UI_VERB ("FilePrintSetup", print_setup_cb),
	BONOBO_UI_VERB ("FilePrintPreview", print_preview_cb),
	BONOBO_UI_VERB ("FileClose", close_cb),
	BONOBO_UI_VERB ("FileExit", exit_cb),

	BONOBO_UI_VERB ("EditCut", cut_cb),
	BONOBO_UI_VERB ("EditCopy", copy_cb),
	BONOBO_UI_VERB ("EditPaste", paste_cb),
	BONOBO_UI_VERB ("EditClear", clear_cb),
	BONOBO_UI_VERB ("EditUndo", undo_cb),
	BONOBO_UI_VERB ("EditRedo", redo_cb),

	BONOBO_UI_VERB ("SettingsPreferences", preferences_cb),
        BONOBO_UI_VERB ("SettingsInterfaceLayout", interface_layout_cb),

        BONOBO_UI_VERB ("InsertInstruments", insert_instruments_cb),
        BONOBO_UI_VERB ("InsertQuarter", insert_quarter_cb),

	BONOBO_UI_VERB ("HelpAbout", about_cb),

        BONOBO_UI_VERB ("PageProperties", page_properties_cb),

	BONOBO_UI_VERB_END
};

static BonoboWindowClass *ludwig_app_parent_class;
static LudwigApp *singleton = NULL;

static void
view_destroy (gpointer key, gpointer value, gpointer user_data)
{
        gtk_object_destroy (value);
}

static void
ludwig_app_destroy (GtkObject *object)
{
        LudwigApp *app = (LudwigApp *) object;

        g_hash_table_foreach (app->views, &view_destroy, NULL);
        bonobo_object_unref (BONOBO_OBJECT (app->zoom_control));
        g_array_free (app->profiles, TRUE);
}

static void
set_document_dependent_verb_sensitivity (gchar *path,
                                         gchar *value)
{
	gchar *full_path;

	full_path = g_strconcat ("/commands/", path, NULL);

	bonobo_ui_component_set_prop (singleton->component, full_path,
				      "sensitive", value, NULL);
}

static void
new_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
	LudwigView *view;
	int i;

        if (singleton->doc == NULL)
        {
                singleton->doc = ludwig_document_new (NULL);
        }

	view = ludwig_view_new (singleton->doc,
                                singleton->container,
                                singleton->zoom_control,
                                NULL);

        ludwig_view_set_left_margin (view, 50.0);
        ludwig_view_set_right_margin (view, 50.0);
        ludwig_view_set_top_margin (view, 50.0);
        ludwig_view_set_bottom_margin (view, 50.0);

	ludwig_app_add_view (view);

	if (singleton->n_views == 0)
	{
		gtk_widget_set_sensitive (bonobo_control_get_widget (BONOBO_CONTROL (singleton->zoom_control)), 1);
		for (i = 0; document_dependent_verbs[i] != NULL; ++i)
		{
			set_document_dependent_verb_sensitivity (document_dependent_verbs[i], "1");
		}
	}

	singleton->n_views++;
}

static void
open_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
}

static void
save_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
}

static void
save_as_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
}

static void
print_setup_cb (BonoboUIComponent *component, gpointer data,
                const char *cname)
{
}

static void
print_preview_cb (BonoboUIComponent *component, gpointer data,
                  const char *cname)
{
}

static void
revert_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
}

static void
close_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
	ludwig_app_close ();
}

static void
exit_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
	ludwig_app_exit ();
}

static void
cut_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
}

static void
copy_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
}

static void
paste_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
}

static void
clear_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
}

static void
undo_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
}

static void
redo_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
}

static void
preferences_cb (BonoboUIComponent *component, gpointer data,
                const char *cname)
{
}

static void
interface_layout_cb (BonoboUIComponent *component, gpointer data,
               const char *cname)
{
        ludwig_interface_prefs_dialog_create (singleton->component);
}

static void
insert_instruments_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
        ludwig_instrument_add_dialog_create ();
}

static void
insert_quarter_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
}

static void
zero_pointer (GtkObject *object, void **pointer)
{
        *pointer = NULL;
}

static void
about_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
        static GtkWidget *about;

	const gchar *authors[] =
	{
		"Cody Russell <bratsche@gnome.org>",
		NULL
	};

        if (about)
        {
                gdk_window_show (about->window);
                gdk_window_raise (about->window);

                return;
        }
 
	about = gnome_about_new (_("Ludwig van"), VERSION,
                                 _("Copyright (C) 2000  Cody Russell"),
                                 authors,
                                 "A music scoring application for GNOME",
                                 NULL);

        gtk_signal_connect (GTK_OBJECT (about), "destroy",
                                    GTK_SIGNAL_FUNC (zero_pointer), &about);
	gnome_dialog_set_parent (GNOME_DIALOG (about), GTK_WINDOW (singleton));
	gnome_dialog_set_close (GNOME_DIALOG (about), TRUE);

	gtk_widget_show (about);
}

static void
page_properties_cb (BonoboUIComponent *component, gpointer data, const char *cname)
{
}

static void
metrics_inches_cb (BonoboUIComponent            *component,
                   const char                   *path,
                   Bonobo_UIComponent_EventType  type,
                   const char                   *state,
                   gpointer                      user_data)
{
        LudwigView *view;

        if (type != Bonobo_UIComponent_STATE_CHANGED)
        {
                return;
        }

        view = ludwig_app_get_current_view ();

        ludwig_view_set_metric_units (view, METRIC_IN);
}

static void
metrics_centimeters_cb (BonoboUIComponent            *component,
                        const char                   *path,
                        Bonobo_UIComponent_EventType  type,
                        const char                   *state,
                        gpointer                      user_data)
{
        LudwigView *view;

        if (type != Bonobo_UIComponent_STATE_CHANGED)
        {
                return;
        }

        view = ludwig_app_get_current_view ();

        ludwig_view_set_metric_units (view, METRIC_CM);
}

static void
metrics_millimeters_cb (BonoboUIComponent            *component,
                        const char                   *path,
                        Bonobo_UIComponent_EventType  type,
                        const char                   *state,
                        gpointer                      user_data)
{
        LudwigView *view;

        if (type != Bonobo_UIComponent_STATE_CHANGED)
        {
                return;
        }

        view = ludwig_app_get_current_view ();

        ludwig_view_set_metric_units (view, METRIC_MM);
}

static void
metrics_points_cb (BonoboUIComponent            *component,
                   const char                   *path,
                   Bonobo_UIComponent_EventType  type,
                   const char                   *state,
                   gpointer                      user_data)
{
        LudwigView *view;

        if (type != Bonobo_UIComponent_STATE_CHANGED)
        {
                return;
        }

        view = ludwig_app_get_current_view ();

        ludwig_view_set_metric_units (view, METRIC_PT);
}

LudwigView *
ludwig_app_get_current_view (void)
{
	GtkNotebook *notebook;
	int page;
        LudwigViewFrame *frame;
        LudwigView      *view;

	g_return_val_if_fail (singleton != NULL, NULL);

	notebook = GTK_NOTEBOOK (singleton->notebook);

	page = gtk_notebook_get_current_page (notebook);
	frame = (LudwigViewFrame *) gtk_notebook_get_nth_page (notebook, page);

        view = g_hash_table_lookup (singleton->views, frame->name);

	return view;
}

LudwigDocument *
ludwig_app_get_document (void)
{
        return singleton->doc;
}

static double
zoom_desc_to_float (const gchar *str)
{
	gchar *tmp;
	double x;

	tmp = g_strdup (str);

	tmp[strlen (tmp) - 1] = '\0';  /* Take off the '%' sign at the end. */
	x = atol (tmp);
	x = x / 100;

	g_free (tmp);

	return x;
}

static gchar *
get_zoom_setting_desc (float f)
{
        int n;

        for (n = 0; n < singleton->zoom_control->zoom_levels->len; n++)
        {
                if (g_array_index (singleton->zoom_control->zoom_levels, float, n) == f)
                {
                        return g_strdup (g_array_index (singleton->zoom_control->zoom_level_names, gchar*, n));
                }
        }

        return NULL;
}

static void
notebook_page_selected (GtkWidget *widget, GtkNotebookPage *page,
                        guint page_num, gpointer data)
{
	LudwigView      *view;
        LudwigViewFrame *frame;
	GtkNotebook     *notebook;
        GtkComboText    *combo;
	gint             i;
	gchar           *str;

	if (gtk_notebook_get_current_page (GTK_NOTEBOOK (widget)) == -1)
		return;

	notebook = GTK_NOTEBOOK (singleton->notebook);
	frame = (LudwigViewFrame *) gtk_notebook_get_nth_page (notebook, page_num);
        view = g_hash_table_lookup (singleton->views, frame->name);

	str = get_zoom_setting_desc (GNOME_CANVAS (view->canvas)->pixels_per_unit);

	for (i = 0; i < singleton->zoom_control->zoom_levels->len; i++)
        {
                if (strcmp (g_array_index (singleton->zoom_control->zoom_level_names, gchar*, i), str) == 0)
                {
                        combo = GTK_COMBO_TEXT (bonobo_control_get_widget (BONOBO_CONTROL (singleton->zoom_control)));
                        gtk_entry_set_text (GTK_ENTRY (combo->entry), str);
                }
        }

        gtk_widget_grab_focus (view->canvas);

	g_free (str);
}

static void
ludwig_app_create_standard_toolbar (void)
{
	Bonobo_UIContainer   corba_container;
	LudwigZoomControl   *zoom_control;
	GtkWidget           *zoom_box;

        corba_container = bonobo_object_corba_objref (BONOBO_OBJECT (singleton->container));

        zoom_control = ludwig_zoom_control_new ();
        zoom_box = bonobo_control_get_widget (BONOBO_CONTROL (zoom_control));
        if (!gnome_preferences_get_toolbar_relief_btn ())
                gtk_combo_box_set_arrow_relief (GTK_COMBO_BOX (zoom_box),
                                                GTK_RELIEF_NONE);
        gtk_widget_set_sensitive (zoom_box, FALSE);

        singleton->zoom_control = zoom_control;

	bonobo_ui_component_object_set (singleton->component,
                                        "/toolbar/Zoom",
                                        bonobo_object_corba_objref (BONOBO_OBJECT (zoom_control)),
                                        NULL);
}

static void
ludwig_app_init (GtkObject *object)
{
	LudwigApp *app = LUDWIG_APP(object);

	app->show_horizontal_scrollbar = TRUE;
	app->show_vertical_scrollbar = TRUE;
	app->show_notebook_tabs = TRUE;
        app->views = g_hash_table_new (ludwig_strcase_hash, ludwig_strcase_equal);
        app->doc = NULL;
}

static void
ludwig_app_class_init (GtkObjectClass *object_class)
{
	ludwig_app_parent_class = gtk_type_class (bonobo_window_get_type ());

	object_class->destroy = ludwig_app_destroy;
	object_class->set_arg = ludwig_app_set_arg;
	object_class->get_arg = ludwig_app_get_arg;

	gtk_object_add_arg_type ("LudwigApp::show_horizontal_scrollbar",
                                 GTK_TYPE_BOOL, GTK_ARG_READWRITE,
                                 ARG_VIEW_HSCROLLBAR);
	gtk_object_add_arg_type ("LudwigApp::show_vertical_scrollbar",
                                 GTK_TYPE_BOOL, GTK_ARG_READWRITE,
                                 ARG_VIEW_VSCROLLBAR);
	gtk_object_add_arg_type ("LudwigApp::show_notebook_tabs",
                                 GTK_TYPE_BOOL, GTK_ARG_READWRITE,
                                 ARG_VIEW_TABS);
}

GtkType
ludwig_app_get_type (void)
{
	static GtkType type = 0;

	if (!type)
	{
		GtkTypeInfo info =
		{
			"LudwigApp",
			sizeof (LudwigApp),
			sizeof (LudwigAppClass),
			(GtkClassInitFunc)  ludwig_app_class_init,
			(GtkObjectInitFunc) ludwig_app_init,
			NULL,  /* Reserved 1 */
			NULL,  /* Reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (bonobo_window_get_type (), &info);
	}

	return type;
}

void
ludwig_app_create (LudwigDocument *doc)
{
	CORBA_Environment   ev;
	Bonobo_UIContainer  corba_container;
        GtkWidget          *widget;
	int                 sx, sy;

	singleton = gtk_type_new (ludwig_app_get_type ());

	/* Create the Bonobo container */
	singleton->container = bonobo_ui_container_new ();
	bonobo_ui_container_set_win (singleton->container, BONOBO_WINDOW (singleton));
	corba_container = bonobo_object_corba_objref (BONOBO_OBJECT (singleton->container));

	/* Create the main window */
	widget = bonobo_window_construct (BONOBO_WINDOW (singleton), "Ludwig van", "Ludwig van");
	gtk_signal_connect (GTK_OBJECT (widget), "delete_event",
                            GTK_SIGNAL_FUNC (ludwig_app_exit), NULL);
	gtk_window_set_policy (GTK_WINDOW (widget), TRUE, TRUE, FALSE);

	/* Set the default size of the app window */
	sx = MAX (gdk_screen_width () - 64, 600);
	sy = MAX (gdk_screen_height () - 64, 200);
	sx = (sx * 3) / 4;
	sy = (sy * 3) / 4;
	gtk_widget_set_usize (widget, sx, sy);

	CORBA_exception_init (&ev);

	singleton->component = bonobo_ui_component_new ("Ludwig");
	bonobo_ui_component_set_container (singleton->component, corba_container);
	bonobo_ui_component_add_verb_list_with_data (singleton->component, verbs, singleton);

        bonobo_ui_component_add_listener (singleton->component, "MetricsInches",
                                          metrics_inches_cb, singleton);
        bonobo_ui_component_add_listener (singleton->component, "MetricsCentimeters",
                                          metrics_centimeters_cb, singleton);
        bonobo_ui_component_add_listener (singleton->component, "MetricsMillimeters",
                                          metrics_millimeters_cb, singleton);
        bonobo_ui_component_add_listener (singleton->component, "MetricsPoints",
                                          metrics_points_cb, singleton);

	bonobo_ui_util_set_ui (singleton->component, NULL, "ludwig.xml", "");

	CORBA_exception_free (&ev);

	/* Add the toolbar */
	ludwig_app_create_standard_toolbar ();

	/* Create notebook */
	singleton->notebook = gtk_notebook_new ();
	gtk_notebook_set_scrollable (GTK_NOTEBOOK (singleton->notebook), TRUE);
	gtk_signal_connect (GTK_OBJECT (singleton->notebook), "switch_page",
                            GTK_SIGNAL_FUNC (notebook_page_selected), singleton);
	gtk_widget_show (singleton->notebook);
	bonobo_window_set_contents (BONOBO_WINDOW (singleton), singleton->notebook);

        /* Initialize the tool manager */
        ludwig_tool_manager_create ();

        /* If a document is already opened, set this */
        if (doc != NULL)
        {
                singleton->doc = doc;  // FIXME: Deal with views here
        }

        gtk_widget_show_all (widget);

}

void
ludwig_app_add_view (LudwigView *view)
{
	GtkWidget *label;

	label = gtk_label_new (view->view_frame->name);
	gtk_widget_show (label);
	gtk_notebook_append_page (GTK_NOTEBOOK (singleton->notebook),
                                  GTK_WIDGET (view->view_frame), label);

        /* If this is the first view, grab the focus for the canvas */
        if (singleton->n_views == 0)
                gtk_widget_grab_focus (view->canvas);

        g_hash_table_insert (singleton->views, g_strdup (view->view_frame->name), view);
}

static void
hash_remove_view_cb (gpointer key, gpointer value, gpointer user_data)
{
        LudwigView *view = (LudwigView *) value;

        gtk_object_destroy (GTK_OBJECT (view));
}

void
ludwig_app_close (void)
{
        g_hash_table_foreach (singleton->views, &hash_remove_view_cb, NULL);

	bonobo_object_unref (BONOBO_OBJECT (singleton->container));
}

void
ludwig_app_exit (void)
{
	gtk_main_quit ();
}

void
ludwig_app_create_canvas_popup (void)
{
        GtkWidget *menu;

        menu = gtk_menu_new ();

        bonobo_window_add_popup (BONOBO_WINDOW (singleton), GTK_MENU (menu), "/popups/Canvas");
        gtk_widget_show (menu);

        gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, 3, 0);
}

void
ludwig_app_create_ruler_popup (void)
{
        GtkWidget *menu;

        menu = gtk_menu_new ();

        bonobo_window_add_popup (BONOBO_WINDOW (singleton), GTK_MENU (menu), "/popups/Ruler");
        gtk_widget_show (menu);

        gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, 3, 0);
}

gchar *
ludwig_app_query_profile (void)
{
        if (!singleton->profiles)
        {
                g_warning ("There are no profiles set.  Creating Standard profile\n");
                ludwig_app_add_profile ("Standard");
                singleton->profile = 0;
        }

        return  g_array_index (singleton->profiles, gchar*, singleton->profile);
}

void
ludwig_app_add_profile (const gchar *profile)
{
        gchar *str = strdup (profile);

        g_array_append_val (singleton->profiles, str);
}

void
ludwig_app_remove_profile (const gchar *profile)
{
        int i;
        gchar *tmp;

        for (i = 0; i < singleton->profiles->len; i++)
        {
                tmp = g_array_index (singleton->profiles, gchar*, i);
                if (strcmp (tmp, profile) == 0)
                {
                        g_array_remove_index (singleton->profiles, i);
                        break;
                }
        }
}

static void
ludwig_app_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
	LudwigApp *app;

	app = LUDWIG_APP (object);

	switch(arg_id)
	{
		case ARG_VIEW_HSCROLLBAR:
			app->show_horizontal_scrollbar = GTK_VALUE_BOOL (*arg);
			break;

		case ARG_VIEW_VSCROLLBAR:
			app->show_vertical_scrollbar = GTK_VALUE_BOOL (*arg);
			break;

		case ARG_VIEW_TABS:
			app->show_notebook_tabs = GTK_VALUE_BOOL (*arg);
			gtk_notebook_set_show_tabs (GTK_NOTEBOOK (app->notebook),
                                                    app->show_notebook_tabs);
			break;
	}
}

static void
ludwig_app_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
	LudwigApp *app;

	app = LUDWIG_APP (object);

	switch(arg_id)
	{
		case ARG_VIEW_HSCROLLBAR:
			GTK_VALUE_BOOL (*arg) = app->show_horizontal_scrollbar;
			break;

		case ARG_VIEW_VSCROLLBAR:
			GTK_VALUE_BOOL (*arg) = app->show_vertical_scrollbar;
			break;

		case ARG_VIEW_TABS:
			GTK_VALUE_BOOL (*arg) = app->show_notebook_tabs;
			break;
	}
}

gint
ludwig_app_load (const gchar *filename)
{
	return 0;
}

gint
ludwig_app_save (const gchar *filename)
{
	return 0;
}
