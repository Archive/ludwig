/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include "ludwig-view-staff.h"
#include "ludwig-view-measure.h"

static void ludwig_view_staff_class_init (LudwigViewStaffClass *class);
static void ludwig_view_staff_init       (LudwigViewStaff      *staff);
static void ludwig_view_staff_destroy    (GtkObject            *object);
static void ludwig_view_staff_set_arg    (GtkObject            *object,
                                          GtkArg               *arg,
                                          guint                 arg_id);
static void ludwig_view_staff_get_arg    (GtkObject            *object,
                                          GtkArg               *arg,
                                          guint                 arg_id);

static LudwigViewContainerClass *parent_class = NULL;

enum
{
        ARG_0,
        ARG_CLEF
};

GtkType
ludwig_view_staff_get_type (void)
{
        static GtkType type = 0;

        if (!type)
        {
                GtkTypeInfo info =
                {
                        "LudwigViewStaff",
                        sizeof (LudwigViewStaff),
                        sizeof (LudwigViewStaffClass),
                        (GtkClassInitFunc) ludwig_view_staff_class_init,
                        (GtkObjectInitFunc) ludwig_view_staff_init,
                        NULL, /* Reserved 1 */
                        NULL, /* Reserved 2 */
                        (GtkClassInitFunc) NULL
                };

                type = gtk_type_unique (ludwig_view_container_get_type (), &info);
        }

        return type;
}

void
ludwig_view_staff_construct (LudwigViewStaff *staff)
{
        GnomeCanvasItem *measure;
        LudwigViewContainer *container;

        measure = gnome_canvas_item_new (GNOME_CANVAS_GROUP (staff),
                                         ludwig_view_measure_get_type (),
                                         "x", 0.0,
                                         "y", 0.0,
                                         "container", LUDWIG_VIEW_ITEM (staff),
                                         NULL);

        container = (LudwigViewContainer *) staff;
        container->items = g_list_append (container->items, measure);

        ludwig_view_measure_construct (LUDWIG_VIEW_MEASURE (measure));

        /* FIXME: Temporary */
        ludwig_view_measure_insert_note (measure, 4, FALSE, FALSE);
}

static void
ludwig_view_staff_class_init (LudwigViewStaffClass *class)
{
        GtkObjectClass *object_class;

        object_class = (GtkObjectClass *) class;

        parent_class = gtk_type_class (ludwig_view_container_get_type ());

        gtk_object_add_arg_type ("LudwigViewStaff::clef",
                                 GTK_TYPE_INT,
                                 GTK_ARG_WRITABLE,
                                 ARG_CLEF);

        object_class->destroy = ludwig_view_staff_destroy;
        object_class->set_arg = ludwig_view_staff_set_arg;
        object_class->get_arg = ludwig_view_staff_get_arg;
};

static void
ludwig_view_staff_init (LudwigViewStaff *staff)
{
        staff->start_clef = CLEF_TREBLE;
}

static void
ludwig_view_staff_destroy (GtkObject *object)
{
}

static void
ludwig_view_staff_set_arg (GtkObject *object,
                           GtkArg    *arg,
                           guint      arg_id)
{
}

static void
ludwig_view_staff_get_arg (GtkObject *object,
                           GtkArg    *arg,
                           guint      arg_id)
{
}
