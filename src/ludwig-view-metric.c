/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-util.h>

#include "ludwig-view-metric.h"

#include <ludwig-font/ludwig-font.h>

#include <canvas-items/gp-path.h>

static void ludwig_view_metric_class_init (LudwigViewMetricClass *class);
static void ludwig_view_metric_init       (LudwigViewMetric      *metric);
static void ludwig_view_metric_destroy    (GtkObject             *object);
static void ludwig_view_metric_set_arg    (GtkObject             *object,
                                           GtkArg                *arg,
                                           guint                  arg_id);
static void ludwig_view_metric_get_arg    (GtkObject             *object,
                                           GtkArg                *arg,
                                           guint                  arg_id);

struct _LudwigViewMetricPrivate
{
        LudwigFont *font;
        LudwigFontGlyph *glyph;
        int code;
};

enum
{
        ARG_0,
        ARG_METRIC_VALUE,
        ARG_DOTTED,
        ARG_DOUBLED
};

static LudwigViewItemClass *parent_class = NULL;

GtkType
ludwig_view_metric_get_type (void)
{
        static GtkType type = 0;

        if (!type)
        {
                GtkTypeInfo info =
                {
                        "LudwigViewMetric",
                        sizeof (LudwigViewMetric),
                        sizeof (LudwigViewMetricClass),
                        (GtkClassInitFunc)  ludwig_view_metric_class_init,
                        (GtkObjectInitFunc) ludwig_view_metric_init,
                        NULL, /* Reserved 1 */
                        NULL, /* Reserved 2 */
                        (GtkClassInitFunc) NULL
                };

                type = gtk_type_unique (ludwig_view_item_get_type (), &info);
        }

        return type;
}

void
ludwig_view_metric_construct (LudwigViewMetric *metric)
{
        GnomeCanvasItem *item;
        GnomeCanvasPoints *points;
        LudwigFont *font;
        LudwigFontGlyph *glyph;
        int code;
        GPPath *path;
        double affine[6];

        font = (LudwigFont *) ludwig_get_standard_font ();
        code = ludwig_font_face_lookup (font->face, 0, metric->code);
        glyph = ludwig_vfont_get_glyph ((LudwigVFont *) font, code);
        path = gp_path_new_from_foreign_bpath (ludwig_font_glyph_get_bpath (glyph, TRUE));

        item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (metric),
                                      gnome_canvas_bpath_get_type (),
                                      "bpath", path,
                                      "fill_color_rgba", 0x00ff,
                                      NULL);

        art_affine_scale (affine, 0.75, 0.75);
        affine[3] = 0.0 - affine[3];
        gnome_canvas_item_affine_absolute (item, affine);

        /* Add the stem */
        points = gnome_canvas_points_new (2);
        points->coords[0] = 0.5;
        points->coords[1] = 0.0;
        points->coords[2] = 0.5;
        points->coords[3] = 36.0;

        item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (metric),
                                      gnome_canvas_line_get_type (),
                                      "width_units", 1.0,
                                      "fill_color_rgba", 0x00ff,
                                      "points", points,
                                      NULL);

        gnome_canvas_points_unref (points);
}

static void
ludwig_view_metric_class_init (LudwigViewMetricClass *class)
{
        GtkObjectClass *object_class;

        object_class = (GtkObjectClass *) class;

        parent_class = gtk_type_class (ludwig_view_item_get_type ());

        gtk_object_add_arg_type ("LudwigViewMetric::metric_value",
                                 GTK_TYPE_INT,
                                 GTK_ARG_READWRITE,
                                 ARG_METRIC_VALUE);
        gtk_object_add_arg_type ("LudwigViewMetric::dotted",
                                 GTK_TYPE_INT,
                                 GTK_ARG_READWRITE,
                                 ARG_DOTTED);
        gtk_object_add_arg_type ("LudwigViewMetric::doubled",
                                 GTK_TYPE_INT,
                                 GTK_ARG_READWRITE,
                                 ARG_DOUBLED);

        object_class->destroy = ludwig_view_metric_destroy;
        object_class->set_arg = ludwig_view_metric_set_arg;
        object_class->get_arg = ludwig_view_metric_get_arg;
};

static void
ludwig_view_metric_init (LudwigViewMetric *metric)
{
        metric->priv = g_new0 (LudwigViewMetricPrivate, 1);

        metric->priv->font = (LudwigFont *) ludwig_get_standard_font ();
}

static void
ludwig_view_metric_destroy (GtkObject *object)
{
}

static void
ludwig_view_metric_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
        LudwigViewMetric *metric;

        metric = (LudwigViewMetric *) object;

        switch (arg_id)
        {
        case ARG_METRIC_VALUE:
                metric->value = GTK_VALUE_INT (*arg);

                switch (metric->value)
                {
                case 1:
                        metric->code = 119;
                        break;

                case 2:
                        metric->code = 205;
                        break;

                default:
                        metric->code = 222;
                        break;
                }

                break;

        case ARG_DOTTED:
                metric->is_dotted = GTK_VALUE_INT (*arg);
                break;

        case ARG_DOUBLED:
                metric->is_double = GTK_VALUE_INT (*arg);
                break;
        }
}

static void
ludwig_view_metric_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
        LudwigViewMetric *metric;

        metric = (LudwigViewMetric *) object;

        switch (arg_id)
        {
        case ARG_METRIC_VALUE:
                GTK_VALUE_INT (*arg) = metric->value;
                break;

        case ARG_DOTTED:
                GTK_VALUE_INT (*arg) = metric->is_dotted;
                break;

        case ARG_DOUBLED:
                GTK_VALUE_INT (*arg) = metric->is_double;
                break;
        }
}
