#ifndef __LUDWIG_DISPLAY_METRICS_H__
#define __LUDWIG_DISPLAY_METRICS_H__

#include <glib.h>
#include <gtk/gtkruler.h>

typedef enum
{
	METRIC_NONE,
	METRIC_MM,
	METRIC_CM,
	METRIC_IN,
	METRIC_PT
} LudwigDisplayMetric;

#define PT_PER_IN 72.0
#define CM_PER_IN 2.54
#define MM_PER_IN 25.4
#define IN_PER_PT (1 / PT_PER_IN)
#define IN_PER_CM (1 / CM_PER_IN)
#define IN_PER_MM (1 / MM_PER_IN)
#define PT_PER_CM (PT_PER_IN * IN_PER_CM)
#define CM_PER_PT (1 / PT_PER_CM)
#define PT_PER_MM (PT_PER_IN * IN_PER_MM)
#define MM_PER_PT (1 / PT_PER_MM)
#define PT_PER_PT 1.0
#define IN_PER_IN 1.0

#define LUDWIG_DISPLAY_DEFAULT_METRIC METRIC_MM

gdouble ludwig_absolute_metric_to_metric (gdouble length_src,
                                          LudwigDisplayMetric metric_src,
                                          LudwigDisplayMetric metric_dest);
gchar  *ludwig_metric_to_metric_string (gdouble length,
                                        LudwigDisplayMetric metric_src,
                                        LudwigDisplayMetric metric_dest,
                                        gboolean m);

#define LUDWIG_METRIC_TO_PT(l,m) ludwig_absolute_metric_to_metric (l, m, METRIC_PT)
#define LUDWIG_PT_TO_METRIC(l,m) ludwig_absolute_metric_to_metric (l, METRIC_PT, m)
#define LUDWIG_PT_TO_METRIC_STRING(l,m) ludwig_metric_to_metric_string(l, METRIC_PT, m, TRUE)
#define LUDWIG_PT_TO_STRING(l,m) ludwig_metric_to_metric_string(l, METRIC_PT, m, FALSE)

static const GtkRulerMetric ludwig_display_ruler_metrics[] =
{
	{ "Pixels",      "Pi", PT_PER_PT, { 1, 2, 5, 10, 25, 50, 100, 250, 500, 1000 }, { 1, 5, 10, 50, 100 }},
	{ "Millimeters", "Mm", PT_PER_MM, { 1, 2, 5, 10, 25, 50, 100, 250, 500, 1000 }, { 1, 5, 10, 50, 100 }},
	{ "Centimeters", "Cm", PT_PER_CM, { 1, 2, 5, 10, 25, 50, 100, 250, 500, 1000 }, { 1, 5, 10, 50, 100 }},
	{ "Inches",      "In", PT_PER_IN, { 1, 2, 5, 10, 25, 50, 100, 250, 500, 1000 }, { 1, 5, 10, 50, 100 }},
	{ "Points",      "Pt", PT_PER_PT, { 1, 2, 5, 10, 25, 50, 100, 250, 500, 1000 }, { 1, 5, 10, 50, 100 }},
};

#endif /* __LUDWIG_DISPLAY_METRICS_H__ */
