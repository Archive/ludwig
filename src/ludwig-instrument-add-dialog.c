/*
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * Ludwig van is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <config.h>

#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-util.h>

#include <gdk-pixbuf/gdk-pixbuf.h>

#include <gal/e-table/e-tree.h>
#include <gal/e-table/e-tree-scrolled.h>
#include <gal/e-table/e-tree-memory-callbacks.h>
#include <gal/e-table/e-tree-memory.h>
#include <gal/e-table/e-cell-text.h>
#include <gal/e-table/e-cell-tree.h>

#include <ludwig-font/ludwig-font.h>
#include <ludwig-font/ludwig-font-glyph.h>
#include <ludwig-font/ludwig-rfont.h>

#include <canvas-items/gp-path.h>

#include "ludwig-app.h"
#include "ludwig-instrument-add-dialog.h"
#include "ludwig-instrument-list.h"

#include "art/empty.xpm"
#include "art/check-filled.xpm"
#include "art/check-unfilled.xpm"

static struct
{
	char **image_base;
	GdkPixbuf  *pixbuf;
} check_state_pixmaps [] =
{
        { check_unfilled_xpm,   NULL },
	{ check_filled_xpm,	NULL },
        { empty_xpm,            NULL },
	{ NULL,			NULL }
};

typedef struct _TreeNode TreeNode;

struct _TreeNode
{
	gint        create;
	gint        solo;
        gint        clef;
	gchar      *name;
        gboolean    group;
        int         position;
};

/*
 * Column list:
 *  0 Create
 *  1 Solo     (checkbox)
 *  2 Title
 */
static const gchar *spec =
"<ETableSpecification cursor-mode=\"line\"              \
     selection-mode=\"browse\" draw-focus=\"true\">     \
   <ETableColumn model_col=\"0\" _title=\"Create\"      \
       expansion=\"1.0\" minimum_width=\"40\"           \
       resizable=\"false\" cell=\"string\"              \
       compare=\"string\"/>                             \
   <ETableColumn model_col=\"1\" _title=\"Solo\"        \
       expansion=\"1.0\" minimum_width=\"30\"           \
       resizable=\"false\" cell=\"render_checkbox\"     \
       compare=\"integer\"/>	                        \
   <ETableColumn model_col=\"2\"                        \
       _title=\"Name\" expansion=\"1.0\"                \
       minimum_width=\"20\" resizable=\"true\"          \
       cell=\"render_tree\" compare=\"string\"/>        \
   <ETableState>                                        \
     <column source=\"0\"/>                             \
     <column source=\"1\"/>                             \
     <column source=\"2\"/>                             \
     <column source=\"3\"/>                             \
     <grouping></grouping>                              \
   </ETableState>                                       \
 </ETableSpecification>";

static GnomeDialogClass          *parent_class = NULL;
static LudwigInstrumentAddDialog *singleton = NULL;
static gint                       unknown = 0;
static gint                       items;

/* ETreeSimple callbacks */
static GdkPixbuf *
etree_icon_at (ETreeModel *model, ETreePath path, void *model_data)
{
        return NULL;
}

static gint
etree_col_count (ETreeModel *model, void *data)
{
	return COL_LAST;
}

static void *
etree_value_at (ETreeModel *model, ETreePath path, int col, void *model_data)
{
	TreeNode *node;
        char str[3];

	node = e_tree_memory_node_get_data (E_TREE_MEMORY (model), path);

	switch (col)
	{
		case COL_CREATE:
                        if (node->group == TRUE)
                        {
                                return NULL;
                        }
                        else
                        {
                                sprintf (str, "%2d", node->create);
                                return g_strdup (str);
                        }

			break;

		case COL_SOLO:
                        if (node->group == TRUE)
                        {
                                return GINT_TO_POINTER (2);
                        }
                        else
                        {
                                return GINT_TO_POINTER (node->solo);
                        }

			break;

		case COL_NAME:
                        return g_strdup (node->name);
			break;

                case COL_POSITION:
                        return GINT_TO_POINTER (node->position);
                        break;

		default:
			return NULL;
	}
}

static void
etree_set_value_at (ETreeModel *model, ETreePath path, int col, const void *val, void *data)
{
	TreeNode *node;

	node = e_tree_memory_node_get_data (E_TREE_MEMORY (model), path);

	switch (col)
	{
                case COL_CREATE:
                        if (node->group == FALSE)
                        {
                                node->create = atoi (val);
                        }
			break;

		case COL_SOLO:
                        if (node->group == FALSE)
                        {
                                node->solo = !node->solo;
                        }
			break;

		case COL_NAME:
			if (node->name != NULL)
				g_free (node->name);
			node->name = g_strdup (val);
			break;

                case COL_POSITION:
                        node->position = GPOINTER_TO_INT (val);
                        break;
	}

	e_tree_memory_node_set_data (E_TREE_MEMORY (model), path, node);
}

static gboolean
etree_is_editable (ETreeModel *model, ETreePath path, int col, void *model_data)
{
        switch (col)
        {
                case COL_CREATE:
                        return FALSE;

                case COL_SOLO:
                        return TRUE;

                case COL_NAME:
                        return FALSE;

                case COL_POSITION:
                        return FALSE;
        }
}

static void *
etree_duplicate_value (ETreeModel *model, int col, const void *value, void *data)
{
	if (col == COL_CREATE || col == COL_NAME)
		return g_strdup (value);
	else
		return (void *) value;
}

static void
etree_free_value (ETreeModel *model, int col, void *value, void *data)
{
	if (col == COL_CREATE || col == COL_NAME)
		g_free (value);
}

static void *
etree_init_value (ETreeModel *model, int col, void *data)
{
	switch (col)
	{
		case COL_CREATE:
			return 0;

		case COL_SOLO:
			return 0;
			break;

		case COL_NAME:
			return 0;
			break;

                case COL_POSITION:
                        return 0;
	}
}

static gboolean
etree_value_is_empty (ETreeModel *model, int col, const void *value, void *data)
{
	if (col == COL_CREATE || col == COL_NAME)
		return !(value && *(char *) value);
	else
		return (value == 0);
}

static char *
etree_value_to_string (ETreeModel *model, int col, const void *value, void *data)
{
	if (col == COL_CREATE || col == COL_NAME)
		return g_strdup (value);
	else
		return g_strdup_printf ("%d", value);
}

static GtkWidget *
create_canvas (char *name)
{
	GtkWidget *canvas;
	LudwigInstrument *instrument;
	LudwigClef clef;
	GnomeCanvasItem *item;
	LudwigFont *font;
	LudwigFontGlyph *glyph;
	int code;
	int clef_code;
	GPPath *path;
	GnomeCanvasPoints *points;
	GnomeCanvasGroup *group;
	double affine[6];
	int i;

	instrument = ludwig_instrument_list_query_instrument ((const char *) name);
	clef = ludwig_instrument_query_default_clef (instrument);

	canvas = gnome_canvas_new_aa ();
	group = (GnomeCanvasGroup *) gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (canvas)),
                                                            gnome_canvas_group_get_type (),
                                                            "x", -20.0,
                                                            "y", 32.0,
                                                            NULL);

	/* Create five horizontal lines */
	for (i = 0; i < 5; i++)
	{
		points = gnome_canvas_points_new (2);
		points->coords[0] = 0.0;
		points->coords[1] = 9.0 * i;
		points->coords[2] = 150.0;
		points->coords[3] = 9.0 * i;

		item = gnome_canvas_item_new (group,
                                              gnome_canvas_line_get_type (),
                                              "width_units", 1.0,
                                              "fill_color", "black",
                                              "points", points,
                                              NULL);

		gnome_canvas_points_unref (points);

		art_affine_scale (affine, 1.0, 1.0);
		gnome_canvas_item_affine_absolute (item, affine);
	}

	/* Setup a barline object */
	points = gnome_canvas_points_new (2);
	points->coords[0] = 150.0 - 0.5;
	points->coords[1] = 0.0;
	points->coords[2] = 150.0 - 0.5;
	points->coords[3] = 36.0;

	item = gnome_canvas_item_new (group,
                                      gnome_canvas_line_get_type (),
                                      "width_units", 1.0,
                                      "fill_color", "black",
                                      "points", points,
                                      NULL);

	gnome_canvas_points_unref (points);

	/* Setup the clef object */
	switch (clef)
	{
	case CLEF_TREBLE:
		clef_code = 38;
		art_affine_scale (affine, 0.8, 0.8);
		affine[4] = 8.0;
		affine[5] = 27.0;
		break;

	case CLEF_ALTO:
		clef_code = 66;
		art_affine_scale (affine, 0.76, 0.76);
		affine[4] = 4.0;
		affine[5] = 18.0;
		break;

	case CLEF_BASS:
		clef_code = 63;
		art_affine_scale (affine, 1.0, 1.0);
		affine[4] = 4.0;
		affine[5] = 11.0;
		break;
	}

	font = (LudwigFont *) ludwig_get_standard_font ();
	code = ludwig_font_face_lookup (font->face, 0, clef_code);
	glyph = ludwig_vfont_get_glyph ((LudwigVFont *) font, code);
	path = gp_path_new_from_foreign_bpath (ludwig_font_glyph_get_bpath (glyph, TRUE));

	item = gnome_canvas_item_new (group,
                                      gnome_canvas_bpath_get_type (),
                                      "bpath", path,
                                      "fill_color_rgba", 0x00ff,
                                      NULL);

	gnome_canvas_item_affine_relative (item, affine);
}

static void
value_changed_cb (GtkWidget *widget, GtkWidget *spin)
{
	TreeNode *node;

	node = e_tree_memory_node_get_data (E_TREE_MEMORY (singleton->model), singleton->selected_path);
	node->create = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (spin));
	gtk_widget_queue_draw (GTK_WIDGET (singleton->tree));
}

static GtkWidget *
create_notebook_page (char *name)
{
	GtkWidget *frame;
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *spin;
	GtkWidget *label;
	GtkWidget *check;
	GtkWidget *hsep;
	GtkWidget *canvas;
	GtkAdjustment *adj;

	/* Create frame */
	frame = gtk_frame_new (name);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (frame), vbox);
	hbox = gtk_hbox_new (FALSE, 0);

	/* Adjustment for spin button */
	adj = (GtkAdjustment *) gtk_adjustment_new (0.0, 0.0, 10.0, 1.0, 5.0, 0.0);

	/* Spin button */
	spin = gtk_spin_button_new (adj, 0, 0);
	gtk_box_pack_start (GTK_BOX (hbox), spin, FALSE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
                            GTK_SIGNAL_FUNC (value_changed_cb), spin);

	label = gtk_label_new (_("Number of instruments to insert.."));
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	/* Check button */
	check = gtk_check_button_new_with_label (_("Is this a solo instrument?"));
	gtk_box_pack_start (GTK_BOX (vbox), check, FALSE, FALSE, 0);

	/* Separator */
	hsep = gtk_hseparator_new ();
	gtk_box_pack_start (GTK_BOX (vbox), hsep, FALSE, FALSE, 0);

	/* Canvas */
	canvas = create_canvas (name);
	gtk_widget_set_usize (canvas, 100, 100);
	gtk_box_pack_start (GTK_BOX (vbox), canvas, FALSE, FALSE, 0);

	return frame;
}

static int
etree_clicked (ETree *tree, int row, ETreePath path, int col, GdkEvent *evt)
{
	TreeNode *node;
	GtkWidget *page;
	int n;

	singleton->selected_path = path;

	node = e_tree_memory_node_get_data (E_TREE_MEMORY (singleton->model), path);

	if (!node->group)
	{
		page = g_hash_table_lookup (singleton->notebook_pages, node->name);

		if (!page)
		{
			g_print ("ERROR: No page in etree_clicked()\n");
			page = create_notebook_page (node->name);
			gtk_widget_show_all (page);
			gtk_notebook_append_page (GTK_NOTEBOOK (singleton->notebook), page, NULL);

			singleton->pages++;
			n = gtk_notebook_page_num (GTK_NOTEBOOK (singleton->notebook), page);
			gtk_notebook_set_page (GTK_NOTEBOOK (singleton->notebook), n);

			g_hash_table_insert (singleton->notebook_pages,
                                             g_strdup (node->name),
                                             page);
		}
		else
		{
			n = gtk_notebook_page_num (GTK_NOTEBOOK (singleton->notebook), page);
			gtk_notebook_set_page (GTK_NOTEBOOK (singleton->notebook), n);
		}
	}
	else
	{
		page = g_hash_table_lookup (singleton->notebook_pages, node->name);
		n = gtk_notebook_page_num (GTK_NOTEBOOK (singleton->notebook), page);
		gtk_notebook_set_page (GTK_NOTEBOOK (singleton->notebook), n);
	}

	return FALSE;
}

static void
init_check_images (void)
{
	int i;

	if (check_state_pixmaps [0].pixbuf)
		return;

	for (i = 0; check_state_pixmaps [i].image_base != NULL; i++)
        {
		check_state_pixmaps [i].pixbuf = gdk_pixbuf_new_from_xpm_data ((const char **) check_state_pixmaps [i].image_base);
	}
}

static void
ludwig_instrument_add_dialog_init (LudwigInstrumentAddDialog *dialog)
{
        int x = 5;

	dialog->tree = NULL;
	dialog->notebook_pages = g_hash_table_new (ludwig_strcase_hash,
                                                   ludwig_strcase_equal);
}

static void
ludwig_instrument_add_dialog_real_destroy (GtkObject *object)
{
	LudwigInstrumentAddDialog *dialog = (LudwigInstrumentAddDialog *) object;

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
ludwig_instrument_add_dialog_class_init (LudwigInstrumentAddDialogClass *class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	object_class->destroy = ludwig_instrument_add_dialog_real_destroy;

        parent_class = gtk_type_class (gnome_dialog_get_type ());

        init_check_images ();
}

static int
column_sort_cb (ETreeMemory *mem, ETreePath path1, ETreePath path2, gpointer closure)
{
        TreeNode *node1;
        TreeNode *node2;

        node1 = e_tree_memory_node_get_data (mem, path1);
        node2 = e_tree_memory_node_get_data (mem, path2);

        return node1->position > node2->position;
}

static ETreePath
insert_item (ETreeModel *model, ETreePath parent_node, const char *name, int clef, gboolean is_group, int pos)
{
	ETreePath  parent;
        ETreePath  path;
	TreeNode  *node;
	gchar *id;
	int n;
	GtkWidget *page;

	/* Create node data */
	node = g_new0 (TreeNode, 1);

	if (name == NULL)
		node->name = g_strdup_printf ("Unknown%d", unknown++);
	else
		node->name = g_strdup (name);

	node->clef = clef;
	node->create = 0;
	node->solo = 0;
        node->group = is_group;
        node->position = pos;

	id = g_strdup_printf ("Child %d", ++items);

	if (parent_node == NULL)
		parent = e_tree_model_get_root (model);
	else
		parent = parent_node;

        path = e_tree_memory_node_insert_id (singleton->memory,
                                             parent,
                                             0,
                                             node,
                                             id);

        singleton->paths = g_slist_append (singleton->paths, path);

        e_tree_memory_sort_node (singleton->memory,
                                 parent,
                                 column_sort_cb,
                                 NULL);

	/* Create page for notebook */
	if (!node->group)
	{
		page = create_notebook_page (node->name);
		gtk_widget_show_all (page);
		gtk_notebook_append_page (GTK_NOTEBOOK (singleton->notebook), page, NULL);
		singleton->pages++;

		g_hash_table_insert (singleton->notebook_pages,
                                     g_strdup (node->name),
                                     page);
	}
	else
	{
		page = gtk_frame_new (node->name);
		gtk_widget_show (page);
		gtk_notebook_append_page (GTK_NOTEBOOK (singleton->notebook), page, NULL);
		singleton->pages++;

		g_hash_table_insert (singleton->notebook_pages,
                                     g_strdup (node->name),
                                     page);
	}

	return path;
}

static ETableExtras *
create_extras (void)
{
	ETableExtras *extras;
	ECell *cell;
        GdkPixbuf *check_images[3];
        GdkPixbuf *clef_images[4];
        int i;

	extras = e_table_extras_new ();

        e_table_extras_add_pixbuf (extras, "empty", check_state_pixmaps[0].pixbuf);
        e_table_extras_add_pixbuf (extras, "filled", check_state_pixmaps[1].pixbuf);
        e_table_extras_add_pixbuf (extras, "unfilled", check_state_pixmaps[2].pixbuf);

	cell = e_cell_text_new (NULL, GTK_JUSTIFY_LEFT);

	e_table_extras_add_cell (extras, "render_tree",
                                 e_cell_tree_new (NULL, NULL, TRUE, cell));

        for (i = 0; i < 3; i++)
                check_images[i] = check_state_pixmaps[i].pixbuf;

        e_table_extras_add_cell (extras, "render_checkbox",
                                 e_cell_toggle_new (0, 3, check_images));

	return extras;
}

/* Button callbacks */
static void
button_add_cb (GtkWidget *widget, gpointer data)
{
        insert_item (singleton->model, NULL, NULL, 0, FALSE, 0);
}

static void
button_edit_cb (GtkWidget *widget, gpointer data)
{
        /* Edit the selected row's properties? */
}

static void
button_remove_cb (GtkWidget *widget, gpointer data)
{
        LudwigInstrumentAddDialog *dialog;
        ETreeMemory *mem;
        char *str;
        ETreePath selected_node;

	items--;

        dialog = (LudwigInstrumentAddDialog *) data;

        mem = dialog->memory;
        selected_node = e_tree_get_cursor (E_TREE (dialog->tree));

        if (selected_node == NULL)
                return;

        str = (char *) e_tree_memory_node_remove (mem, selected_node);
        g_free (str);
}

static void
dialog_ok_cb (GtkWidget *widget, gpointer data)
{
	LudwigInstrumentAddDialog *dialog;
        LudwigInstrument *instrument;
        LudwigDocument *doc;
        ETreePath path;
        TreeNode *node;
        GSList *tmp;
	int rows;
        int i;

	dialog = data;
        doc = ludwig_app_get_document ();

	rows = e_tree_row_count (dialog->tree);

        tmp = singleton->paths;
        while (tmp != NULL)
        {
                node = e_tree_memory_node_get_data (E_TREE_MEMORY (dialog->model), tmp->data);

                if (node->create)
                {
			for (i = 0; i < node->create; i++)
			{
				instrument = ludwig_instrument_list_query_instrument (node->name);
				ludwig_document_add_part (doc, instrument, node->clef);
			}
                }

                tmp = g_slist_next (tmp);
        }

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
dialog_cancel_cb (GtkWidget *widget, gpointer data)
{
	LudwigInstrumentAddDialog *dialog = data;

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
install_instrument (const gchar *instrument, int clef, int pos, gpointer user_data)
{
        ETreePath path = user_data;

        insert_item (singleton->model,
                     path,
                     instrument,
                     clef,
                     FALSE,
                     pos);
}

static void
install_group (const char *group, int pos)
{
        ETreePath path;

        path = insert_item (singleton->model,
                            e_tree_model_get_root (singleton->model),
                            group,
                            0,
                            TRUE,
                            pos);

        ludwig_instrument_list_foreach_instrument_in_group (group, &install_instrument, path);
}

static void
populate_tree (void)
{
	ludwig_instrument_list_foreach_group (install_group);
}

/* Public interface */
GtkType
ludwig_instrument_add_dialog_get_type (void)
{
	static GtkType type = 0;

	if (!type)
	{
		const GtkTypeInfo info =
		{
			"LudwigInstrumentAddDialog",
			sizeof (LudwigInstrumentAddDialog),
			sizeof (LudwigInstrumentAddDialogClass),
			(GtkClassInitFunc)  ludwig_instrument_add_dialog_class_init,
			(GtkObjectInitFunc) ludwig_instrument_add_dialog_init,
			NULL, /* Reserved 1 */
			NULL, /* Reserved 2 */
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (gnome_dialog_get_type (), &info);
	}

	return type;
}

void
ludwig_instrument_add_dialog_create (void)
{
	LudwigInstrumentAddDialog *dialog;
	ETreeMemory  *mem;
        ETreePath     root_node;
	ETableExtras *extras;
	GtkWidget    *hbox;
	GtkWidget    *vbox;
	GtkWidget    *scrolled;
        GtkWidget    *button;
        GtkWidget    *widget;

	ludwig_instrument_list_create ();

        /* Create the dialog */
	dialog = gtk_type_new (ludwig_instrument_add_dialog_get_type ());
	singleton = dialog;
        gtk_window_set_title (GTK_WINDOW (dialog), _("Instruments in the score"));

	/* Install buttons */
        gnome_dialog_append_button (GNOME_DIALOG (dialog),
                                    GNOME_STOCK_BUTTON_OK);
	gnome_dialog_button_connect (GNOME_DIALOG (dialog),
                                     0,
                                     GTK_SIGNAL_FUNC (dialog_ok_cb),
                                     dialog);
        gnome_dialog_append_button (GNOME_DIALOG (dialog),
                                    GNOME_STOCK_BUTTON_CANCEL);
	gnome_dialog_button_connect (GNOME_DIALOG (dialog),
                                     1,
                                     GTK_SIGNAL_FUNC (dialog_cancel_cb),
                                     dialog);

	/* Create the notebook */
	dialog->notebook = gtk_notebook_new ();
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (dialog->notebook), FALSE);

        /* Create the ETree */
        dialog->model = e_tree_memory_callbacks_new (etree_icon_at,
                                                     etree_col_count,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     etree_value_at,
                                                     etree_set_value_at,
                                                     etree_is_editable,
                                                     etree_duplicate_value,
                                                     etree_free_value,
                                                     etree_init_value,
                                                     etree_value_is_empty,
                                                     etree_value_to_string,
                                                     dialog);

        mem = E_TREE_MEMORY (dialog->model);
        dialog->memory = mem;

        root_node = e_tree_memory_node_insert (mem,
                                               NULL,
                                               0,
                                               NULL);

	extras = create_extras ();

        scrolled = e_tree_scrolled_new (dialog->model, extras, spec, NULL);
        dialog->tree = e_tree_scrolled_get_tree (E_TREE_SCROLLED (scrolled));
        e_tree_load_state (E_TREE (dialog->tree), "header_state");
        e_tree_load_expanded_state (E_TREE (dialog->tree), "expanded_state");
        e_tree_root_node_set_visible (E_TREE (dialog->tree), FALSE);
	gtk_signal_connect (GTK_OBJECT (dialog->tree), "click",
                            GTK_SIGNAL_FUNC (etree_clicked), NULL);

        gtk_widget_set_usize (scrolled, 240, 150);

        populate_tree ();

        /* Create widgets */
        hbox = gtk_hbox_new (FALSE, 0);
        gtk_box_pack_start (GTK_BOX (hbox), scrolled, FALSE, FALSE, 0);
        gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), hbox, FALSE, FALSE, 0);

	/* Insert notebook */
	gtk_box_pack_start (GTK_BOX (hbox), dialog->notebook, FALSE, FALSE, 0);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), hbox, FALSE, FALSE, 0);

	button = gtk_button_new_with_label (_("Add"));
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
                            GTK_SIGNAL_FUNC (button_add_cb), dialog);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);

	button = gtk_button_new_with_label (_("Edit"));
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
                            GTK_SIGNAL_FUNC (button_edit_cb), dialog);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);

	button = gtk_button_new_with_label (_("Remove"));
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
                            GTK_SIGNAL_FUNC (button_remove_cb), dialog);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);

	gtk_widget_show_all (GTK_WIDGET (dialog));
}
