/*
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <gtk/gtkobject.h>
#include <glib.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <gnome-xml/xmlmemory.h>

#include "ludwig-util.h"
#include "ludwig-config.h"
#include "ludwig-instrument-list.h"
#include "ludwig-instrument.h"

static void ludwig_instrument_list_load             (void);
static void ludwig_instrument_list_save             (void);
static void ludwig_instrument_list_SAX_startElement (void *ctx,
                                                     const xmlChar *full,
                                                     const xmlChar **atts);
static void ludwig_instrument_list_SAX_endElement   (void *ctx,
                                                     const xmlChar *fullname);
static void ludwig_instrument_list_SAX_characters   (void *ctx,
                                                     const xmlChar *ch,
                                                     int len);

typedef struct _LILInstrumentCtxt LILInstrumentCtxt;

struct _LILInstrumentCtxt
{
        LILInstrumentGroupFunc func;
        gpointer               data;
};

static GtkObject            *parent_class = NULL;
static LudwigInstrumentList *singleton = NULL;

/* GtkObject overloads */
static void
instrument_destroy (gpointer key,
                    gpointer value,
                    gpointer data)
{
        LudwigInstrument *instrument = (LudwigInstrument *) value;

	gtk_object_destroy (GTK_OBJECT (instrument));
}

static void
ludwig_instrument_list_destroy (GtkObject *object)
{
	LudwigInstrumentList *list;

	list = LUDWIG_INSTRUMENT_LIST (object);

	g_hash_table_foreach (list->records, &instrument_destroy, NULL);

	gtk_object_unref (GTK_OBJECT (list->gconf_client));
}

static void
ludwig_instrument_list_class_init (LudwigInstrumentListClass *class)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (class);
	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = ludwig_instrument_list_destroy;
}

static void
ludwig_instrument_list_init (GtkObject *object)
{
	LudwigInstrumentList *list = LUDWIG_INSTRUMENT_LIST (object);

	list->gconf_client = NULL;
	list->records = NULL;
        list->groups = NULL;
}

/* ************ SAX ******************** */

xmlSAXHandler instrumentSAXHandlerData =
{
	NULL, /* internalSubset */
	NULL, /* isStandalone */
	NULL, /* hasInternalSubset */
	NULL, /* hasExternalSubset */
	NULL, /* resolveEntity */
	NULL, /* getEntity */
	NULL, /* entityDecl */
	NULL, /* notationDecl */
	NULL, /* attributeDecl */
	NULL, /* elementDecl */
	NULL, /* unparsedEntityDecl */
	NULL, /* setDocumentLocator */
	NULL, /* startDocument */
	NULL, /* endDocument */
	ludwig_instrument_list_SAX_startElement, /* startElement */
	ludwig_instrument_list_SAX_endElement, /* endElement */
	NULL, /* reference */
	ludwig_instrument_list_SAX_characters, /* characters */
	NULL, /* ignorableWhitespace */
	NULL, /* processingInstruction */
	NULL, /* comment */
	NULL, /* xmlParserWarning */
	NULL, /* xmlParserError */
	NULL, /* xmlParserError */
	NULL, /* getParameterEntity */
	NULL, /* cdataBlock */
};

xmlSAXHandler *instrumentSAXHandler = &instrumentSAXHandlerData;

static void
ludwig_instrument_list_load (void)
{
	gchar *path = "instrument-list.xml";
	xmlSAXUserParseFile (instrumentSAXHandler, NULL, path);
}

static void
ludwig_instrument_list_save (void)
{
	xmlDoc *doc;
	xmlNode *node;

	doc = xmlNewDoc ("1.0");
	/* Save the file here */
}

static void
ludwig_instrument_list_SAX_startElement (void *ctx,
                                         const xmlChar *fullname,
                                         const xmlChar **atts)
{
}

static void
ludwig_instrument_list_SAX_endElement (void *ctx, const xmlChar *fullname)
{
}

static void
ludwig_instrument_list_SAX_characters (void *ctx, const xmlChar *ch, int len)
{
}


/*******************/


/* XML loading code */

static void
parse_xml_instrument_translation (xmlNode *parent)
{
        xmlNode *node;
        xmlChar *locale = NULL;

        locale = xmlGetProp (parent, "locale");
}

static void
parse_xml_profile (xmlNode *parent, const char *profile, LudwigInstrument *instrument)
{
        xmlNode *node;
	xmlChar *clef;
	xmlChar *default_clef;
	xmlChar *pitch_floor;
	xmlChar *pitch_ceiling;
        gboolean is_default = FALSE;

        for (node = ludwig_xml_get_children (parent); node != NULL; node = node->next)
        {
                if (strcmp (node->name, "clef") == 0)
                {
                        clef = xmlNodeGetContent (node);
                        default_clef = xmlGetProp (node, "default");

                        if (default_clef != NULL)
                        {
                                if (strcmp (default_clef, "true") == 0)
                                {
                                        is_default = TRUE;
                                }
                                else
                                {
                                        is_default = FALSE;
                                }
                        }
                        else
                        {
                                is_default = FALSE;
                        }

                        if (strcmp (clef, "treble") == 0)
                        {

                                ludwig_instrument_add_valid_clef (instrument, profile, CLEF_TREBLE);
                                if (is_default)
                                {
                                        ludwig_instrument_set_default_clef (instrument, profile, CLEF_TREBLE);
                                }
                        }
                        else if (strcmp (clef, "alto") == 0)
                        {
                                ludwig_instrument_add_valid_clef (instrument, profile, CLEF_ALTO);
                                if (is_default)
                                {
                                        ludwig_instrument_set_default_clef (instrument, profile, CLEF_ALTO);
                                }
                        }
                        else if (strcmp (clef, "tenor") == 0)
                        {
                                ludwig_instrument_add_valid_clef (instrument, profile, CLEF_TENOR);
                                if (is_default)
                                {
                                        ludwig_instrument_set_default_clef (instrument, profile, CLEF_TENOR);
                                }
                        }
                        else if (strcmp (clef, "bass") == 0)
                        {
                                ludwig_instrument_add_valid_clef (instrument, profile, CLEF_BASS);
                                if (is_default)
                                {
                                        ludwig_instrument_set_default_clef (instrument, profile, CLEF_BASS);
                                }
                        }
                }
                else if (strcmp (node->name, "pitch-floor") == 0)
                {
                        pitch_floor = xmlNodeGetContent (node);
                }
                else if (strcmp (node->name, "pitch-ceiling") == 0)
                {
                        pitch_ceiling = xmlNodeGetContent (node);
                }
                else
                {
                        g_warning ("LudwigInstrumentList: Invalid XML tag <%s>.\n", node->name);
                }
        }
}
        
static void
parse_xml_instrument (xmlNode *parent, LudwigInstrument *instrument)
{
        xmlNode *node;
        xmlChar *id;
        xmlChar *profile;
        xmlChar *locale;
        xmlChar *name;
	gboolean is_default = FALSE;

	id = xmlGetProp (parent, "id");

        for (node = ludwig_xml_get_children (parent); node != NULL; node = node->next)
        {
                if (strcmp (node->name, "translation") == 0)
                {
			locale = xmlGetProp (node, "locale");
			name = xmlNodeGetContent (node);
                }
                else if (strcmp (node->name, "profile") == 0)
                {
                        profile = xmlGetProp (node, "id");
                        ludwig_instrument_add_profile (instrument, profile);
                        parse_xml_profile (node, profile, instrument);
                        xmlFree (profile);
                }
        }
}

static void
parse_xml_group (xmlNode *parent, const xmlChar *group_name)
{
        LudwigInstrument *instrument;
        xmlNode *node;
        xmlChar *id;
        xmlChar *pos;

        for (node = ludwig_xml_get_children (parent); node != NULL; node = node->next)
        {
                if (strcmp (node->name, "instrument") == 0)
                {
                        id = xmlGetProp (node, "id");
                        pos = xmlGetProp (node, "pos");
                        if (!pos)
                        {
                                g_warning ("Instrument %s parsed with no \"pos\" attribute!\n", id, pos);
                        }

                        if (id != NULL)
                        {
                                instrument = ludwig_instrument_new (id, atoi (pos), group_name);
                                ludwig_instrument_list_add_instrument (id, group_name, instrument);
                                parse_xml_instrument (node, instrument);
                                xmlFree (id);
                        }
                }
                else
                {
                        g_warning ("LudwigInstrumentList: Juse parsed tag <%s>; only <instrument> is valid here.\n");
                }
        }
}

static void
parse_xml_instrument_list (xmlNode *parent)
{
        xmlNode *node;
	xmlChar *id;
        xmlChar *pos;

        for (node = ludwig_xml_get_children (parent); node != NULL; node = node->next)
        {
                if (strcmp (node->name, "group") == 0)
                {
			id = xmlGetProp (node, "id");
                        pos = xmlGetProp (node, "pos");
                        if (!pos)
                        {
                                g_warning ("group %s parsed with no \"pos\" attribute!\n", id);
                        }

			if (id != NULL)
			{
				ludwig_instrument_list_add_group (id, atoi (pos));
	                        parse_xml_group (node, id);
				xmlFree (id);
			}
                }
                else
                {
                        g_warning ("LudwigInstrumentList: Just parsed tag <%s> from XML file.\n", node->name);
                        g_warning ("LudwigInstrumentList: Only <group> tag may be under <instrument-list>\n");
                }
        }
}

static void
load_instruments (void)
{
	xmlNode *node;
	xmlDoc  *doc;
	FILE *file;

	file = fopen ("instrument-list.xml", "r");
	if (file != NULL)
	{
		int res, size = 1024;
		char chars[1024];
		xmlParserCtxt *ctxt;

		res = fread (chars, 1, 4, file);

		if (res > 0)
		{
			ctxt = xmlCreatePushParserCtxt (NULL, NULL, chars, res, "instrument-list.xml");

			while ((res = fread (chars, 1, size, file)) > 0)
			{
				xmlParseChunk (ctxt, chars, res, 0);
			}

			xmlParseChunk (ctxt, chars, 0, 1);
			doc = ctxt->myDoc;
			xmlFreeParserCtxt (ctxt);
		}
	}

	node = xmlDocGetRootElement (doc);
	if (strcmp (node->name, "instrument-list") != 0)
	{
		g_warning ("LudwigInstrumentList: XML toplevel tag is <%s>!\n", node->name);
		g_warning ("Only <instrument-list> is allowed here!\n");
	}
	else
	{
		parse_xml_instrument_list (node);
	}
}

GtkType
ludwig_instrument_list_get_type (void)
{
	static GtkType type = 0;

	if (!type)
	{
		GtkTypeInfo info =
		{
			"LudwigInstrumentList",
			sizeof (LudwigInstrumentList),
			sizeof (LudwigInstrumentListClass),
			(GtkClassInitFunc)  ludwig_instrument_list_class_init,
			(GtkObjectInitFunc) ludwig_instrument_list_init,
			NULL, /* Reserved 1 */
			NULL, /* Reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gtk_object_get_type (), &info);
	}

	return type;
}

void
ludwig_instrument_list_create (void)
{
	if (singleton == NULL)
	{
		singleton = gtk_type_new (ludwig_instrument_list_get_type ());

		singleton->gconf_client = gconf_client_get_default ();
		gconf_client_add_dir (singleton->gconf_client, "/apps/ludwig",
                                      GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);

		singleton->groups = g_hash_table_new (ludwig_strcase_hash,
                                                      ludwig_strcase_equal);
                singleton->records = g_hash_table_new (ludwig_strcase_hash,
                                                       ludwig_strcase_equal);
	}

	load_instruments ();
}

void
ludwig_instrument_list_add_group (const gchar *name, int pos)
{
        LudwigInstrumentGroup *group;

        group = g_new0 (LudwigInstrumentGroup, 1);

        group->records = g_hash_table_new (ludwig_strcase_hash,
                                           ludwig_strcase_equal);
        group->pos = pos;

        g_hash_table_insert (singleton->groups, g_strdup (name), group);
}

LudwigInstrument *
ludwig_instrument_list_query_instrument (const char *id)
{
        LudwigInstrument *instrument;

        instrument = g_hash_table_lookup (singleton->records, id);

        return instrument;
}

void
ludwig_instrument_list_add_instrument (const char *name, const gchar *group_name, LudwigInstrument *instrument)
{
        GHashTable *table;
        LudwigInstrumentGroup *group;

        group = g_hash_table_lookup (singleton->groups, group_name);

        g_hash_table_insert (singleton->records, g_strdup (name), instrument);
        g_hash_table_insert (group->records, g_strdup (name), instrument);
}

static void
group_hash_foreach (gpointer key, gpointer value, gpointer user_data)
{
        LILGroupFunc func = user_data;
        LudwigInstrumentGroup *group = value;

        (* func) (key, group->pos);
};

void
ludwig_instrument_list_foreach_group (LILGroupFunc func)
{
        g_hash_table_foreach (singleton->groups, &group_hash_foreach, func);
}

static void
instrument_hash_foreach (gpointer key, gpointer value, gpointer user_data)
{
        LILInstrumentFunc func;
        LudwigInstrument *instrument;

        func = user_data;
        instrument = value;

        (* func) (instrument->group, key);
}

void
ludwig_instrument_list_foreach (LILInstrumentFunc func)
{
	g_hash_table_foreach (singleton->records, &instrument_hash_foreach, func);
}

static void
instrument_in_group_hash_foreach (gpointer key, gpointer value, gpointer user_data)
{
        LILInstrumentCtxt *ctxt;
        LudwigInstrument *instrument;
        LudwigClef clef;

        ctxt = user_data;
        instrument = value;
        clef = ludwig_instrument_query_default_clef (instrument);

        (* ctxt->func) (key, clef, instrument->pos, ctxt->data);
}

void
ludwig_instrument_list_foreach_instrument_in_group (const char *group_name,
                                                    LILInstrumentGroupFunc func,
                                                    gpointer user_data)
{
        LILInstrumentCtxt *ctxt;
        LudwigInstrumentGroup *group;

        group = g_hash_table_lookup (singleton->groups, group_name);

        ctxt = g_new0 (LILInstrumentCtxt, 1);

        ctxt->func = func;
        ctxt->data = user_data;

        g_hash_table_foreach (group->records, instrument_in_group_hash_foreach, ctxt);

        g_free (ctxt);
}
