/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include "ludwig-tool-manager.h"
#include "ludwig-util.h"

static GtkObjectClass *parent_class = NULL;
static LudwigToolManager *singleton = NULL;

/* Destroy one tool */
static void
ludwig_tool_manager_destroy_tool (gpointer key,
                                  gpointer value,
                                  gpointer data)
{
	LudwigTool *tool = (LudwigTool *) value;

	gtk_object_destroy (GTK_OBJECT (tool));
}

static void
ludwig_tool_manager_destroy (GtkObject *object)
{
	LudwigToolManager *manager;

	manager = LUDWIG_TOOL_MANAGER (object);

	/* Destroy the tools, then destroy the hash table */
	g_hash_table_foreach (manager->tools,
                              &ludwig_tool_manager_destroy_tool,
                              NULL);

	g_hash_table_destroy (manager->tools);
}

static void
ludwig_tool_manager_set_arg (GtkObject *object,
                             GtkArg    *arg,
                             guint      arg_id)
{
}

static void
ludwig_tool_manager_get_arg (GtkObject *object,
                             GtkArg    *arg,
                             guint      arg_id)
{
}

static void
ludwig_tool_manager_class_init (LudwigToolManagerClass *class)
{
        GtkObjectClass *object_class;

        object_class = (GtkObjectClass *) class;

        parent_class = gtk_type_class (gtk_object_get_type ());

        object_class->destroy = ludwig_tool_manager_destroy;
        object_class->set_arg = ludwig_tool_manager_set_arg;
        object_class->get_arg = ludwig_tool_manager_get_arg;
}

static void
ludwig_tool_manager_init (LudwigToolManager *tm)
{
        tm->tools = NULL;
        tm->tool_stack = NULL;
        tm->current_tool = NULL;
}

GtkType
ludwig_tool_manager_get_type (void)
{
        static GtkType type = 0;

        if (!type)
        {
                const GtkTypeInfo info =
                {
                        "LudwigToolManager",
                        sizeof (LudwigToolManager),
                        sizeof (LudwigToolManagerClass),
                        (GtkClassInitFunc)  ludwig_tool_manager_class_init,
                        (GtkObjectInitFunc) ludwig_tool_manager_init,
                        NULL, /* Reserved 1 */
                        NULL, /* Reserved 2 */
                        (GtkClassInitFunc) NULL
                };

                type = gtk_type_unique (gtk_object_get_type (), &info);
        }

        return type;
}

void
ludwig_tool_manager_create (void)
{
        if (singleton == NULL)
        {
                singleton = gtk_type_new (ludwig_tool_manager_get_type ());
        }

	/* Create our hash table to store tools in */
	singleton->tools = g_hash_table_new (ludwig_strcase_hash, ludwig_strcase_equal);
}

void
ludwig_tool_manager_shutdown (void)
{
        gtk_object_destroy (GTK_OBJECT (singleton));
}

void
ludwig_tool_manager_select_tool (LudwigTool *tool)
{
}

void
ludwig_tool_manager_push_tool (LudwigTool *tool)
{
        singleton->tool_stack = g_slist_prepend (singleton->tool_stack, tool);
}

void
ludwig_tool_manager_pop_tool (void)
{
        GSList *tmp;

        if (singleton->tool_stack)
        {
                tmp = singleton->tool_stack;
                singleton->tool_stack = singleton->tool_stack->next;
                g_slist_free_1 (tmp);
        }
}

/* Retrieve the current tool */
LudwigTool *
ludwig_tool_manager_get_tool (void)
{
        return singleton->current_tool;
}

/* Retrieve a tool by its name */
LudwigTool *
ludwig_tool_manager_get_tool_by_name (const char *name)
{
	return g_hash_table_lookup (singleton->tools, name);
}

int
ludwig_tool_manager_motion (GtkWidget *widget,
                            GdkEventMotion *evt,
                            gpointer data)
{
	if (!singleton->current_tool)
		return FALSE;

	return ludwig_tool_motion (singleton->current_tool,
                                   widget,
                                   evt,
                                   data);
}

