/*
 * Copyright (C) 2000 by Cody Russell
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <gtk/gtkobject.h>
#include "ludwig-string.h"

enum
{
	TEXT_CHANGED,
	FONT_CHANGED,
	LAST_SIGNAL
};

static GtkObjectClass *parent_class = NULL;
static guint signals[LAST_SIGNAL];

static void
ludwig_string_class_init (GtkObjectClass *object_class)
{
	parent_class = gtk_type_class (gtk_object_get_type ());

	signals[TEXT_CHANGED] =
		gtk_signal_new ("text_changed",
                                GTK_RUN_FIRST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (LudwigStringClass,
                                                   text_changed),
                                gtk_marshal_NONE__NONE,
                                GTK_TYPE_NONE, 0);
	signals[FONT_CHANGED] =
		gtk_signal_new ("font_changed",
                                GTK_RUN_FIRST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (LudwigStringClass,
                                                   font_changed),
                                gtk_marshal_NONE__NONE,
                                GTK_TYPE_NONE, 0);

	gtk_object_class_add_signals(object_class,
                                     signals,
                                     LAST_SIGNAL);
}

static void
ludwig_string_init (LudwigString *string)
{
	string->adj = NULL;
}

/* Public interfaces */
GtkType
ludwig_string_get_type (void)
{
	static GtkType string_type = 0;

	if (!string_type)
	{
		const GtkTypeInfo string_info =
		{
			"LudwigString",
			sizeof(LudwigString),
			sizeof(LudwigStringClass),
			(GtkClassInitFunc)  ludwig_string_class_init,
			(GtkObjectInitFunc) ludwig_string_init,
			NULL, /* Reserved 1 */
			NULL, /* Reserved 2 */
			(GtkClassInitFunc) NULL,
		};

		string_type = gtk_type_unique (gtk_object_get_type (),
                                               &string_info);
	}

	return string_type;
}

LudwigString *
ludwig_string_new (gchar *text)
{
	LudwigString *ret;

	ret = gtk_type_new (LUDWIG_TYPE_STRING);
	ret->text = g_new (gchar, sizeof (text));
	strcpy(ret->text, text);

	return ret;
}

gint
ludwig_string_request_font (LudwigString *string,
                            gchar *name, gint size)
{
	return 0;
}
