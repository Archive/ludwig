/*
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <bonobo.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define LUDWIG_TYPE_ZOOM_CONTROL        (ludwig_zoom_control_get_type ())
#define LUDWIG_ZOOM_CONTROL(o)          (GTK_CHECK_CAST ((o), LUDWIG_TYPE_ZOOM_CONTROL, LudwigZoomControl))
#define LUDWIG_ZOOM_CONTROL_CLASS(k)    (GTK_CHECK_CLASS_CAST ((k), LUDWIG_TYPE_ZOOM_CONTROL, LudwigZoomControlClass))
#define LUDWIG_IS_ZOOM_CONTROL(o)       (GTK_CHECK_TYPE ((o), LUDWIG_TYPE_ZOOM_CONTROL))
#define LUDWIG_IS_ZOOM_CONTROL_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), LUDWIG_TYPE_ZOOM_CONTROL))

struct _LudwigZoomControl
{
        BonoboControl  parent_object;

        GtkWidget     *zoom_box;
        GArray        *zoom_levels;
        GArray        *zoom_level_names;
};

struct _LudwigZoomControlClass
{
        BonoboControlClass parent_class;
};

GtkType            ludwig_zoom_control_get_type (void);
LudwigZoomControl *ludwig_zoom_control_new      (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
