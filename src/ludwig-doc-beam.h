/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#ifndef __LUDWIG_DOC_BEAM_H__
#define __LUDWIG_DOC_BEAM_H__

#include "ludwig-doc-metric.h"
#include "ludwig-types.h"

#define LUDWIG_TYPE_DOC_BEAM        (ludwig_doc_beam_get_type ())
#define LUDWIG_DOC_BEAM(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_DOC_BEAM, LudwigDocBeam))
#define LUDWIG_DOC_BEAM_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_DOC_BEAM, LudwigDocBeamClass))
#define LUDWIG_IS_DOC_BEAM(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_DOC_BEAM))
#define LUDWIG_IS_DOC_BEAM_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_DOC_BEAM))

struct _LudwigDocBeam
{
        LudwigDocMetric parent_object;

        /* When we port to GTK+ 2.0, either metric or container or both
         * should become GInterfaces, and less code will be duplicated in
         * here.
         */

        GList *items;  /* List of LudwigDocMetric objects */
};

struct _LudwigDocBeamClass
{
        LudwigDocMetricClass parent_class;
};

GtkType   ludwig_doc_beam_get_type   (void);
void      ludwig_doc_beam_add_metric (LudwigDocBeam *beam, LudwigDocMetric *metric);

#endif /* __LUDWIG_DOC_BEAM_H__ */
