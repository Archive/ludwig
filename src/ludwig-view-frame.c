/*
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include "ludwig-view-frame.h"
#include "ludwig-view.h"

static GtkScrolledWindowClass *parent_class = NULL;
static gint                    total_views;

static void
ludwig_view_frame_destroy (GtkObject *object)
{
	LudwigViewFrame *view;

	view = LUDWIG_VIEW_FRAME (object);

	g_free (view->name);

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
ludwig_view_frame_class_init (LudwigViewFrameClass *class)
{
        GtkObjectClass *object_class;

        object_class = GTK_OBJECT_CLASS (class);
        object_class->destroy = ludwig_view_frame_destroy;

	parent_class = gtk_type_class (gtk_scrolled_window_get_type ());
}

static void
ludwig_view_frame_init (LudwigViewFrame *view)
{
	view->name = NULL;
}

GtkType
ludwig_view_frame_get_type (void)
{
	static GtkType view_type = 0;

	if (!view_type)
	{
		const GtkTypeInfo view_info =
		{
			"LudwigViewFrame",
			sizeof (LudwigViewFrame),
			sizeof (LudwigViewFrameClass),
			(GtkClassInitFunc)  ludwig_view_frame_class_init,
			(GtkObjectInitFunc) ludwig_view_frame_init,
			NULL,  /* Reserved 1 */
			NULL,  /* Reserved 2 */
			(GtkClassInitFunc) NULL,
		};

		view_type = gtk_type_unique (gtk_scrolled_window_get_type(), &view_info);
	}

	return view_type;
}

static void
ruler_popup (GtkWidget *widget, GdkEventButton *evt, gpointer data)
{
        ludwig_app_create_ruler_popup ();
}

#define EVENT_METHOD(i, x) GTK_WIDGET_CLASS (GTK_OBJECT(i)->klass)->x

static void
layout_hadjustment_changed (GtkAdjustment *adj, LudwigView *view)
{
        GnomeCanvas *canvas;
        GtkLayout *layout;
        float lower, upper, max;

        canvas = GNOME_CANVAS (view->canvas);
        layout = GTK_LAYOUT (canvas);

        lower = layout->hadjustment->value;
        upper = layout->hadjustment->page_size + lower;

        gtk_ruler_set_range (GTK_RULER (view->view_frame->hruler),
                             LUDWIG_PT_TO_METRIC (lower / canvas->pixels_per_unit, view->units),
                             LUDWIG_PT_TO_METRIC (upper / canvas->pixels_per_unit, view->units),
                             5.0,  /* What the hell is this? */
                             LUDWIG_PT_TO_METRIC (layout->width * canvas->pixels_per_unit, view->units));

        gtk_signal_emit_stop_by_name (GTK_OBJECT (adj), "value_changed");
}

static void
layout_vadjustment_changed (GtkAdjustment *adj, LudwigView *view)
{
        GnomeCanvas *canvas;
        GtkLayout *layout;
        float upper, lower;

        canvas = GNOME_CANVAS (view->canvas);
        layout = GTK_LAYOUT (canvas);

        lower = layout->vadjustment->value;
        upper = layout->vadjustment->page_size + lower;

        gtk_ruler_set_range (GTK_RULER (view->view_frame->vruler),
                             LUDWIG_PT_TO_METRIC (lower / canvas->pixels_per_unit, view->units),
                             LUDWIG_PT_TO_METRIC (upper / canvas->pixels_per_unit, view->units),
                             5.0,  /* What the fuck is this?? */
                             LUDWIG_PT_TO_METRIC (layout->height * canvas->pixels_per_unit, view->units));

        gtk_signal_emit_stop_by_name (GTK_OBJECT (adj), "value_changed");
}

LudwigViewFrame *
ludwig_view_frame_new (const gchar *name, BonoboUIContainer *container, LudwigView *view)
{
	LudwigViewFrame *ret;
        GtkObject *hadj;
        GtkObject *vadj;

	ret = gtk_type_new (LUDWIG_TYPE_VIEW_FRAME);

	if (name == NULL)
	{
		char buf[12];
		sprintf (buf, "Untitled%d", total_views);
		ret->name = g_new (gchar, sizeof(buf));
		strcpy (ret->name, buf);
	}
	else
	{
		strcpy (ret->name, name);
	}

        /* Setup the scrolled window. */

        /* PARENTAL ADVISORY:  EXPLICIT HACK
         *   Parents are strongly advised to keep small children away from the following
         *   code.  It has received a rating of "HHH" for strong hackish content.
         *
         * GnomeCanvas scrolling is dependent upon the canvas being the child of the
         * GtkScrolledWindow widget.  We need to put the canvas inside a GtkTable, and
         * put that inside the scrolled window using a viewport, so we set the scrolled
         * window's adjustments to be the canvas' layout adjustments.  To keep it from
         * scrolling at double-speed (and resulting in black area) we have to kill the
         * emitted signal from a "value_changed" callback.
         */
        gtk_scrolled_window_set_hadjustment (GTK_SCROLLED_WINDOW (ret), GTK_LAYOUT (view->canvas)->hadjustment);
        gtk_scrolled_window_set_vadjustment (GTK_SCROLLED_WINDOW (ret), GTK_LAYOUT (view->canvas)->vadjustment);

        gtk_signal_connect (GTK_OBJECT (GTK_LAYOUT (view->canvas)->vadjustment), "value_changed",
                            GTK_SIGNAL_FUNC (layout_vadjustment_changed), view);
        gtk_signal_connect (GTK_OBJECT (GTK_LAYOUT (view->canvas)->hadjustment), "value_changed",
                            GTK_SIGNAL_FUNC (layout_hadjustment_changed), view);

        /* Create the table */
        ret->table = gtk_table_new (2, 2, FALSE);

        gtk_table_attach (GTK_TABLE (ret->table), view->canvas, 1, 2, 1, 2,
                          GTK_EXPAND | GTK_FILL,
                          GTK_EXPAND | GTK_FILL, 0, 0);

        gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (ret), ret->table);
        gtk_widget_show (ret->table);

        /* Create horizontal ruler */
        ret->hruler = gtk_hruler_new ();
        gtk_ruler_set_metric (GTK_RULER (ret->hruler), GTK_PIXELS);
        gtk_ruler_set_range (GTK_RULER (ret->hruler), 1.0, 1100.0, 0, 1100.0);
        gtk_signal_connect_object (GTK_OBJECT (view->canvas), "motion_notify_event",
                                   (GtkSignalFunc) EVENT_METHOD (ret->hruler,
                                                                 motion_notify_event),
                                   GTK_OBJECT (ret->hruler));
        gtk_widget_set_events (ret->hruler, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);
        gtk_signal_connect (GTK_OBJECT (ret->hruler), "button_press_event",
                            GTK_SIGNAL_FUNC (ruler_popup), NULL);
        gtk_table_attach (GTK_TABLE (ret->table), ret->hruler, 1, 2, 0, 1,
                          GTK_EXPAND | GTK_FILL,
                          GTK_FILL,
                          0, 0);
        gtk_widget_show (ret->hruler);

        /* Create vertical ruler */
        ret->vruler = gtk_vruler_new ();
        gtk_ruler_set_metric (GTK_RULER (ret->vruler), GTK_PIXELS);
        gtk_ruler_set_range (GTK_RULER (ret->vruler), 1.0, 700.0, 0, 700.0);
        gtk_signal_connect_object (GTK_OBJECT (view->canvas), "motion_notify_event",
                                   (GtkSignalFunc) EVENT_METHOD (ret->vruler,
                                                                 motion_notify_event),
                                   GTK_OBJECT (ret->vruler));
        gtk_widget_set_events (ret->vruler, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);
        gtk_signal_connect (GTK_OBJECT (ret->vruler), "button_press_event",
                            GTK_SIGNAL_FUNC (ruler_popup), NULL);
        gtk_table_attach (GTK_TABLE (ret->table), ret->vruler, 0, 1, 1, 2,
                          GTK_FILL,
                          GTK_EXPAND | GTK_FILL,
                          0, 0);
        gtk_widget_show (ret->vruler);

        ret->uic = container;

        ret->control = view->control;

	total_views++;

	return ret;
}
