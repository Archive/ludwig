/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#ifndef __LUDWIG_DOC_METRIC_H__
#define __LUDWIG_DOC_METRIC_H__

#include "ludwig-doc-item.h"
#include "ludwig-types.h"

enum
{
	METRIC_DOUBLE       = 1 << 0,
	METRIC_WHOLE        = 1 << 1,
	METRIC_HALF         = 1 << 2,
	METRIC_QUARTER      = 1 << 3,
	METRIC_EIGHTH       = 1 << 4,
	METRIC_SIXTEENTH    = 1 << 5,
	METRIC_THIRTYSECOND = 1 << 6,
	METRIC_SIXTYFOURTH  = 1 << 7,
};

struct _LudwigDocMetric
{
	LudwigDocItem parent_object;

        /* Our owners. All metrics have a measure,
         * only some have a beam owner.
         */
        LudwigDocMeasure *measure;
        LudwigDocBeam *beam;

	int value;
	int divisor;    /* e.g., triplet == 3 */
	int dots;

        int refcount;   /* A metric may be contained in a measure
                         * and also in a beam at the same time.
                         */

	gboolean is_double;
};

struct _LudwigDocMetricClass
{
	LudwigDocItemClass parent_class;
};

GtkType  ludwig_doc_metric_get_type (void);

#endif /* __LUDWIG_DOC_METRIC__ */
