/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include "ludwig-view-item.h"

#ifndef __LUDWIG_VIEW_METRIC_H__
#define __LUDWIG_VIEW_METRIC_H__

#define LUDWIG_TYPE_VIEW_METRIC        (ludwig_view_metric_get_type())
#define LUDWIG_VIEW_METRIC(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_VIEW_METRIC, LudwigViewMetric))
#define LUDWIG_VIEW_METRIC_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_VIEW_METRIC, LudwigViewMetricClass))
#define LUDWIG_IS_VIEW_METRIC(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_VIEW_METRIC))
#define LUDWIG_IS_VIEW_METRIC_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_VIEW_METRIC))

typedef struct _LudwigViewMetric        LudwigViewMetric;
typedef struct _LudwigViewMetricClass   LudwigViewMetricClass;
typedef struct _LudwigViewMetricPrivate LudwigViewMetricPrivate;

struct _LudwigViewMetric
{
	LudwigViewItem           parent_object;
        LudwigViewMetricPrivate *priv;

        int                      code;      /* LudwigFontFace lookup code */

        int                      value;
        gboolean                 is_double;
        gboolean                 is_dotted;

        GList                    *items;
};

struct _LudwigViewMetricClass
{
	LudwigViewItemClass parent_class;
};

GtkType      ludwig_view_metric_get_type    (void);

#endif /* __LUDWIG_VIEW_METRIC_H__ */
