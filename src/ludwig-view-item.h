/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

/* LudwigViewItem - Any item that is rendered on the score.
 *                  We're forced to derive from GnomeCanvasGroup due
 *                  to limitations in GTK's type system.  A LudwigViewItem
 *                  should not necessarily be a group; a LudwigViewContainer
 *                  is.  So, we have a couple wasted pointers for everything
 *                  that is not a container.
 */

#include <libgnomeui/gnome-canvas.h>

#ifndef __LUDWIG_VIEW_ITEM_H__
#define __LUDWIG_VIEW_ITEM_H__

#define LUDWIG_TYPE_VIEW_ITEM        (ludwig_view_item_get_type())
#define LUDWIG_VIEW_ITEM(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_VIEW_ITEM, LudwigViewItem))
#define LUDWIG_VIEW_ITEM_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_VIEW_ITEM, LudwigViewItemClass))
#define LUDWIG_IS_VIEW_ITEM(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_VIEW_ITEM))
#define LUDWIG_IS_VIEW_ITEM_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_VIEW_ITEM))

typedef struct _LudwigViewItem      LudwigViewItem;
typedef struct _LudwigViewItemClass LudwigViewItemClass;

struct _LudwigViewItem
{
	GnomeCanvasGroup parent_object;

        LudwigViewItem *container;

        LudwigViewItem *next;
        LudwigViewItem *prev;

        double x;
        double y;
};

struct _LudwigViewItemClass
{
	GnomeCanvasGroupClass parent_class;
};

GtkType            ludwig_view_item_get_type    (void);
GnomeCanvasItem   *ludwig_view_item_new         (void);

#endif /* __LUDWIG_VIEW_ITEM_H__ */
