/*
 * Copyright (C) 2000 by Cody Russell
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_STRING_H__
#define __LUDWIG_STRING_H__

#include <gtk/gtkobject.h>
#include <ludwig-font/ludwig-font.h>

#define LUDWIG_TYPE_STRING        (ludwig_string_get_type())
#define LUDWIG_STRING(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_STRING, LudwigString))
#define LUDWIG_STRING_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_STRING, LudwigStringClass))
#define LUDWIG_IS_STRING(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_STRING))
#define LUDWIG_IS_STRING_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_STRING))

typedef struct _LudwigString      LudwigString;
typedef struct _LudwigStringClass LudwigStringClass;

struct _LudwigString
{
	GtkObject      parent_object;

	gchar         *text;
	LudwigFont    *font;
	GtkAdjustment *adj;
};

struct _LudwigStringClass
{
	GtkObjectClass parent_class;

	void (* text_changed) (LudwigString *string);
	void (* font_changed) (LudwigString *string);
};

/* Public interfaces */
GtkType       ludwig_string_get_type       (void);
LudwigString *ludwig_string_new            (gchar *title);
gint          ludwig_string_request_font   (LudwigString *string,
                                            gchar *name, gint size);

#endif /* __LUDWIG_STRING_H__ */
