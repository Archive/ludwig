/*
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>

#include <ludwig-font/ludwig-font.h>
#include <ludwig-font/ludwig-font-glyph.h>
#include <ludwig-font/ludwig-rfont.h>

#include "ludwig-types.h"
#include "ludwig-app.h"
#include "ludwig-view.h"
#include "ludwig-zoom-control.h"
#include "ludwig-display-metrics.h"
#include "ludwig-tool.h"
#include "ludwig-util.h"

#include "canvas-items/gp-path.h"
#include "canvas-items/gnome-canvas-ttext.h"

static GtkObjectClass *parent_class;
static int             running_objects;

#define STAFF_SPACING   75.0

enum
{
        PROP_ZOOM,
        /* TODO: Implement margins and page sizes */
        PROP_LEFT_MARGIN,
        PROP_RIGHT_MARGIN,
        PROP_TOP_MARGIN,
        PROP_BOTTOM_MARGIN,
        PROP_PAGE_HSIZE,
        PROP_PAGE_VSIZE
};

typedef struct _InstrumentYSort InstrumentYSort;

struct _InstrumentYSort
{
	double y;
	GnomeCanvasItem *part;
};

static void
ludwig_view_destroy (GtkObject *object)
{
        LudwigView *view = (LudwigView *) object;

        gtk_widget_destroy (view->canvas);
        bonobo_object_unref (BONOBO_OBJECT (view->control));
}

static void
ludwig_view_class_init (LudwigViewClass *class)
{
        GtkObjectClass *object_class;

        object_class = (GtkObjectClass *) class;
        object_class->destroy = ludwig_view_destroy;

        parent_class = gtk_type_class (gtk_object_get_type ());
}

static void
ludwig_view_init (LudwigView *view)
{
        view->canvas = NULL;
        view->control = NULL;
        view->root = NULL;
        view->page = NULL;
        view->page_group = NULL;
        view->page_hsize = 0.0;
        view->page_vsize = 0.0;
        view->left_margin = 0.0;
        view->right_margin = 0.0;
        view->top_margin = 0.0;
        view->bottom_margin = 0.0;
        view->units = METRIC_PT;

        view->parts = g_hash_table_new (ludwig_strcase_hash, ludwig_strcase_equal);
	view->y_array = g_array_new (FALSE, TRUE, sizeof (double));
}

static void
control_activate_cb (BonoboControl *control, gboolean activate, gpointer data)
{
        bonobo_control_activate_notify (control, activate);
}

static void
zoomable_zoom_in_cb (BonoboZoomable *zoomable, LudwigView *view)
{
}

static void
zoomable_zoom_out_cb (BonoboZoomable *zoomable, LudwigView *view)
{
}

static void
zoomable_zoom_to_default_cb (BonoboZoomable *zoomable, LudwigView *view)
{
}

static void
ludwig_view_set_property (BonoboPropertyBag *pb,
                          const BonoboArg   *arg,
                          guint              arg_id,
                          CORBA_Environment *ev,
                          gpointer           user_data)
{
        LudwigView *view;
        GtkObject *canvas;
        float zoom;

        canvas = (GtkObject *) user_data;

        switch (arg_id)
        {
        case PROP_LEFT_MARGIN:
                break;

        case PROP_RIGHT_MARGIN:
                break;

        case PROP_TOP_MARGIN:
                break;

        case PROP_BOTTOM_MARGIN:
                break;

        case PROP_PAGE_HSIZE:
                break;

        case PROP_PAGE_VSIZE:
                break;

        case PROP_ZOOM:
                view = (LudwigView *) gtk_object_get_data (canvas, "view");
                zoom = view->zoom_level;
                BONOBO_ARG_SET_DOUBLE (arg, zoom);
                break;

        default:
                g_warning ("LudwigView.PropertyBag->set_prop(): Unhandled arg %d\n", arg_id);
                break;
        }
}

static void
persist_stream_load (BonoboPersistStream *ps, Bonobo_Stream stream,
                     Bonobo_Persist_ContentType type, void *data,
                     CORBA_Environment *ev)
{
}

static void
persist_stream_save (BonoboPersistStream *ps, Bonobo_Stream stream,
                     Bonobo_Persist_ContentType type, void *data,
                     CORBA_Environment *ev)
{
}

static void
ludwig_view_get_property (BonoboPropertyBag *pb,
                          BonoboArg         *arg,
                          guint              arg_id,
                          CORBA_Environment *ev,
                          gpointer           user_data)
{
        LudwigView *view;
        GtkObject *canvas;
        double zoom;

        canvas = (GtkObject *) user_data;

        switch (arg_id)
        {
        case PROP_ZOOM:
                zoom = BONOBO_ARG_GET_DOUBLE (arg);
                view = gtk_object_get_data (GTK_OBJECT (canvas), "view");

                if (view->zoom_level != zoom)
                {
                        view->zoom_level = zoom;
                        bonobo_zoomable_report_zoom_level_changed (BONOBO_ZOOMABLE (view->zoomable), zoom);
                }

                break;

        default:
                g_warning ("LudwigView.PropertyBag->get_prop(): Unhandled arg %d\n", arg_id);
        }
}

static void
zoom_out_callback (BonoboZoomable *zoomable, LudwigView *view)
{
}

static void
rezoom_control (LudwigView *view, float new_zoom)
{
        gnome_canvas_set_pixels_per_unit (GNOME_CANVAS (view->canvas), new_zoom);
}

static void
zoomable_set_zoom_level_cb (BonoboZoomable *zoomable,
                            float new_zoom,
                            LudwigView *view)
{
        rezoom_control (view, new_zoom);
        view->zoom_level = new_zoom;
        bonobo_zoomable_report_zoom_level_changed (view->zoomable, new_zoom);
}

GtkType
ludwig_view_get_type (void)
{
        static GtkType view_type = 0;

        if (!view_type)
        {
                const GtkTypeInfo view_info =
                {
                        "LudwigView",
                        sizeof (LudwigView),
                        sizeof (LudwigViewClass),
                        (GtkClassInitFunc)  ludwig_view_class_init,
                        (GtkObjectInitFunc) ludwig_view_init,
                        NULL, /* Reserved 1 */
                        NULL, /* Reserved 2 */
                        (GtkClassInitFunc) NULL,
                };

                view_type = gtk_type_unique (gtk_object_get_type (), &view_info);
        }

        return view_type;
}

static gint
key_press_cb (GtkWidget *widget, GdkEventKey *evt, gpointer data)
{
        GnomeCanvas *canvas;
	int x, y;

        canvas = GNOME_CANVAS (widget);

	gnome_canvas_get_scroll_offsets (canvas, &x, &y);

	if (evt->keyval == GDK_Up)
		gnome_canvas_scroll_to (canvas, x, y - 20);
	else if (evt->keyval == GDK_Down)
		gnome_canvas_scroll_to (canvas, x, y + 20);
	else if (evt->keyval == GDK_Left)
		gnome_canvas_scroll_to (canvas, x - 10, y);
	else if (evt->keyval == GDK_Right)
		gnome_canvas_scroll_to (canvas, x + 10, y);

	return TRUE;
}

static int
motion_cb (GtkWidget *widget, GdkEventMotion *evt, gpointer data)
{
        GList *tmp;
        InstrumentYSort *ysort;
        InstrumentYSort *match;
        LudwigView *view;

        view = (LudwigView *) data;

        ysort = NULL;
        match = NULL;

        tmp = view->y_list;
        while (tmp != NULL)
        {
                ysort = tmp->data;
                if ((match == NULL) || ((evt->y > ysort->y) && ((evt->y - ysort->y) < (evt->y - match->y))))
                {
                        g_print ("Setting match.\n");
                        match = ysort;
                }

                tmp = g_list_next (tmp);
        }

        if (match != NULL)
        {
                g_print ("Match: y == %f\n", match->y);
        }

	return FALSE;
}

#if 0
static void
canvas_popup (LudwigApp *app)
{
        GtkWidget *menu;

        menu = gtk_menu_new ();

        bonobo_window_add_popup (BONOBO_WINDOW (app), GTK_MENU (menu), "/popups/Canvas");
        gtk_widget_show (menu);

        gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, 3, 0);
}
#endif

static gint
button_press_cb (GtkWidget *widget, GdkEvent *evt, gpointer data)
{
        LudwigView *view;
        LudwigTool *tool;
        LudwigViewPart *part;
        GList      *tmp;

        g_print ("Button press event\n");

        view = ludwig_app_get_current_view ();
        tool = ludwig_tool_manager_get_tool ();

        if (tool != NULL)
        {
                if (tool->target & TARGET_PART)
                {
                        /* iterate through parts, find nearest part */
                        tmp = view->parts;
                        while (tmp != NULL)
                        {
                                tmp = g_list_next (tmp);
                                part = tmp->data;
                                g_print ("part %s\n", part->name);
                        }
                }
        }

        if ((evt->type == GDK_BUTTON_PRESS) &&
            ((evt->button.button == 1) ||
             (evt->button.button == 3)))
        {
                switch (evt->button.button)
                {
                case 1:
                        g_print ("mouse button 1 (%f,%f)\n", evt->button.x, evt->button.y);
                        break;

                case 2:
                        g_print ("mouse button 2\n");
                        break;

                case 3:
                        ludwig_app_create_canvas_popup ();
                        g_print ("mouse button 3\n");
                        break;
                }

                return TRUE;
        }

        return FALSE;
}

static void
add_instrument (LudwigDocument *doc,
                const char *instrument,
                LudwigClef clef,
                LudwigView *view)
{
        GnomeCanvasItem *part;
        double x, y;
	InstrumentYSort *ysort;

        x = 15.0;
        y = view->n_parts * STAFF_SPACING + 25.0;

        part = gnome_canvas_item_new (GNOME_CANVAS_GROUP (view->page_group),
                                      ludwig_view_part_get_type (),
                                      "x", x,
                                      "y", y,
                                      "name", g_strdup (instrument),
                                      "view", view,
                                      NULL);

        ludwig_view_part_construct (LUDWIG_VIEW_PART (part), clef);

        g_hash_table_insert (view->parts, g_strdup (instrument), LUDWIG_VIEW_PART (part));
        view->n_parts++;

        g_print ("Adding y: %f\n", y);

	ysort = g_new0 (InstrumentYSort, 1);
	ysort->y = y;
	ysort->part = part;

	view->y_list = g_list_append (view->y_list, ysort);
	view->y_array = g_array_append_val (view->y_array, y);

        gtk_widget_queue_draw (view->canvas);
}

#if 0
static void
add_instrument (LudwigDocument *doc,
                const char *instrument,
                LudwigClef clef,
                LudwigView *view)
{
        LudwigViewStaff *staff;
        LudwigViewPart *part;
        GnomeCanvasItem *item;
        GnomeCanvasPoints *points;
        LudwigFont *font;
        LudwigFontGlyph *glyph;
        GPPath *path;
        int i;
        int clef_code;
        int code;
        double affine[6];

        staff = g_new0 (LudwigViewStaff, 1);

        staff->y = view->n_parts * STAFF_SPACING + 25.0;
        staff->x = 15.0;

        staff->group = gnome_canvas_item_new (GNOME_CANVAS_GROUP (view->page_group),
                                              gnome_canvas_group_get_type (),
                                              "x", staff->x,
                                              "y", staff->y,
                                              NULL);

        for (i = 0; i < 5; i++)
        {
                points = gnome_canvas_points_new (2);
                points->coords[0] = 0.0;
                points->coords[1] = 9.0 * i;
                points->coords[2] = 1100.0;
                points->coords[3] = 9.0 * i;

                item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (staff->group),
                                              gnome_canvas_line_get_type (),
                                              "width_units", 1.0,
                                              "fill_color", "black",
                                              "points", points,
                                              NULL);

                gnome_canvas_points_unref (points);

                art_affine_scale (affine, 1.0, 1.0);
                gnome_canvas_item_affine_absolute (item, affine);

                staff->lines = g_slist_prepend (staff->lines, item);
        }

        part = g_new0 (LudwigViewPart, 1);
        part->staff = staff;

        g_hash_table_insert (view->parts, g_strdup (instrument), part);
        view->n_parts++;

        /* Create the clef */
        switch (clef)
        {
        case CLEF_TREBLE:
                clef_code = 38;
                art_affine_scale (affine, 0.8, 0.8);
                affine[4] = 8.0;
                affine[5] = 27.0;
                affine[3] = 0.0 - affine[3];  /* Flip on x-axis */
                break;

        case CLEF_ALTO:
                clef_code = 66;
                art_affine_scale (affine, 0.76, 0.76);
                affine[4] = 4.0;
                affine[5] = 18.0;
                affine[3] = 0.0 - affine[3];
                break;

        case CLEF_TENOR:
                clef_code = 66;
                art_affine_scale (affine, 0.76, 0.76);
                affine[4] = 4.0;
                affine[5] = 9.0;
                affine[3] = 0.0 - affine[3];
                break;

        case CLEF_BASS:
                clef_code = 63;
                art_affine_scale (affine, 1.0, 1.0);
                affine[4] = 10.0;
                affine[5] = 11.0;
                affine[3] = 0.0 - affine[3];
                break;
        }

        font = (LudwigFont *) ludwig_get_standard_font ();
        code = ludwig_font_face_lookup (font->face, 0, clef_code);
        glyph = ludwig_vfont_get_glyph ((LudwigVFont *) font, code);
        path = gp_path_new_from_foreign_bpath (ludwig_font_glyph_get_bpath (glyph));

        item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (staff->group),
                                      gnome_canvas_bpath_get_type (),
                                      "bpath", path,
                                      "fill_color_rgba", 0x00ff,
                                      NULL);

        gnome_canvas_item_affine_absolute (item, affine);

        /* Workaround for ugly GnomeCanvas bug */
        gtk_widget_queue_draw (view->canvas);
}
#endif

LudwigView *
ludwig_view_new (LudwigDocument *doc,
                 BonoboUIContainer *container,
                 LudwigZoomControl *zoom_control,
                 const gchar *name)
{
        BonoboPropertyBag   *pb;
        BonoboPersistStream *ps;
        LudwigView          *view;

        view = gtk_type_new (LUDWIG_TYPE_VIEW);

        view->zoom_control = zoom_control;

        gtk_signal_connect (GTK_OBJECT (doc), "instrument_added",
                            GTK_SIGNAL_FUNC (add_instrument), view);

        /* Create GnomeCanvas */
        view->canvas = gnome_canvas_new_aa ();
        gtk_object_set_data (GTK_OBJECT (view->canvas), "view", view);
        gtk_widget_show (view->canvas);
        view->root = gnome_canvas_root (GNOME_CANVAS (view->canvas));

        gtk_signal_connect_after (GTK_OBJECT (view->canvas), "key_press_event",
                                  GTK_SIGNAL_FUNC (key_press_cb), view);
        gtk_signal_connect_after (GTK_OBJECT (view->canvas), "button_press_event",
                                  GTK_SIGNAL_FUNC (button_press_cb), NULL);
        gtk_signal_connect_after (GTK_OBJECT (view->canvas), "motion_notify_event",
                                  GTK_SIGNAL_FUNC (motion_cb), view);

        ludwig_view_set_page_size (view, 1200.0, 1200.0);

#if 0
        view->acetate = gnome_canvas_item_new (GNOME_CANVAS_GROUP (view->root),
                                               gnome_canvas_acetate_get_type (),
                                               NULL);
        gtk_signal_connect (GTK_OBJECT (view->acetate), "event",
                            GTK_SIGNAL_FUNC (acetate_event), view);
        gnome_canvas_item_raise_to_top (view->acetate);
#endif

        /* Create BonoboControl */
        view->control = bonobo_control_new (view->canvas);

        pb = bonobo_property_bag_new (ludwig_view_get_property,
                                      ludwig_view_set_property,
                                      view->canvas);
        bonobo_control_set_properties (view->control, pb);
        bonobo_object_unref (BONOBO_OBJECT (pb));

        bonobo_property_bag_add (pb, "zoom_level", 1, BONOBO_ARG_DOUBLE,
                                 NULL, "Zoom level", 0);

        bonobo_control_set_automerge (view->control, TRUE);

        view->zoomable = bonobo_zoomable_new ();
        gtk_signal_connect (GTK_OBJECT (view->zoomable), "set_zoom_level",
                            GTK_SIGNAL_FUNC (zoomable_set_zoom_level_cb), view);
        gtk_signal_connect (GTK_OBJECT (view->zoomable), "zoom_in",
                            GTK_SIGNAL_FUNC (zoomable_zoom_in_cb), view);
        gtk_signal_connect (GTK_OBJECT (view->zoomable), "zoom_out",
                            GTK_SIGNAL_FUNC (zoomable_zoom_out_cb), view);
        gtk_signal_connect (GTK_OBJECT (view->zoomable), "zoom_to_default",
                            GTK_SIGNAL_FUNC (zoomable_zoom_to_default_cb), view);

        view->zoom_level = 1.0;

        bonobo_zoomable_set_parameters_full (view->zoomable,
                                             view->zoom_level,
                                             zoom_control->zoom_levels->data[0],
                                             zoom_control->zoom_levels->data[zoom_control->zoom_levels->len],
                                             FALSE, FALSE, TRUE,
                                             (float *) zoom_control->zoom_levels->data,
                                             (const char **) zoom_control->zoom_level_names->data,
                                             zoom_control->zoom_levels->len);

        bonobo_object_add_interface (BONOBO_OBJECT (view->control),
                                     BONOBO_OBJECT (view->zoomable));

        gtk_signal_connect (GTK_OBJECT (view->control), "activate",
                            GTK_SIGNAL_FUNC (control_activate_cb), view);

        /*
         * Bonobo::PersistStream interface
         */
        ps = bonobo_persist_stream_new (persist_stream_load,
                                        persist_stream_save,
                                        NULL, NULL, view);
        bonobo_object_add_interface (BONOBO_OBJECT (view->control),
                                     BONOBO_OBJECT (ps));

        view->view_frame = ludwig_view_frame_new (name, container, view);
        gtk_widget_show (GTK_WIDGET (view->view_frame));

        running_objects++;

        return view;
}

void
ludwig_view_add_staff (LudwigView *view, gint n_measures)
{
#if 0
        LudwigViewStaff *staff;
        GnomeCanvasItem *line;
        GnomeCanvasPoints *points;
        int i;
        double y;
        double affine[6];

        staff = g_new0 (LudwigViewStaff, 1);

        staff->group = gnome_canvas_item_new (GNOME_CANVAS_GROUP (view->page_group),
                                              gnome_canvas_group_get_type (),
                                              "x", 15.0,
                                              "y", 25.0,
                                              NULL);

        for (i = 0; i < 5; i++)
        {
                points = gnome_canvas_points_new (2);
                points->coords[0] = 0.0;
                points->coords[1] = 9.0 * i;
                points->coords[2] = 1100.0;
                points->coords[3] = 9.0 * i;

                line = gnome_canvas_item_new (GNOME_CANVAS_GROUP (staff->group),
                                              gnome_canvas_line_get_type (),
                                              "width_units", 1.0,
                                              "fill_color", "black",
                                              "points", points,
                                              NULL);

                gnome_canvas_points_unref (points);

                art_affine_scale (affine, 1.0, 1.0);
                gnome_canvas_item_affine_absolute (line, affine);
        }

        /* Workaround for ugly GnomeCanvas bug. */
        gtk_widget_queue_draw (view->canvas);
#endif
}

static void
ludwig_view_update_scroll_region (LudwigView *view)
{
        if (!view->page_group)
        {
                view->page_group = gnome_canvas_item_new (view->root,
                                                          gnome_canvas_group_get_type (),
                                                          "x", 0.0,
                                                          "y", 0.0,
                                                          NULL);
        }

        if (!view->page)
        {
                view->page = gnome_canvas_item_new (GNOME_CANVAS_GROUP (view->page_group),
                                                    gnome_canvas_rect_get_type (),
                                                    "fill_color_rgba", 0xeeeeeeee,  /* off-whitish */
                                                    "outline_color", "black",
                                                    "width_units", 1.0,
                                                    "x1", 0.0,
                                                    "y1", 0.0,
                                                    "x2", 1200.0,
                                                    "y2", 1200.0,
                                                    NULL);
        }

        gnome_canvas_item_set (view->page_group,
                               "x", view->left_margin,
                               "y", view->right_margin,
                               NULL);

        gnome_canvas_item_set (view->page,
                               "x1", 0.0, // view->left_margin,
                               "y1", 0.0, // view->top_margin,
                               "x2", view->page_hsize - view->right_margin,
                               "y2", view->page_vsize - view->bottom_margin,
                               NULL);

        gnome_canvas_set_scroll_region (GNOME_CANVAS (view->canvas),
                                        0.0, 0.0,
                                        view->page_hsize + view->left_margin + view->right_margin,
                                        view->page_vsize + view->top_margin + view->bottom_margin);
}

void
ludwig_view_set_left_margin (LudwigView *view, double margin)
{
        view->left_margin = margin;

        ludwig_view_update_scroll_region (view);
}

void
ludwig_view_set_right_margin (LudwigView *view, double margin)
{
        view->right_margin = margin;

        ludwig_view_update_scroll_region (view);
}

void
ludwig_view_set_top_margin (LudwigView *view, double margin)
{
        view->top_margin = margin;

        ludwig_view_update_scroll_region (view);
}

void
ludwig_view_set_bottom_margin (LudwigView *view, double margin)
{
        view->bottom_margin = margin;

        ludwig_view_update_scroll_region (view);
}

void
ludwig_view_set_page_size (LudwigView *view, double hsize, double vsize)
{
        view->page_hsize = hsize;
        view->page_vsize = vsize;

        ludwig_view_update_scroll_region (view);
}

void
ludwig_view_set_metric_units (LudwigView *view, LudwigDisplayMetric units)
{
        view->units = units;

        gtk_signal_emit_by_name (GTK_OBJECT (GTK_LAYOUT (view->canvas)->vadjustment),
                                 "value_changed");
        gtk_signal_emit_by_name (GTK_OBJECT (GTK_LAYOUT (view->canvas)->hadjustment),
                                 "value_changed");
}
