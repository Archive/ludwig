/*
 * Copyright (C) 2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell  <bratsche@gnome.org>
 */

#include "ludwig-view-item.h"

static void ludwig_view_item_class_init (LudwigViewItemClass *class);
static void ludwig_view_item_init       (LudwigViewItem      *item);
static void ludwig_view_item_destroy    (GtkObject           *object);
static void ludwig_view_item_set_arg    (GtkObject           *object,
                                         GtkArg              *arg,
                                         guint                arg_id);
static void ludwig_view_item_get_arg    (GtkObject           *object,
                                         GtkArg              *arg,
                                         guint                arg_id);

static GnomeCanvasGroupClass *parent_class = NULL;

enum
{
        ARG_0,
        ARG_CONTAINER
};

GtkType
ludwig_view_item_get_type (void)
{
        static GtkType type = 0;

        if (!type)
        {
                GtkTypeInfo info =
                {
                        "LudwigViewItem",
                        sizeof (LudwigViewItem),
                        sizeof (LudwigViewItemClass),
                        (GtkClassInitFunc)  ludwig_view_item_class_init,
                        (GtkObjectInitFunc) ludwig_view_item_init,
                        NULL, /* Reserved 1 */
                        NULL, /* Reserved 2 */
                        (GtkClassInitFunc) NULL
                };

                type = gtk_type_unique (gnome_canvas_group_get_type (), &info);
        }

        return type;
}

static void
ludwig_view_item_class_init (LudwigViewItemClass *class)
{
        GtkObjectClass *object_class;

        object_class = (GtkObjectClass *) class;

        parent_class = gtk_type_class (gnome_canvas_group_get_type ());

        gtk_object_add_arg_type ("LudwigViewItem::container",
                                 GTK_TYPE_POINTER,
                                 GTK_ARG_READWRITE,
                                 ARG_CONTAINER);

        object_class->destroy = ludwig_view_item_destroy;
        object_class->set_arg = ludwig_view_item_set_arg;
        object_class->get_arg = ludwig_view_item_get_arg;
};

static void
ludwig_view_item_init (LudwigViewItem *item)
{
        item->container = NULL;
        item->x = 0.0;
        item->y = 0.0;
}

static void
ludwig_view_item_destroy (GtkObject *object)
{
}

static void
ludwig_view_item_set_arg (GtkObject *object,
                          GtkArg    *arg,
                          guint      arg_id)
{
        LudwigViewItem *item = (LudwigViewItem *) object;

        switch (arg_id)
        {
        case ARG_CONTAINER:
                item->container = (LudwigViewItem *) GTK_VALUE_POINTER (*arg);
                break;
        }
}

static void
ludwig_view_item_get_arg (GtkObject *object,
                          GtkArg    *arg,
                          guint      arg_id)
{
        LudwigViewItem *item = (LudwigViewItem *) object;

        switch (arg_id)
        {
                GTK_VALUE_POINTER (*arg) = item->container;
                break;
        }
}
