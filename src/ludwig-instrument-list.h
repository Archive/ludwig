/*
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#ifndef __LUDWIG_INSTRUMENT_LIST_H__
#define __LUDWIG_INSTRUMENT_LIST_H__

#include <gtk/gtkobject.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "ludwig-instrument.h"
#include "ludwig-types.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*
 * We want to store a list of instruments with the following data:
 *   1/ Instrument name       \
 *   2/ Standard abbreviation / Foreign translations for these
 *   3/ Default clef
 *   4/ All available clefs
 *   5/ Range
 *   6/ Any special notes about the instrument
 *
 * We also want to store several profiles, e.g., "Student", "Pro", etc.
 */

#define LUDWIG_TYPE_INSTRUMENT_LIST        (ludwig_instrument_list_get_type())
#define LUDWIG_INSTRUMENT_LIST(o)          (GTK_CHECK_CAST((o), LUDWIG_TYPE_INSTRUMENT_LIST, LudwigInstrumentList))
#define LUDWIG_INSTRUMENT_LIST_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), LUDWIG_TYPE_INSTRUMENT_LIST, LudwigInstrumentListClass))
#define LUDWIG_IS_INSTRUMENT_LIST(o)       (GTK_CHECK_TYPE((o), LUDWIG_TYPE_INSTRUMENT_LIST))
#define LUDWIG_IS_INSTRUMENT_LIST_CLASS(k) (GTK_CHECK_CLASS_TYPE((k), LUDWIG_TYPE_INSTRUMENT_LIST))

typedef struct _LudwigInstrumentList       LudwigInstrumentList;
typedef struct _LudwigInstrumentListClass  LudwigInstrumentListClass;
typedef struct _LudwigInstrumentListName   LudwigInstrumentListName;
typedef struct _LudwigInstrumentListRecord LudwigInstrumentListRecord;

typedef struct _LudwigInstrumentGroup      LudwigInstrumentGroup;

typedef void (* LILGroupFunc)           (const gchar *name, int pos);
typedef void (* LILInstrumentFunc)      (const gchar *group, const gchar *name);
typedef void (* LILInstrumentGroupFunc) (const gchar *instrument, int clef, int pos, gpointer user_data);

struct _LudwigInstrumentList
{
        GtkObject parent_object;

        GConfClient *gconf_client;
        GHashTable  *groups;
        GHashTable  *records;
};

struct _LudwigInstrumentListClass
{
        GtkObjectClass parent_class;
};

struct _LudwigInstrumentGroup
{
        GHashTable *records;
        int pos;
};

/* Public interfaces */
GtkType               ludwig_instrument_list_get_type                 (void);
void                  ludwig_instrument_list_create                   (void);

/* Query functions */
LudwigInstrument  *ludwig_instrument_list_query_instrument            (const char *name);
GArray            *ludwig_instrument_list_query_names                 (void);

void               ludwig_instrument_list_foreach_group               (LILGroupFunc func);
void               ludwig_instrument_list_foreach                     (LILInstrumentFunc func);
void               ludwig_instrument_list_foreach_instrument_in_group (const char *group,
                                                                       LILInstrumentGroupFunc func,
                                                                       gpointer user_data);

/* Set functions */
void               ludwig_instrument_list_add_group                   (const gchar *name, int pos);
void               ludwig_instrument_list_add_instrument              (const char *id, const gchar *group, LudwigInstrument *instrument);

#ifdef __cplusplus
}
#endif /* __cpluplus */

#endif /* __LUDWIG_INSTRUMENT_LIST_H__ */
