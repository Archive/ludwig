/*
 * ludwig-document.c - Document object for Ludwig van.  Maintains the
 *                     score structures, and handles loading and saving
 *                     them into XML files.
 *
 * Copyright (C) 2000-2001 by Cody Russell
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Author: Cody Russell <bratsche@gnome.org>
 */

#include <stdlib.h>
#include <glib.h>
#include <gtk/gtksignal.h>
#include <gnome-xml/parser.h>
#include <gnome-xml/tree.h>
#include "ludwig-document.h"
#include "ludwig-doc-part.h"
#include "ludwig-util.h"

enum
{
        INSTRUMENT_ADDED,
        MEASURE_ADDED,
	LAST_SIGNAL
};

static GtkObjectClass *parent_class = NULL;
static guint           signals[LAST_SIGNAL];

/*
 * Object destruction
 */
static void
part_destroy_cb (gpointer key, gpointer value, gpointer user_data)
{
        LudwigDocPart *part = value;

        gtk_object_destroy (GTK_OBJECT (part));
}

static void
ludwig_document_destroy (GtkObject *object)
{
        LudwigDocument *doc = (LudwigDocument *) object;

        g_hash_table_foreach (doc->parts, &part_destroy_cb, NULL);
        g_free (doc->filename);

        GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

/*
 * Object initialization
 */

static void
ludwig_document_class_init (LudwigDocumentClass *class)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (class);
	parent_class = gtk_type_class (gtk_object_get_type ());

        signals[INSTRUMENT_ADDED] =
                gtk_signal_new ("instrument_added",
                                GTK_RUN_FIRST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (LudwigDocumentClass,
                                                   instrument_added),
                                gtk_marshal_NONE__POINTER_INT,
                                GTK_TYPE_NONE, 2,
                                GTK_TYPE_STRING,
                                GTK_TYPE_INT);

        signals[MEASURE_ADDED] =
                gtk_signal_new ("measure_added",
                                GTK_RUN_FIRST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (LudwigDocumentClass,
                                                   measure_added),
                                gtk_marshal_NONE__POINTER,
                                GTK_TYPE_NONE,
                                1,
                                GTK_TYPE_STRING);

		gtk_object_class_add_signals (object_class,
                                              signals,
                                              LAST_SIGNAL);

        object_class->destroy = ludwig_document_destroy;
}

static void
ludwig_document_init (LudwigDocument *document)
{
	document->title = NULL;
	document->composer = NULL;
	document->parts = g_hash_table_new (ludwig_strcase_hash, ludwig_strcase_equal);
}

/*
 * Internal XML loading code..
 */

static void
parse_xml_font (LudwigString *string, xmlNode *node)
{
	xmlChar *xml_name;
	xmlChar *xml_size;
	xmlChar *xml_adjustment;
	float    size;
	float    x, y;

	xml_name = xmlGetProp (node, "name");
	xml_size = xmlGetProp (node, "size");
	xml_adjustment = xmlGetProp (node, "adjustment");

	size = atoi (xml_size);
	sscanf (xml_adjustment, "%f%f", &x, &y);

	ludwig_string_request_font (string, xml_name, size);
	/* Set string->adj adjustment here */
}

static void
parse_xml_score_title (LudwigDocument *doc, xmlNode *parent)
{
	xmlNode *node;
	xmlChar *xml_name;

	if (doc->title)
		g_free (doc->title);

	xml_name = xmlGetProp (parent, "name");
	doc->title = ludwig_string_new ((gchar *) xml_name);
	g_free (xml_name);

	for (node = ludwig_xml_get_children (parent); node != NULL; node = node->next)
	{
		if (strcmp (node->name, "text") == 0)
			parse_xml_font (doc->title, node);
		else
			g_warning ("ludwigdoc:score:title:%s is invalid.\n", node->name);
	}
}

static void
parse_xml_score_composer (LudwigDocument *doc, xmlNode *parent)
{
	xmlNode *node;
	xmlChar *xml_name;

	if (doc->composer)
		g_free (doc->composer);

	xml_name = xmlGetProp (parent, "name");
	doc->composer = ludwig_string_new ((gchar *) xml_name);
	g_free (xml_name);

	for (node = ludwig_xml_get_children (parent); node != NULL; node = node->next)
	{
		if (strcmp (node->name, "font") == 0)
			parse_xml_font (doc->composer, node);
		else
			g_warning ("ludwigdoc:score:composer:%s is invalid.\n", node->name);
	}
}

static void
parse_xml_score_staff_measure (LudwigDocument *doc, xmlNode *parent)
{
	xmlNode *node;

	for (node = ludwig_xml_get_children (parent); node != NULL; node = node->next)
	{
		if (strcmp (node->name, "note") == 0)
		{
			xmlChar *xml_beat;
			xmlChar *xml_length;
			xmlChar *xml_pitch;

			xml_beat = xmlGetProp (node, "beat");
			xml_length = xmlGetProp (node, "length");
			xml_pitch = xmlGetProp (node, "pitch");

			/* Insert the note into the measure. */

			g_free (xml_beat);
			g_free (xml_length);
			g_free (xml_pitch);
		}
	}
}

static void
parse_xml_score_staff (LudwigDocument *doc, xmlNode *parent)
{
	xmlNode *node;
	xmlChar *xml_name;
	xmlChar *xml_originclef;
	xmlChar *xml_originkey;
	xmlChar *xml_origintime;

	xml_name       = xmlGetProp (parent, "name");
	xml_originclef = xmlGetProp (parent, "originclef");
	xml_originkey  = xmlGetProp (parent, "originkey");
	xml_origintime = xmlGetProp (parent, "origintime");

	for (node = ludwig_xml_get_children (parent); node != NULL; node = node->next)
	{
		if (strcmp (node->name, "measure") == 0)
			parse_xml_score_staff_measure (doc, node);
		else
			g_warning ("ludwigdoc:score:staff:%s is invalid.\n", node->name);
	}
}

static void
parse_xml_score (LudwigDocument *doc, xmlNode *parent)
{
	xmlNode *node;

	for (node = ludwig_xml_get_children (parent); node != NULL; node = node->next)
	{
		if (strcmp (node->name, "title") == 0)
			parse_xml_score_title (doc, node);
		else if (strcmp (node->name, "composer") == 0)
			parse_xml_score_composer(doc, node);
		else if (strcmp(node->name, "staff") == 0)
			parse_xml_score_staff (doc, node);
		else
			g_warning ("XML: ludwigdoc:score:%s is invalid.\n", node->name);
	}
}

static void
parse_xml_ludwigdoc (LudwigDocument *doc, xmlNode *parent)
{
	xmlNode *node;

	for (node = ludwig_xml_get_children (parent); node != NULL; node = node->next)
	{
		if (strcmp(node->name, "score") == 0)
			parse_xml_score (doc, node);
		else
			g_warning ("XML error: Only 'score' may be under 'ludwigdoc'.\n");
	}
}

/*
 * Private XML file saving code..
 */

static void
append_xml_score_node (gpointer key, gpointer val, gpointer user_data)
{
#if 0
	xmlNodePtr root_node, score_node;
	LudwigDocument *doc;

	g_assert (LUDWIG_IS_DOCUMENT (data));

	doc = LUDWIG_DOCUMENT (data);
	root_node = (xmlNode *) user_data;

	score_node = xmlNewChild (root_node, NULL, "score", NULL);
	xmlSetProp (score_node, "beat", doc->measure->note);
#endif
}

/*
 * Public Interfaces
 */

GtkType
ludwig_document_get_type (void)
{
	static GtkType document_type = 0;

	if (!document_type)
	{
		const GtkTypeInfo document_info =
		{
			"LudwigDocument",
			sizeof (LudwigDocument),
			sizeof (LudwigDocumentClass),
			(GtkClassInitFunc)  ludwig_document_class_init,
			(GtkObjectInitFunc) ludwig_document_init,
			NULL,  /* Reserved 1 */
			NULL,  /* Reserved 2 */
			(GtkClassInitFunc) NULL,
		};

		document_type = gtk_type_unique (gtk_object_get_type (),
                                                 &document_info);
	}

	return document_type;
}

LudwigDocument *
ludwig_document_new (gchar *filename)
{
	LudwigDocument *ret;

        ret = gtk_type_new (LUDWIG_TYPE_DOCUMENT);

        if (filename != NULL)
        {
                ret->filename = g_strdup (filename);

                if (g_file_exists (filename))
                {
                        ludwig_document_load (ret);
                }
        }

	return ret;
}

void
ludwig_document_load (LudwigDocument *doc)
{
	xmlNode *node;
	FILE    *file;

	g_return_if_fail (doc != NULL);
	g_return_if_fail (doc->filename != NULL);

	file = fopen (doc->filename, "r");
	if (file != NULL)
	{
		int res, size = 1024;
		char chars[1024];
		xmlParserCtxtPtr ctxt;

		res = fread (chars, 1, 4, file);
		if (res > 0)
		{
			ctxt = xmlCreatePushParserCtxt (NULL, NULL, chars, res, doc->filename);
			while ((res = fread (chars, 1, size, file)) > 0)
			{
				xmlParseChunk (ctxt, chars, res, 0);
			}

			xmlParseChunk (ctxt, chars, 0, 1);
			doc->doc = ctxt->myDoc;
			xmlFreeParserCtxt (ctxt);
		}
	}

	for (node = ludwig_xml_get_root_children (doc->doc); node != NULL; node = node->next)
	{
		if (strcmp (node->name, "ludwigdoc") == 0)
		{
			parse_xml_ludwigdoc (doc, node);
		}
		else
		{
			g_warning ("LudwigDocument: Toplevel XML tag must be <ludwigdoc>!\n");
		}
	}
}

void
ludwig_document_save (LudwigDocument *doc)
{
	xmlNode *root;

	doc->doc = xmlNewDoc ("1.0");
	root = xmlNewDocNode (doc->doc, NULL, "ludwigdoc", NULL);
	xmlDocSetRootElement (doc->doc, root);

	/* Save scores.. */
	g_hash_table_foreach (doc->parts, append_xml_score_node, root);

	xmlSaveFile (doc->filename, doc->doc);
	xmlFreeDoc (doc->doc);
}

LudwigDocPart *
ludwig_document_get_part (LudwigDocument *doc, const gchar *part)
{
        return g_hash_table_lookup (doc->parts, part);
}

void
ludwig_document_add_part (LudwigDocument *doc, LudwigInstrument *instrument, LudwigClef clef)
{
        GHashTable *table;
        LudwigDocPart *part;

        table = doc->parts;

        part = ludwig_doc_part_new ();
        part->instrument = instrument;

        g_hash_table_insert (doc->parts, g_strdup (instrument->id), part);

        gtk_signal_emit (GTK_OBJECT (doc), signals[INSTRUMENT_ADDED], instrument->id, clef);
}
