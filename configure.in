dnl ============================================================
dnl Ludwig van's configure scripts.. shamelessly stolen from
dnl Eric Fischer's Nautilus configure scripts!

AC_INIT(src)

GNOME_REQUIRED=1.2.4
GNOME_LIBS_REQUIRED=1.2.11
GTK_REQUIRED=1.2.8
GLIB_REQUIRED=1.2.8
BONOBO_REQUIRED=0.33
GAL_REQUIRED=0.7
OAF_REQUIRED=0.6.2
GCONF_REQUIRED=0.11
LIBXML_REQUIRED=1.8.10
FREETYPE_REQUIRED=6:1:0

AC_SUBST(GNOME_REQUIRED)
AC_SUBST(GNOME_LIBS_REQUIRED)
AC_SUBST(GTK_REQUIRED)
AC_SUBST(GLIB_REQUIRED)
AC_SUBST(BONOBO_REQUIRED)
AC_SUBST(GAL_REQUIRED)
AC_SUBST(OAF_REQUIRED)
AC_SUBST(GCONF_REQUIRED)
AC_SUBST(LIBXML_REQUIRED)
AC_SUBST(FREETYPE_REQUIRED)

dnl ===========================================================

dnl LUDWIG_VERSION_CANON(version)
dnl

AC_DEFUN(LUDWIG_VERSION_CANON, [`
	dnl Assumes that there are no more than 999 revisions at a level,
	dnl no more than three levels of revision.
	dnl
	dnl Any more than that, and test starts messing up the numeric
	dnl comparisons because its integers overflow, and there's no
	dnl way to do string comparisons in the shell.  Grr.
	dnl
	dnl Must come up with some way to fix this.

	echo "$1" |
	tr . '\012' |
	sed -e 's/^/000/' -e 's/^.*\(...\)/\1/' |
	tr -d '\012' |
	sed 's/$/000000000/
	     s/^\(.........\).*/\1/'
`])

dnl LUDWIG_VERSION_INSIST(package, get-version-cmd, operator, want-version-var)
dnl                       1        2                3         4

AC_DEFUN(LUDWIG_VERSION_INSIST, [
	want_version=[$]$4

	case "$3" in
		">")	operator=-gt ;;
		">=")	operator=-ge ;;
		"<")	operator=-lt ;;
		"<=")	operator=-le ;;
		"=")	operator=-eq ;;
		"!=")	operator=-ne ;;
		*)	AC_ERROR(Unknown operator $3 in configure script) ;;
	esac

	AC_MSG_CHECKING(for $1 $3 [$want_version])

	if installed_version="`$2`"
	then
		AC_MSG_RESULT([$installed_version])
	else
		AC_ERROR($2 failed)
	fi

	if test "LUDWIG_VERSION_CANON([$installed_version])" "$operator" \
		"LUDWIG_VERSION_CANON([$want_version])"
	then
		:
		AC_SUBST($4)
	else
		AC_ERROR($1 version [$want_version] is required.)
	fi
])


AM_INIT_AUTOMAKE(Ludwig_van, 0.1)
AM_CONFIG_HEADER(config.h)

AM_MAINTAINER_MODE

AM_DISABLE_STATIC
AM_PROG_LIBTOOL

AM_PROG_XML_I18N_TOOLS

AM_SANITY_CHECK
AC_PROG_CC
AC_PROG_INSTALL
AC_PROG_MAKE_SET
AC_ISC_POSIX
AM_PATH_ORBIT

dnl ====================================
dnl = Begin tests for FreeType2
dnl ====================================
AM_PATH_FREETYPE2($FREETYPE_REQUIRED,,AC_MSG_ERROR([*** Freetype2 $FREETYPE_REQUIRED or better is required.]))

dnl =======================
dnl = End tests for FreeType2
dnl =======================

dnl Checks for libraries.
AM_PATH_GLIB($GLIB_REQUIRED,,
AC_MSG_ERROR([
*** GLIB $GLIB_REQUIRED or better is required. The latest version of GLIB
*** is always available from ftp://ftp.gtk.org/.]))

AC_SUBST(GLIB_REQUIRED)

AM_PATH_GTK($GTK_REQUIRED,,
AC_MSG_ERROR([
*** Gtk+ $GTK_REQUIRED or better is required. The latest version of GTK
*** is always available from ftp://ftp.gtk.org/.]))

AC_SUBST(GTK_REQUIRED)  

AM_PATH_GNOME($GNOME_REQUIRED,,AC_MSG_ERROR([*** GNOME $GNOME_REQUIRED or better is 
required.]), bonobo bonobox bonobox_print libglade gal libart)

AC_SUBST(GNOME_REQUIRED)
AC_PATH_PROG(GNOME_CONFIG,gnome-config,no)
if test x$GNOME_CONFIG = xno; then
  AC_MSG_ERROR(Couldn't find gnome-config. Please install the GNOME package)
fi

LUDWIG_VERSION_INSIST(gnome-libs, $GNOME_CONFIG --version | awk '{print $2}', >=, 
GNOME_LIBS_REQUIRED)

dnl Bonobo
LUDWIG_VERSION_INSIST(bonobo, $GNOME_CONFIG --modversion bonobo | awk -F- '{print $2}', >=, 
BONOBO_REQUIRED)

AC_SUBST(BONOBO_CFLAGS)
AC_SUBST(BONOBO_LIBS)
AC_SUBST(BONOBOX_CFLAGS)
AC_SUBST(BONOBOX_LIBS)
AC_SUBST(BONOBO_PRINT_CFLAGS)
AC_SUBST(BONOBO_PRINT_LIBS)

dnl   Bug 7315: Rediscover CFLAGS for GTK, because gnome-config won't know
dnl   all the necessary flags if GTK has been upgraded more recently than
dnl   it has. 

AC_PATH_PROG(GTK_CONFIG,gtk-config,false)
AC_MSG_CHECKING(for gtk-config cflags)
GTK_CFLAGS=`$GTK_CONFIG --cflags`
AC_MSG_RESULT($GTK_CFLAGS)
GNOMEUI_CFLAGS="$GNOMEUI_CFLAGS $GTK_CFLAGS"
AC_SUBST(GNOMEUI_CFLAGS)

AM_PATH_OAF($OAF_REQUIRED) 
AC_SUBST(OAF_REQUIRED)

AM_PATH_GCONF($GCONF_REQUIRED,,,gconf-gtk)
AC_SUBST(GCONF_REQUIRED)

dnl ********************
dnl libglade checking
dnl ********************
AC_MSG_CHECKING(for Glade libraries)
if gnome-config --libs libglade > /dev/null 2>&1; then
   AC_MSG_RESULT(found)
else
   AC_MSG_ERROR(Did not find libGlade)
fi

GLADE_CFLAGS=`$GNOME_CONFIG --cflags libglade`
GLADE_LIBS=`$GNOME_CONFIG --libs libglade`

AC_SUBST(GLADE_CFLAGS)
AC_SUBST(GLADE_LIBS)

dnl ****************************
dnl Finished libglade configure
dnl ****************************

dnl ***  GAL ***
LUDWIG_VERSION_INSIST(gal, $GNOME_CONFIG --modversion gal | awk -F- '{print $2}', >=, GAL_REQUIRED)

GAL_CFLAGS=`$GNOME_CONFIG --cflags gal`
GAL_LIBS=`$GNOME_CONFIG --libs gal`


AC_SUBST(GAL_CFLAGS)
AC_SUBST(GAL_LIBS)


dnl ***********************
dnl gnome-print checking
dnl ***********************
AC_MSG_CHECKING(for gnome-print)
if $GNOME_CONFIG --libs print > /dev/null 2>&1; then
	AC_MSG_RESULT(found)
else
	AC_MSG_ERROR(Didn't find gnome-print)
fi
AC_SUBST(GNOME_PRINT_LIBS)

PRINT_LIBS="`$GNOME_CONFIG --libs print` -lunicode"
PRINT_CFLAGS=`$GNOME_CONFIG --cflags print`

AC_SUBST(PRINT_LIBS)
AC_SUBST(PRINT_CFLAGS)

dnl ************************
dnl gnome-print checks done
dnl ************************


AC_PATH_PROG(XML_CONFIG,xml-config,no)
if test x$XML_CONFIG = xno; then
  AC_MSG_ERROR(Couldn't find xml-config please install the gnome-xml package)
fi

XML_VERSION="`$XML_CONFIG --version`"
case "$XML_VERSION" in
        1.*)
                :
        ;;

        *)
                AC_ERROR(Ludwig van needs gnome-xml version 1.x not $XML_VERSION)
        ;;
esac

XML_LIBS=`$XML_CONFIG --libs`
XML_CFLAGS=`$XML_CONFIG --cflags`
AC_SUBST(XML_LIBS)
AC_SUBST(XML_CFLAGS)

dnl Checks for i18n
ALL_LINGUAS="ru sv"
AM_GNOME_GETTEXT
# AM_GNOME_GETTEXT above substs $DATADIRNAME
# this is the directory where the *.{mo,gmo} files are installed
gnomelocaledir='${prefix}/${DATADIRNAME}/locale'
AC_SUBST(gnomelocaledir)
        
dnl Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
                
dnl Turn on the additional warnings last, so -Werror doesn't affect other tests.
     
AC_ARG_ENABLE(more-warnings,
[  --enable-more-warnings  Maximum compiler warnings],
set_more_warnings="$enableval",[
if test -f $srcdir/CVSVERSION; then
        set_more_warnings=yes
else
        set_more_warnings=no
fi
])
AC_MSG_CHECKING(for more warnings, including -Werror)
if test "$GCC" = "yes" -a "$set_more_warnings" != "no"; then
        AC_MSG_RESULT(yes)
        CFLAGS="\
        -Wall \
        -Wchar-subscripts -Wmissing-declarations -Wmissing-prototypes \
        -Wnested-externs -Wpointer-arith \
        -Wcast-align -Wsign-compare \
        -Werror \
        $CFLAGS"

        for option in -Wsign-promo -Wno-sign-compare; do
                SAVE_CFLAGS="$CFLAGS"
                CFLAGS="$CFLAGS $VFS_CFLAGS $option"  
                AC_MSG_CHECKING([whether gcc understands $option])
                AC_TRY_COMPILE([], [],
                        has_option=yes,
                        has_option=no,)
                CFLAGS="$SAVE_CFLAGS"
                if test $has_option = yes; then
                  VFS_CFLAGS="$VFS_CFLAGS $option"
                fi
                AC_MSG_RESULT($has_option)
                unset has_option
                unset SAVE_CFLAGS
        done   
        unset option
else
        AC_MSG_RESULT(no)
fi

AC_OUTPUT([
Makefile
fonts/Makefile
src/Makefile
src/canvas-items/Makefile
src/ludwig-font/Makefile
src/ludwig-font/Makefile
src/ludwig-font/ft2/Makefile
src/ludwig-font/Makefile
])
